const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
require('dotenv').config()
const webpack = require("webpack")

function parseBool(value) {
			if (value.match(/^true$/i)) {
				return true;
			}
			return false;
}

const config = {
    entry: {
        app: './src/client/App.tsx',
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].js',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
	},
	plugins: [
		new webpack.DefinePlugin({
				'process.env.SMS_AUTH': parseBool(process.env.SMS_AUTH)
		})
	],
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                loader: 'ts-loader',
                options: {
                    context: path.resolve(__dirname, 'src', 'client'),
                    configFile: 'tsconfig.json',
                },
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
};

module.exports = (env, argv) => {
    config.mode = argv.mode;
    const isDev = argv.mode === 'development';
		const isProd = argv.mode === 'production';


    if (isProd) {
        config.devtool = false;

        config.optimization = {
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        format: {
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
            ],
        };
    }

    if (isDev) {
        config.devtool = 'eval-source-map';

        config.devServer = {
            noInfo: true,
            overlay: true,
            compress: true, // gzip
            publicPath: '/', // path where output will be accessible
            historyApiFallback: true, // always return index.html
            contentBase: [path.join(__dirname, './public')],
        };
    }

    return config;
};
