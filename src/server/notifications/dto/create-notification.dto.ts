import { User } from 'src/server/users/entities/user.entity';

export class CreateNotificationDto {
    user: User;
    text: string;
}
