import { User } from 'src/server/users/entities/user.entity';
import {
    Column,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('viewed_notifications')
export class ViewedNotification {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => User, { nullable: false })
    @JoinColumn()
    user: User;

    @Column()
    last_viewed_id: number;
}
