import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseGuards,
} from '@nestjs/common';
import { RoleGuard } from '../guards/role.guard';
import { SessionAuthGuard } from '../guards/session-auth.guard';
import { Role } from '../users/entities/role';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { NotificationsService } from './notifications.service';

@Controller('api/notifications')
@UseGuards(SessionAuthGuard)
export class NotificationsController {
    constructor(private readonly notificationsService: NotificationsService) {}

    @Get()
    async getNotifications() {
        return await this.notificationsService.getNotifications();
    }

    @UseGuards(new RoleGuard([Role.ADMINISTRATOR]))
    @Post()
    async createNotification(@Body() body: CreateNotificationDto) {
        return await this.notificationsService.createNotification(body);
    }

    @UseGuards(new RoleGuard([Role.ADMINISTRATOR]))
    @Put(':id')
    async updateNotification(
        @Param('id') id: string,
        @Body() body: UpdateNotificationDto,
    ) {
        return await this.notificationsService.updateNotification(+id, body);
    }

    @UseGuards(new RoleGuard([Role.ADMINISTRATOR]))
    @Delete(':id')
    async deleteNotification(@Param('id') id: string) {
        return await this.notificationsService.deleteNotification(+id);
    }

    @Post('/view/:id')
    async viewNotification(@Param('id') id: string) {
        return await this.notificationsService.viewNotification(+id);
    }

    @Get('viewed')
    async getViewedInfo() {
        return await this.notificationsService.getViewedInfo();
    }
}
