import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, MoreThan } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { Notification } from './entities/notification.entity';
import { Request } from 'express';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { ViewedNotification } from './entities/viewed-notification';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable({
    scope: Scope.REQUEST,
})
export class NotificationsService {
    private user: User;

    constructor(
        @Inject(REQUEST) private readonly request: Request,
        @InjectRepository(Notification)
        private notificationsRepository: Repository<Notification>,
        @InjectRepository(ViewedNotification)
        private viewedRepository: Repository<ViewedNotification>,
        private eventEmitter: EventEmitter2,
    ) {
        this.user = this.request.user as User;
    }

    async createNotification(createNotificationDto: CreateNotificationDto) {
        const notification = this.notificationsRepository.create({
            ...createNotificationDto,
            user: this.user,
        });

        return await this.notificationsRepository
            .save<Notification>(notification)
            .then((res) => this.eventEmitter.emit('notification.created', res));
    }

    async getNotifications() {
        return await this.notificationsRepository.find({
            order: {
                created_at: 'DESC',
            },
        });
    }

    async updateNotification(
        id: number,
        updateNotificationDto: UpdateNotificationDto,
    ) {
        return await this.notificationsRepository
            .update(
                {
                    id,
                },
                updateNotificationDto,
            )
            .then(async () => {
                const notification = await this.notificationsRepository.findOne(
                    { where: { id } },
                );
                this.eventEmitter.emit('notification.updated', notification);
            });
    }

    async deleteNotification(id: number) {
        const notification = await this.notificationsRepository.findOne({
            where: { id },
        });
        if (notification)
            return await this.notificationsRepository
                .delete({ id })
                .then(() =>
                    this.eventEmitter.emit(
                        'notification.deleted',
                        notification,
                    ),
                );
    }

    async viewNotification(id: number) {
        let viewed = await this.viewedRepository.findOne({
            where: {
                user: {
                    id: this.user.id,
                },
            },
        });
        if (!viewed) {
            viewed = await this.viewedRepository.create({
                user: {
                    id: this.user.id,
                },
                last_viewed_id: id,
            });
            return await this.viewedRepository.save(viewed);
        } else {
            return await this.viewedRepository.update(
                {
                    user: {
                        id: this.user.id,
                    },
                },
                {
                    last_viewed_id: id,
                },
            );
        }
    }

    async getViewedInfo() {
        const last_viewed =
            +(
                await this.viewedRepository.findOne({
                    where: {
                        user: {
                            id: this.user.id,
                        },
                    },
                })
            )?.last_viewed_id || 0;

        return {
            notifications_count:
                (await this.notificationsRepository.count({
                    where: {
                        id: MoreThan(last_viewed),
                    },
                })) || 0,
            last_viewed,
        };
    }
}
