import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { User } from '../users/entities/user.entity';

@Injectable()
export class CookieSerializer extends PassportSerializer {
    serializeUser(user: any, done: (err: any, id?: any) => void): void {
        done(null, user);
    }

    async deserializeUser(payload: any, done: (err: any, id?: any) => void) {
        done(
            null,
            (await User.findOne({
                where: {
                    id: payload.id,
                },
            })) || payload,
        );
    }
}
