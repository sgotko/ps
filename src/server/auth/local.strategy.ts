import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Strategy } from 'passport-local';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
	
    constructor(private authService: AuthService, private configService: ConfigService) {
        super({
            usernameField: 'login',
            passReqToCallback: true,
        });
    }

		parseBool(value: string): boolean {
			if (value.match(/^true$/i)) {
				return true;
			}
			return false;
		}

    async validate(req: any, login: string, password: string): Promise<any> {
        const code = req.body.code;

				const withCode = this.parseBool(this.configService.get("SMS_AUTH") || "");

        const user = withCode ? await this.authService.validateUserWithCode(
            login,
            password,
            code,
        ): await this.authService.validateUser(login, password);

        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
