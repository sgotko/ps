import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { AuthService } from './auth.service';
import { CookieSerializer } from './cookie.serializer';
import { BasicStrategy } from './basic.strategy';

@Module({
    imports: [
        UsersModule,
        PassportModule.register({ session: true, defaultStrategy: 'local' }),
    ],
    providers: [AuthService, LocalStrategy, BasicStrategy, CookieSerializer],
    exports: [AuthService, PassportModule],
})
export class AuthModule {}
