import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { BasicStrategy as Strategy } from 'passport-http';
import { AuthService } from './auth.service';

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy, 'basic') {
    constructor(private authService: AuthService) {
        super({
            usernameField: 'login',
        });
    }

    async validate(login: string, password: string): Promise<any> {
        const user = await this.authService.validateUser(login, password);

        if (!user) {
            return {
                response: {
                    status: 'ERROR',
                    message: 'UNAUTHORIZED',
                },
            };
        }
        return user;
    }
}
