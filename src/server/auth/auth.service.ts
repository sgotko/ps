import { Injectable } from '@nestjs/common';

import { Role } from '../users/entities/role';
import { User, verify } from '../users/entities/user.entity';
@Injectable()
export class AuthService {
    async validateUser(
        login: string,
        password: string,
    ): Promise<Partial<User> | null> {
        const user = await this.getUser(login);
        if (user) {
            const match = await verify(password, user.password);
            if (match && user.role) {
                user.password = undefined;
                return user;
            }
        }
        return null;
    }

    async validateUserWithCode(
        login: string,
        password: string,
        code: string,
    ): Promise<Partial<User> | null> {
        const user = await this.getUser(login);

        if (user) {
            const match = await verify(password, user.password);

            if (match && code === user.code && user.role !== Role.TERMINAL) {
                user.password = undefined;
                // return User after successful auth for cookie
                user.last_login = new Date();
                await user.save();
                return user;
            }
        }
        return null;
    }

    private async getUser(login: string) {
        return await User.findOne({
            where: {
                login: login,
            },
            select: [
                'password',
                'login',
                'id',
                'name',
                'isAdmin',
                'msisdn',
                'role',
                'code',
            ],
        });
    }
}
