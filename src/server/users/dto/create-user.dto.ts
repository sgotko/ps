import { User } from '../entities/user.entity';
import { Role } from '../entities/role';
import { Amount } from 'src/server/ps/types';

export class CreateUserDto {
    login: string;
    password: string;
    name: string;
    isAdmin?: boolean;
    balance?: Amount;
    percents?: Record<string, Amount>;
    code?: string;
    msisdn?: string;
    role?: Role;
    parent?: User;
}
