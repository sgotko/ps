import { Amount } from 'src/server/ps/types';
import { Percent } from '../entities/percent.entity';

export type CreatePercentDto = Partial<Percent> & {
    ps: string;
    percent: Amount;
};
