import {
    HttpException,
    Inject,
    Injectable,
    Logger,
    Scope,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Not, Repository, In } from 'typeorm';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

import * as xml2js from 'xml2js';
import * as iconv from 'iconv-lite';
import fetch from 'node-fetch';

import * as money from 'money-math';
import * as totp from 'totp-generator';
import { BalanceLog, LogType } from './entities/balance-log.entity';
import { UserLog, UserLogType } from './entities/user-log.entity';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Role } from './entities/role';
import { Bonus } from './entities/bonus.entity';
import { isProduction } from '../main';
import { Percent } from './entities/percent.entity';
import { Amount } from '../ps/types';

export type UserInfo = {
    id: number;
    name: string;
    balance?: string;
    role: string;
};

@Injectable({ scope: Scope.REQUEST })
export class UsersService {
    private PS_HOST: string;
    private PS_USER_NAME: string;
    private PS_PORT: string;

    private SMSC_HOST: string;
    private SMSC_PORT: string;
    private SMSC_LOGIN: string;
    private SMSC_PASSWORD: string;
    private SMSC_FROM: string;
    private user: User;

    private readonly logger = new Logger(UsersService.name);

    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        @InjectRepository(BalanceLog)
        private balanceLogRepository: Repository<BalanceLog>,
        @InjectRepository(UserLog)
        private userLogRepository: Repository<UserLog>,
        @InjectRepository(Bonus)
        private bonusRepository: Repository<Bonus>,
        @InjectRepository(Percent)
        private percentRepository: Repository<Percent>,
        configService: ConfigService,
        @Inject(REQUEST) private readonly request: Request,
    ) {
        this.PS_HOST = configService.get<string>('PS_HOST') || '127.0.0.1';
        this.PS_PORT = configService.get<string>('PS_PORT') || '0';
        this.PS_USER_NAME = configService.get<string>('PS_USER_NAME') || '';

        this.SMSC_HOST = configService.get<string>('SMSC_HOST') || '127.0.0.1';
        this.SMSC_PORT = configService.get<string>('SMSC_PORT') || '0';
        this.SMSC_LOGIN = configService.get<string>('SMSC_LOGIN') || '';
        this.SMSC_PASSWORD = configService.get<string>('SMSC_PASSWORD') || '';
        this.SMSC_FROM = configService.get<string>('SMSC_FROM') || '';

        this.user = this.request.user as User;
    }

    async getUserInfo(id: number): Promise<unknown> {
        const user = (await this.findOne(id)) as any;
        user.balance = (user.balance + '₽') as any;
        if (user.role === Role.PARTNER) {
            const children = await this.usersRepository.find({
                where: {
                    parent: {
                        id: id,
                    },
                },
            });
            const childs = children.reduce(
                (prev, curr) => money.add(prev, curr.balance),
                '0.00',
            );
            user.available = money.subtract(user.balance, childs) + '₽';
        }
        return user;
    }

    async withBalance(user: User) {
        if (user.isAdmin) {
            const url = `${this.PS_HOST}:${this.PS_PORT}/${this.PS_USER_NAME}/KKM_PG_GATE/GET_BALANCE_INFO`;
            let res: any = await fetch(url);
            res = await res.buffer();
            res = iconv.decode(res, 'win1251', {
                defaultEncoding: 'utf-16le',
            });

            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });

            if (res?.KKM_PG_GATE?.BALANCE) {
                return {
                    ...user,
                    balance: res.KKM_PG_GATE.BALANCE,
                };
            } else {
                throw new Error(`Ошибка при проверке счёта платёжной системы`);
            }
        } else return user;
    }

    async create(createUserDto: CreateUserDto) {
        try {
            if (this.user.isPartner()) createUserDto.parent = this.user;
            let user = this.usersRepository.create({ ...createUserDto } as Omit<
                CreateUserDto,
                'percents'
            >);
            user = await this.usersRepository.save<User>(user);
            Object.entries(createUserDto.percents).forEach(([key, value]) => {
                this.percentRepository
                    .create({
                        user: user,
                        ps: key,
                        percent: value,
                    })
                    .save();
            });
            this.userLogRepository.save({
                admin: this.user,
                user: user,
                type: UserLogType.Create,
                affected: Object.keys(createUserDto).join(','),
            });
            this.balanceLogRepository.save({
                admin: this.user,
                user: user,
                type: LogType.Add,
                old_balance: '0.00',
                new_balance: createUserDto.balance,
                diff: money.subtract(createUserDto.balance, '0.00'),
            });

            return user;
        } catch (e) {
            this.logger.error({ message: e });
            throw new HttpException(
                'Ошибка при создании пользователя: логин уже занят',
                500,
            );
        }
    }

    async findAll() {
        if (this.user.isPartner())
            return await this.usersRepository.find({
                where: [
                    {
                        parent: { id: this.user.id },
                        id: Not(this.user.id),
                    },
                ],
                order: {
                    name: 'ASC',
                },
                relations: ['parent', 'percents'],
                withDeleted: true,
            });
        else
            return await this.usersRepository.find({
                where: {
                    id: Not(this.user.id),
                },
                order: {
                    name: 'ASC',
                },
                relations: ['parent', 'percents'],
                withDeleted: true,
            });
    }

    async findFiltered(filters: { roles: string[] }) {
        return this.usersRepository.find({
            where: [
                {
                    id: Not(this.user.id),
                    role: In(filters.roles),
                },
            ],
            select: ['id', 'name'],
            relations: [],
        });
    }

    async findActive() {
        if (this.user.isPartner())
            return await this.usersRepository.find({
                where: {
                    parent: { id: this.user.id },
                    id: Not(this.user.id),
                    deleted_at: IsNull(),
                },
                order: {
                    name: 'ASC',
                },
                relations: ['parent', 'percents'],
            });
        else
            return await this.usersRepository.find({
                where: {
                    id: Not(this.user.id),
                    deleted_at: IsNull(),
                },
                order: {
                    name: 'ASC',
                },
                relations: ['parent', 'percents'],
            });
    }

    async findActiveTerminals() {
        return await this.usersRepository.find({
            where: {
                id: Not(this.user.id),
                deleted_at: IsNull(),
                role: Role.TERMINAL,
            },
            order: {
                name: 'ASC',
            },
            relations: ['parent', 'percents'],
        });
    }

    async findDeleted() {
        if (this.user.isPartner())
            return await this.usersRepository.find({
                where: {
                    parent: { id: this.user.id },
                    id: Not(this.user.id),
                    deleted_at: Not(IsNull()),
                },
                order: {
                    name: 'ASC',
                },
                relations: ['parent', 'percents'],
                withDeleted: true,
            });
        else
            return await this.usersRepository.find({
                where: {
                    id: Not(this.user.id),
                    deleted_at: Not(IsNull()),
                },
                order: {
                    name: 'ASC',
                },
                relations: ['parent', 'percents'],
                withDeleted: true,
            });
    }

    async findDeletedTerminals() {
        return await this.usersRepository.find({
            where: {
                id: Not(this.user.id),
                deleted_at: Not(IsNull()),
                role: Role.TERMINAL,
            },
            order: {
                name: 'ASC',
            },
            relations: ['parent', 'percents'],
        });
    }

    async findOne(id: number) {
        const user = await this.usersRepository.findOne({
            where: { id },
            relations: ['parent', 'percents', 'parent.percents'],
        });

        return user;
    }

    async findForValidate(login: string) {
        return await this.usersRepository.findOne({
            where: {
                login: login,
            },
            select: [
                'password',
                'login',
                'id',
                'name',
                'isAdmin',
                'msisdn',
                'role',
                'code',
            ],
        });
    }

    async update(id: number, updateUserDto: UpdateUserDto) {
        let user = await this.usersRepository.findOne({
            where: { id },
        });
        if (user) {
            if (updateUserDto.balance) {
                const oldBalance = user.balance;
                const newBalance = updateUserDto.balance;
                const diff = money.subtract(newBalance, oldBalance);
                this.balanceLogRepository.save({
                    admin: this.user,
                    user: user,
                    type: LogType.Edit,
                    old_balance: oldBalance,
                    new_balance: newBalance,
                    diff: diff,
                });
            }
            // remove link between user and parent if PARTNER becomes USER
            if (
                user.role === Role.PARTNER &&
                updateUserDto.role === Role.USER
            ) {
                User.createQueryBuilder()
                    .update(User)
                    .set({ parent: null })
                    .where('parentId = :id', { id: id })
                    .execute();
            }

            user = await this.usersRepository.save(
                Object.assign(user, { ...updateUserDto, percents: undefined }),
            );
            if (updateUserDto.percents) {
                Object.entries(updateUserDto.percents).forEach(
                    async ([key, value]) => {
                        const percent = await this.percentRepository.findOne({
                            where: {
                                user: {
                                    id: user.id,
                                },
                                ps: key,
                            },
                        });
                        if (percent) {
                            percent.percent = value;
                            percent.save();
                        }
                    },
                );
            }

            return user;
        }
    }

    async remove(id: number) {
        let user = await this.usersRepository.findOne({
            where: { id },
        });
        if (user && !user.isAdmin) {
            user = await this.usersRepository.softRemove({ id });
            this.userLogRepository.save({
                admin: this.user,
                user: user,
                type: UserLogType.Delete,
                affected: Object.keys(user).join(','),
            });

            return user;
        } else throw Error(`Ошибка при удалении пользователя`);
    }

    async restore(id: number) {
        let user = await this.usersRepository.findOne({
            where: { id },
            withDeleted: true,
        });
        if (user) {
            user = await this.usersRepository.recover({ id });
            this.userLogRepository.save({
                admin: this.user,
                user: user,
                type: UserLogType.Edit,
                affected: Object.keys(user).join(','),
            });

            return user;
        } else throw Error(`Ошибка при восстановлении пользователя`);
    }

    async chargeMoney(user: User, amount: Amount) {
        const oldBalance = user.balance;
        const newBalance = money.subtract(user.balance, amount);
        const diff = money.subtract(newBalance, oldBalance);
        const log = this.balanceLogRepository.create({
            admin: this.user,
            user: user,
            type: LogType.Charge,
            old_balance: oldBalance,
            new_balance: newBalance,
            diff: diff,
        });
        await this.balanceLogRepository.save(log);
        user.balance = newBalance;
        return user.save();
    }

    async addToBalanceBonus(user: User, amount: Amount) {
        const oldBalance = user.balance;
        const newBalance = money.add(user.balance, amount);
        const diff = money.subtract(newBalance, oldBalance);
        this.balanceLogRepository.save({
            admin: this.user,
            user: user,
            type: LogType.Bonus,
            old_balance: oldBalance,
            new_balance: newBalance,
            diff: diff,
        });
        return await this.usersRepository.save(
            Object.assign(user, { balance: newBalance }),
        );
    }

    async addMoney(id: number, amount: Amount) {
        const user = await this.usersRepository.findOne({
            where: { id },
        });
        if (user) {
            const oldBalance = user.balance;
            const newBalance = money.add(user.balance, amount);
            const diff = money.subtract(newBalance, oldBalance);

            this.balanceLogRepository.save({
                admin: this.user,
                user: user,
                type: LogType.Add,
                old_balance: oldBalance,
                new_balance: newBalance,
                diff: diff,
            });

            return await this.usersRepository.save(
                Object.assign(user, { balance: newBalance }),
            );
        } else throw Error(`Пользователь не найден`);
    }

    async addBonus(partner: User, user: User, amount: Amount, bonus: Amount) {
        return this.bonusRepository.save({
            partnerId: partner.id,
            userId: user.id,
            pAmount: amount,
            bonus: bonus,
        });
    }

    async sendTotpCode(login: string) {
        const user = await this.findForValidate(login);
        if (user && user.msisdn && user.msisdn.length > 0) {
            const code = isProduction ? totp('DFBHGFJK4KK') : '000000';
            const text = `Ваш код для входа: ${code}`;
            await this.update(user.id, { code: code });
            if (isProduction) {
                const url = `${this.SMSC_HOST}:${this.SMSC_PORT}/smsc?login=${this.SMSC_LOGIN}&password=${this.SMSC_PASSWORD}&to=${user.msisdn}&content=${text}&from=${this.SMSC_FROM}`;
                try {
                    await fetch(url);
                } catch (e) {
                    this.logger.error(e);
                    throw new HttpException(`Ошибка при отправке СМС`, 500);
                }
            }
        } else
            throw new HttpException(
                `Невозможно отправить СМС: не указан номер телефона`,
                500,
            );
    }
}
