import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersSeederService } from '../database/seeds/users.seeder.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './entities/user.entity';
import { BalanceLog } from './entities/balance-log.entity';
import { UserLog } from './entities/user-log.entity';
import { Bonus } from './entities/bonus.entity';
import { Percent } from './entities/percent.entity';
import { PercentsSeederService } from '../database/seeds/percents.seeder.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, BalanceLog, UserLog, Bonus, Percent]),
    ],
    controllers: [UsersController],
    providers: [UsersService, UsersSeederService, PercentsSeederService],
    exports: [UsersService, UsersSeederService, PercentsSeederService],
})
export class UsersModule {}
