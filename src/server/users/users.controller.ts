import {
    Controller,
    Get,
    Post,
    Body,
    Put,
    Param,
    Delete,
    UseGuards,
    Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { SessionAuthGuard } from '../guards/session-auth.guard';
import { SameUserGuard } from '../guards/same-user';
import { RoleGuard } from '../guards/role.guard';
import { Role } from './entities/role';

@Controller('api/users')
@UseGuards(SessionAuthGuard)
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Post()
    async create(@Body() createUserDto: CreateUserDto) {
        return await this.usersService.create(createUserDto);
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Get()
    findAll() {
        return this.usersService.findAll();
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Get('/active')
    findActive() {
        return this.usersService.findActive();
    }

    @Post('/filtered')
    findForFilter(@Body() filters: { roles: string[] }) {
        return this.usersService.findFiltered(filters);
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Get('/deleted')
    findDeleted() {
        return this.usersService.findDeleted();
    }

    @Get('/user')
    async user(@Request() req: any) {
        const user = await this.usersService.getUserInfo(req.user.id);
        return user;
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.usersService.findOne(+id);
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Post(':id/add_money')
    addMoney(@Param('id') id: string, @Body() body) {
        const { amount } = body;
        return this.usersService.addMoney(+id, amount);
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]) ||
            new SameUserGuard(),
    )
    @Put(':id')
    update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
        return this.usersService.update(+id, updateUserDto);
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.usersService.remove(+id);
    }

    @UseGuards(
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT, Role.PARTNER]),
    )
    @Post(':id/restore')
    restore(@Param('id') id: string) {
        return this.usersService.restore(+id);
    }

    @UseGuards(new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT]))
    @Get('terminals/active')
    findActiveTerminals() {
        this.usersService.findActiveTerminals();
    }

    @UseGuards(new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT]))
    @Get('terminals/deleted')
    findDeletedTerminals() {
        this.usersService.findDeletedTerminals();
    }
}
