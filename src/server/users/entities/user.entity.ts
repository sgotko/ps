import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    BeforeInsert,
    CreateDateColumn,
    AfterLoad,
    BeforeUpdate,
    DeleteDateColumn,
    OneToMany,
    ManyToOne,
    UpdateDateColumn,
    BaseEntity,
} from 'typeorm';

import * as crypto from 'crypto';
const algorithm = 'aes-192-cbc';
const secret = '448323cc1a4c0e898b928619774712630de435257ef30de6';

import * as money from 'money-math';

import { Payment } from 'src/server/statistics/entities/payment.entity';
import { Role } from './role';
import { TerminalPayment } from 'src/server/statistics/entities/terminal-payment.entity';
import { Percent } from './percent.entity';
import { Amount, defaultAmount } from 'src/server/ps/types';

@Entity('users')
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    login: string;

    @Column({
        select: false,
    })
    password: string;

    @Column()
    name: string;

    @Column({
        default: defaultAmount,
    })
    balance: Amount;

    @Column({
        default: defaultAmount,
    })
    percent: Amount;

    @OneToMany(() => Percent, (percent) => percent.user, { eager: false })
    percents: Array<Percent>;

    @Column({
        default: null,
    })
    msisdn: string;

    @Column({
        default: null,
    })
    code: string;

    @Column({ default: false })
    isAdmin: boolean;

    @Column({ type: 'enum', enum: Role, default: Role.USER })
    role: Role;

    @OneToMany(() => User, (user) => user.parent, { eager: false })
    children: Array<User>;

    @ManyToOne(() => User, (user) => user.children, {
        eager: false,
        nullable: true,
    })
    parent: User;

    @CreateDateColumn({
        precision: null,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @UpdateDateColumn({
        precision: null,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at?: Date;

    @BeforeInsert()
    @BeforeUpdate()
    async hashPassword() {
        if (this.password) this.password = await encrypt(this.password);
    }

    @Column({
        precision: null,
        type: 'datetime',
        nullable: true,
    })
    last_login?: Date;

    public fullName = '';

    public available = null;

    @AfterLoad()
    async afterLoad() {
        this.getFullName();
    }

    private getFullName() {
        if (this.parent) this.fullName = `${this.name} (${this.parent.name})`;
        else this.fullName = this.name;
    }

    public calcAmount(psName: string, amount: Amount) {
        return money.add(
            amount,
            money.percent(amount, this.getPercentByPaymentSystem(psName)),
        );
    }

    public getPercentByPaymentSystem(psName: string): Amount | undefined {
        const myPercent: Percent = this.percents.find((p) => p.ps === psName);
        return myPercent.percent;
    }

    public isAdministrator() {
        return this.role === Role.ADMINISTRATOR;
    }

    public isPartner() {
        return this.role === Role.PARTNER;
    }

    public isTerminal() {
        return this.role === Role.TERMINAL;
    }

    public isRegularUser() {
        return this.role === Role.USER;
    }

    public isAccauntant() {
        return this.role === Role.ACCOUNTANT;
    }

    public hasParent() {
        return this.parent !== null;
    }
}

export function encrypt(text: string): Promise<string> {
    return new Promise((resolve) => {
        // Key length is dependent on the algorithm. In this case for aes192, it is
        // 24 bytes (192 bits).
        const key = Buffer.from(secret, 'hex');
        const iv = crypto.randomBytes(16); // Initialization vector.

        const cipher = crypto.createCipheriv(algorithm, key, iv);

        let encrypted = '';
        cipher.on('readable', () => {
            let chunk: Buffer;
            while (null !== (chunk = cipher.read())) {
                encrypted += chunk.toString('hex');
            }
        });
        cipher.on('end', () => {
            resolve(iv.toString('hex') + ':' + encrypted);
        });

        cipher.write(text);
        cipher.end();
    });
}

export function decrypt(text: string): Promise<string> {
    // eslint-disable-next-line prefer-const
    let [iv, encrypted] = text.split(':') as [any, string];
    iv = Buffer.from(iv, 'hex');
    return new Promise((resolve) => {
        const decipher = crypto.createDecipheriv(
            algorithm,
            Buffer.from(secret, 'hex'),
            iv,
        );

        let decrypted = '';
        decipher.on('readable', (chunk: Buffer) => {
            while (null !== (chunk = decipher.read())) {
                decrypted += chunk.toString('utf8');
            }
        });
        decipher.on('end', () => {
            resolve(decrypted);
        });

        // Encrypted with same algorithm, key and iv.
        decipher.write(encrypted, 'hex');
        decipher.end();
    });
}

export function verify(text: string, encrypted: string): Promise<boolean> {
    return new Promise(async (resolve) => {
        const decrypted = await decrypt(encrypted);
        resolve(text === decrypted);
    });
}
