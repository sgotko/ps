import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

export enum UserLogType {
    Create = 1,
    Edit = 2,
    Delete = 3,
    Restore = 4,
}

@Entity()
export class UserLog {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @ManyToOne(() => User)
    admin: User;

    @ManyToOne(() => User)
    user: User;

    @Column()
    affected: string;

    @Column({
        type: 'enum',
        enum: UserLogType,
    })
    type: UserLogType;

    @CreateDateColumn({
        precision: null,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
