export enum Role {
    ADMINISTRATOR = 'АДМИНИСТРАТОР',
    PARTNER = 'ПАРТНЁР',
    USER = 'ПОЛЬЗОВАТЕЛЬ',
    ACCOUNTANT = 'БУХГАЛТЕР',
    TERMINAL = 'ТЕРМИНАЛ',
}
