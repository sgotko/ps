import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
    BaseEntity,
} from 'typeorm';
import { User } from './user.entity';

export enum LogType {
    Charge = 1,
    Edit = 2,
    Add = 3,
    Tax = 4,
    Bonus = 5,
}

@Entity()
export class BalanceLog extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @ManyToOne(() => User)
    admin: User;

    @ManyToOne(() => User)
    user: User;

    @Column()
    old_balance: string;

    @Column()
    new_balance: string;

    @Column()
    diff: string;

    @Column()
    type: LogType;

    @CreateDateColumn({
        precision: null,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
