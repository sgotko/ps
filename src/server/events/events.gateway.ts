import {
    WebSocketGateway,
    SubscribeMessage,
    MessageBody,
    WebSocketServer,
    ConnectedSocket,
} from '@nestjs/websockets';

import { Socket, Server } from 'socket.io';
import { Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';

@WebSocketGateway({ namespace: 'events' })
export class EventsGateway {
    @WebSocketServer()
    wss!: Server;

    private logger: Logger = new Logger('EventsGateway');

    afterInit(server: Server) {
        this.logger.log(`EventsGateway initialized`);
    }

    @SubscribeMessage('join')
    handleJoin(@MessageBody() data: string, @ConnectedSocket() client: Socket) {
        client.join(data);
        client.emit('joined', data);
    }

    @SubscribeMessage('leave')
    handleLeave(client: Socket, room: string) {
        client.leave(room);
        client.emit('leaved', room);
    }

    // * App event handlers

    @OnEvent('notification.created')
    handleNotificationsCreatedEvent(payload: any) {
        this.wss.to('notifications').emit('notification.created', payload);
    }

    @OnEvent('notification.updated')
    handleNotificationsUpdatedEvent(payload: any) {
        this.wss.to('notifications').emit('notification.updated', payload);
    }

    @OnEvent('notification.deleted')
    handleNotificationsDeletedEvent(payload: any) {
        this.wss.to('notifications').emit('notification.deleted', payload);
    }
}
