import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UsersModule } from './users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseModule } from './database/database.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { StatisticsModule } from './statistics/statistics.module';
import { PsModule } from './ps/ps.module';
import { NotificationsModule } from './notifications/notifications.modulte';
import { EventsModule } from './events/events.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        EventEmitterModule.forRoot(),
        TypeOrmModule.forRootAsync({
            useFactory: (configService: ConfigService) => ({
                type: 'mysql',
                host: configService.get('MYSQL_HOST'),
                port: +configService.get<number>('MYSQL_PORT'),
                username: configService.get('MYSQL_USER'),
                password: configService.get('MYSQL_PASSWORD'),
                database: configService.get('MYSQL_DATABASE'),
                entities: [__dirname + '/**/*.entity{.ts,.js}'],
                synchronize: true,
                migrationsTableName: 'migrations',
                autoLoadEntities: true,
                name: 'default',
                logging: false,
            }),
            inject: [ConfigService],
        }),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'public'),
            renderPath: '.html',
            serveStaticOptions: {
                cacheControl: false,
                setHeaders: (res, path) => {
                    if (path.endsWith('.js') || path.endsWith('.md')) {
                        res.setHeader('Cache-Control', 'no-cache');
                        res.setHeader('expires', '-1');
                    }
                },
            },
        }),
        PsModule,
        UsersModule,
        DatabaseModule,
        StatisticsModule,
        NotificationsModule,
        AuthModule,
        EventsModule,
    ],
    controllers: [AppController],
    providers: [],
})
export class AppModule {}
