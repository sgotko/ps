import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    ForbiddenException,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';

@Catch(ForbiddenException)
export class ForbiddenExceptionFilter implements ExceptionFilter {
    catch(exception: ForbiddenException, host: ArgumentsHost) {
        throw new HttpException('Доступ запрещён', HttpStatus.FORBIDDEN);
    }
}
