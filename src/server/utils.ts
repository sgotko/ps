export const wait = (timeout: number) =>
    new Promise((resolve) => setTimeout(resolve, timeout));

export const noop = () => {};
// export const noopAsync = ()
