import { noop } from 'rxjs';
import { wait } from './utils';

export type FallbackOptions = {
    maxRetry: number;
    startTimeout: number;
    cb: () => Promise<unknown>;
};

export const fallback = async (
    options: FallbackOptions = {
        maxRetry: 5,
        startTimeout: 50,
        cb: () =>
            new Promise((resolve) => {
                noop();
                resolve(void 0);
            }),
    },
): Promise<void> => {
    let currTimeout = options.startTimeout;
    let finished = false;
    while (options.maxRetry > 0 && !finished) {
        try {
            await options.cb();
            finished = true;
        } catch (e) {
            await wait(currTimeout);
            options.maxRetry -= 1;
            currTimeout *= 2;
        }
    }
    if (finished) {
        return Promise.resolve(void 0);
    } else {
        return Promise.reject();
    }
};
