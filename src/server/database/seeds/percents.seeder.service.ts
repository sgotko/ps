import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Percent } from 'src/server/users/entities/percent.entity';
import { User } from 'src/server/users/entities/user.entity';
import { Repository } from 'typeorm';
import { PaymentSystems } from 'src/server/ps/payment-system/index';
import { Amount, defaultAmount } from 'src/server/ps/types';

@Injectable()
export class PercentsSeederService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(Percent)
        private readonly percentRepository: Repository<Percent>,
    ) {}

    async run(): Promise<void> {
        const users: User[] = await this.userRepository.find();

        for (const user of users) {
            const percents: Percent[] = await this.percentRepository.findBy({
                user: {
                    id: user.id,
                },
            });
            let percent: Amount = defaultAmount;
            // If first launch after update
            if (percents.length === 0) {
                percent = user.percent as Amount;
            }
            if (user.percent !== defaultAmount) {
                user.percent = defaultAmount;
                user.save();
            }
            for (const ps of PaymentSystems) {
                if (percents.find((p) => p.ps === ps) === undefined) {
                    this.percentRepository
                        .create({
                            percent,
                            ps: ps,
                            user: user,
                        })
                        .save();
                }
            }
        }
    }
}
