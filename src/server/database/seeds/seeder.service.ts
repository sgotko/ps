import { Injectable, Logger } from '@nestjs/common';
import { UsersSeederService } from './users.seeder.service';
import { PercentsSeederService } from './percents.seeder.service';
import { Between, DataSource } from 'typeorm';
import { Payment } from 'src/server/statistics/entities/payment.entity';
import {
    BalanceLog,
    LogType,
} from 'src/server/users/entities/balance-log.entity';
import { User } from 'src/server/users/entities/user.entity';
import * as money from 'money-math';
import { TerminalPayment } from 'src/server/statistics/entities/terminal-payment.entity';

@Injectable()
export class SeederService {
    constructor(
        private readonly usersSeeder: UsersSeederService,
        private readonly percentsSeeder: PercentsSeederService,
        private readonly dataSource: DataSource,
    ) {}

    logger = new Logger('SeederService');

    async seed() {
        await this.users()
            .then(() => {
                this.logger.log('Seeding successfuly completed');
            })
            .catch((error) => {
                this.logger.error(`Error during seeding: ${error}`);
            });

        await this.percents();
    }

    async users() {
        return await Promise.all(this.usersSeeder.run())
            .then((createdUsers) => {
                return Promise.resolve(createdUsers);
            })
            .catch((error) => Promise.reject(error));
    }

    async percents() {
        return await this.percentsSeeder.run();
    }
}
