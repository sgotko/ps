import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../users/entities/user.entity';
import { Role } from '../../users/entities/role';
import { CreateUserDto } from 'src/server/users/dto/create-user.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class UsersSeederService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private configService: ConfigService,
    ) {}

    run(): Promise<User>[] {
        const usersToSeed: Array<CreateUserDto> = [
            {
                login: 'admin',
                password: 'admin',
                name: 'ADMINISTRATOR',
                isAdmin: true,
                role: Role.ADMINISTRATOR,
                balance: '100000.00',
                msisdn: this.configService.get<string>('ADMIN_MSISDN') || null,
            },
        ];
        return usersToSeed.map(async (user) => {
            return await this.userRepository
                .findOne({
                    where: { login: user.login },
                    select: ['id', 'login'],
                })
                .then(async (dbUser) => {
                    if (dbUser) {
                        return Promise.resolve(null);
                    }
                    const newUser = await this.userRepository.create(
                        user as any,
                    );
                    return Promise.resolve(
                        await this.userRepository.save(newUser),
                    );
                })
                .catch((error) => Promise.reject(error));
        });
    }
}
