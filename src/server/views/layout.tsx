import * as React from 'react';

const Layout = (props: any) => {
    return (
        <html lang="ru">
            <head>
                <meta charSet="UTF-8" />
                <title>web-ps</title>
            </head>
            <body>
                <div id="app"></div>
                <script src="/app.js"></script>
            </body>
        </html>
    );
};

export default Layout;
