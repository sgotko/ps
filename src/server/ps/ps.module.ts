import { Module } from '@nestjs/common';
import { PsService } from './ps.service';
import { PsController } from './ps.controller';
import { UsersModule } from '../users/users.module';
import { StatisticsModule } from '../statistics/statistics.module';

@Module({
    imports: [UsersModule, StatisticsModule],
    providers: [PsService],
    controllers: [PsController],
    exports: [PsService],
})
export class PsModule {}
