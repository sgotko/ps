import { PaymentSystem } from './types';
import { HttpException, Logger } from '@nestjs/common';
import * as iconv from 'iconv-lite';
import * as xml2js from 'xml2js';
import fetch from 'node-fetch';

const uniqueId = () => new Date().valueOf() % 100000;
const paymentDt = () =>
    new Date().toISOString().replace(/T/g, ' ').replace(/Z/g, '');

type PaymentRequestBody = {
    telematicaPaymentInfo: {
        paymentLogin: string;
        paymentPassword: string;
        paymentMSISDN: string;
        paymentAmount: number;
        paymentDt: string;
        transactionId: number;
        payerFullName: string;
        payerBirthDay: string;
        payerPassportNumber: string;
    };
};

export class MCS extends PaymentSystem {
    private logger = new Logger(MCS.name);

    private credentials() {
        return 'vertu2';
    }

    override async addPayment(
        account: string,
        amount: string,
        passport: string,
        name: string,
        birthDay: string,
    ): Promise<boolean> {
        const url =
            'https://pmt.mcs.ooo:48463/vertu2/KKM_PG_GATE/HTTP_ADD_PAYMENT';
        const body: PaymentRequestBody = {
            telematicaPaymentInfo: {
                transactionId: uniqueId(),
                paymentDt: paymentDt(),
                paymentMSISDN: account,
                paymentLogin: 'vertu2',
                paymentPassword: 'vertu2',
                paymentAmount: +amount,
                payerFullName: name,
                payerBirthDay: birthDay,
                payerPassportNumber: passport,
            },
        };
        let res = null;
        const builder = new xml2js.Builder();
        const bodyStr = builder.buildObject(body);
        try {
            res = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/xml',
                },
                body: bodyStr,
            });
            res = await res.buffer();
            console.log((res as Buffer).toString('utf-8'));
            res = iconv.decode(res, 'win1251', {
                defaultEncoding: 'utf-16le',
            });
            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });
            res = res.KKM_PG_GATE.ERROR.SQLERRM === 'OK';
            this.logger.log({ method: 'add', message: { res } });
            return res;
        } catch (e) {
            this.logger.error({
                method: 'add',
                message: { e, res },
            });
            throw new HttpException(e, 500);
        }
    }

    override async checkAccount(account: string): Promise<boolean> {
        const info = await this.getAccountInfo(account);
        return info && info.KKM_PG_GATE.ERROR.SQLERRM === 'OK';
    }

    override async getAccountInfo(account: any): Promise<any> {
        const url = `https://pmt.mcs.ooo:48463/${this.credentials()}/KKM_PG_GATE/CLIENT_INFO?P_MSISDN=${account}`;
        let res = null;
        try {
            res = await fetch(url);
            res = await res.buffer();
            res = iconv.decode(res, 'win1251', {
                defaultEncoding: 'utf-16le',
            });
            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });

            // res = res.res.KKM_PG_GATE.ERROR.SQLERRM === 'OK';
            this.logger.log({
                method: 'getAccountInfo',
                message: { account, res },
            });
        } catch (e) {
            this.logger.error({
                method: 'getAccountInfo',
                message: { account, res, e },
            });
            throw new HttpException(e, 500);
        }
        return res;
    }

    override async getBalance(): Promise<string> {
        const url = `https://pmt.mcs.ooo:48463/${this.credentials()}/KKM_PG_GATE/GET_BALANCE_INFO`;

        try {
            let res: any = await fetch(url);
            res = await res.buffer();
            res = iconv.decode(res, 'win1251', {
                defaultEncoding: 'utf-16le',
            });

            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });

            if (res?.KKM_PG_GATE?.BALANCE) {
                return res.KKM_PG_GATE.BALANCE;
            }
        } catch (e) {
            this.logger.error({
                method: 'balance',
                message: { e },
            });
            throw new Error(`Ошибка при проверке счёта платёжной системы`);
        }
    }
}
