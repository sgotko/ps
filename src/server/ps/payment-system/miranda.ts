import { HttpException, Logger } from '@nestjs/common';
import { PaymentSystem } from './types';
import { createHash } from 'node:crypto';
import { Amount } from '../types';
import * as moment from 'moment';
import * as xml2js from 'xml2js';
import fetch from 'node-fetch';
import { isProduction } from 'src/server/main';

type MirandaOptions = {
    terminalCode: string;
    terminalType: number;
    paymentType: number;
    acVps: number;
    contractCode: number;
};

type CanAcceptPayment = {
    ESPP_1204010: {
        f_05: number;
    };
};

type CantAcceptPayment = {
    ESPP_2204010: unknown;
};

type CanAcceptPaymentResponse = CanAcceptPayment | CantAcceptPayment;

const isCanAcceptPayment = (
    response: CanAcceptPaymentResponse,
): response is CanAcceptPayment =>
    typeof response === 'object' &&
    response !== null &&
    'ESPP_1204010' in response;

type PaymentAccepted = {
    ESPP_1204090: unknown;
};

type PaymentNotAccepted = {
    ESPP_2204090: unknown;
};

type AcceptPaymentResponse = PaymentAccepted | PaymentNotAccepted;

const isPaymentAccepted = (
    response: AcceptPaymentResponse,
): response is PaymentAccepted =>
    typeof response === 'object' &&
    response !== null &&
    'ESPP_1204090' in response;

export class Miranda extends PaymentSystem {
    private logger = new Logger(Miranda.name);

    private getOptions = (): MirandaOptions =>
        isProduction
            ? {
                  terminalCode: 'VERTU____MM.VIRTUAL_LNR_000001',
                  terminalType: 8,
                  paymentType: 8,
                  acVps: 1603141535,
                  contractCode: 9712300035,
              }
            : {
                  terminalCode: '00000000001.12836',
                  terminalType: 1,
                  paymentType: 1,
                  acVps: 1603141500,
                  contractCode: 9712300000,
              };

    public constructor() {
        super();
    }

    private url = 'https://pgw.miranda-media.ru:8443/service';

    private makeControl = (...fields: string[]): string => {
        const md5 = createHash('md5').update(fields.join('&')).digest('hex');
        const base64 = Buffer.from(md5).toString('base64');
        return base64;
    };

    private canAcceptPaymentRequestBody = (account: string, amount: Amount) => {
        return `<?xml version = "1.0" encoding = "UTF-8"?>
<ESPP_0104010 xmlns = "http://schema.mts.ru/ESPP/AgentPayments/Protocol/Messages/v5_02" xmlns:ESPP-constraints = "http://schema.mts.ru/ESPP/Core/Constraints/v5_02" xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation = "http://schema.mts.ru/ESPP/AgentPayments/Protocol/Messages/v5_02 ESPP_AgentPayments_Protocol_Messages_v5_02.xsd http://schema.mts.ru/ESPP/Core/Constraints/v5_02 ESPP_Core_Constraints_v5_02.xsd">
  <f_01 xsi:type = "ESPP-constraints:PHN_CODE_fmt_01">${account}</f_01>
  <f_02>${amount}</f_02>
  <f_03 xsi:type = "ESPP-constraints:CUR_fmt_01">810</f_03>
  <f_04>${this.getOptions().paymentType}</f_04>
  <f_05>${this.getOptions().terminalCode}</f_05>
  <f_06>${this.getOptions().terminalType}</f_06>
  <f_07>${this.getOptions().acVps}</f_07>
  <f_08>${this.getOptions().contractCode}</f_08>
  <f_10>miranda</f_10>
</ESPP_0104010>`;
    };

    private acceptPaymentRequestBody = (
        account: string,
        amount: Amount,
        transactionNumber: number,
    ) => {
        const now = moment();
        const uuid = now.unix();
        const date1 = now.format('YYYY-MM-DDTHH:mm:ss');
        const date2 = now.format();
        return `<?xml version = "1.0" encoding = "UTF-8"?>
<ESPP_0104090	xmlns = "http://schema.mts.ru/ESPP/AgentPayments/Protocol/Messages/v5_02"
	xmlns:ESPP-constraints = "http://schema.mts.ru/ESPP/Core/Constraints/v5_02"
	xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation = "http://schema.mts.ru/ESPP/AgentPayments/Protocol/Messages/v5_02 ESPP_AgentPayments_Protocol_Messages_v5_02.xsd http://schema.mts.ru/ESPP/Core/Constraints/v5_02 ESPP_Core_Constraints_v5_02.xsd">
	<f_01 xsi:type = "ESPP-constraints:PHN_CODE_fmt_01">${account}</f_01>
	<f_02>${amount}</f_02>
	<f_03 xsi:type = "ESPP-constraints:CUR_fmt_01">810</f_03>
	<f_04>${this.getOptions().paymentType}</f_04>
	<f_06>${uuid}</f_06>
	<f_07>${uuid}</f_07>
	<f_08>${date1}</f_08>
	<f_10>${transactionNumber}</f_10>
	<f_11>${this.getOptions().acVps}</f_11>
	<f_12>${this.getOptions().terminalCode}</f_12>
	<f_13>${this.getOptions().terminalType}</f_13>	
	<f_14>99</f_14>	
	<f_16>${date2}</f_16>
	<f_18>${this.makeControl(
        account,
        amount,
        '810',
        `${this.getOptions().paymentType}`,
        '',
        `${uuid}`,
        date1,
        `${this.getOptions().acVps}`,
        `${this.getOptions().terminalCode}`,
        `${this.getOptions().terminalType}`,
        date2,
        '',
        'miranda',
    )}</f_18>
	<f_19>${this.getOptions().contractCode}</f_19>
	<f_21>miranda</f_21>
	<f_22>${account}</f_22>
</ESPP_0104090>`;
    };

    public override getBalance(): Promise<string> {
        return Promise.resolve('Безлимит');
    }

    public override checkAccount(): Promise<boolean> {
        return Promise.resolve(true);
    }

    private canAcceptPaymentRequest = async (
        account: string,
        amount: Amount,
    ): Promise<CanAcceptPaymentResponse> => {
        let res: CanAcceptPaymentResponse | null = null;
        try {
            const body = this.canAcceptPaymentRequestBody(account, amount);
            this.logger.log(body);
            const response = await fetch(this.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'text/xml',
                },
                body,
            });
            this.logger.log(response);
            const buffer = await response.buffer();
            this.logger.log(
                `Response from Miranda: ${buffer.toString('utf-8')}`,
            );
            res = await xml2js.parseStringPromise(buffer, {
                explicitArray: false,
                mergeAttrs: true,
            });
            this.logger.log({
                method: 'canAcceptPaymentRequest',
                message: res,
            });
            return res;
        } catch (e: unknown) {
            this.logger.error({
                method: 'canAcceptPaymentRequest',
                message: e,
            });
            throw new HttpException(
                {
                    message:
                        'Ошибка про проверке возможности проведения платежа',
                },
                500,
            );
        }
    };

    private acceptPaymentRequest = async (
        account: string,
        amount: Amount,
        transaction: number,
    ): Promise<AcceptPaymentResponse> => {
        let res: AcceptPaymentResponse | null = null;
        try {
            const body = this.acceptPaymentRequestBody(
                account,
                amount,
                transaction,
            );
            this.logger.log(body);
            const response = await fetch(this.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'text/xml',
                },
                body,
            });
            const buffer = await response.buffer();
            this.logger.log(
                `Response from Miranda: ${buffer.toString('utf-8')}`,
            );
            res = await xml2js.parseStringPromise(buffer, {
                explicitArray: false,
                mergeAttrs: true,
            });
            this.logger.log({
                method: 'acceptPaymentRequest',
                message: res,
            });
            return res;
        } catch (e: unknown) {
            this.logger.error({
                method: 'acceptPaymentRequest',
                message: e,
            });
            throw new HttpException(
                {
                    message: 'Ошибка про проведении платежа',
                },
                500,
            );
        }
    };

    public override async addPayment(
        account: string,
        amount: Amount,
    ): Promise<boolean> {
        const canAcceptResponse: CanAcceptPaymentResponse =
            await this.canAcceptPaymentRequest(account, amount);
        if (!isCanAcceptPayment(canAcceptResponse)) {
            return false;
        }
        const acceptPaymentResponse = await this.acceptPaymentRequest(
            account,
            amount,
            +canAcceptResponse.ESPP_1204010.f_05,
        );

        return isPaymentAccepted(acceptPaymentResponse);
    }
}
