import { HttpException, Logger } from '@nestjs/common';
import moment from 'moment';
import { isProduction } from 'src/server/main';
import { PaymentSystem } from './types';
import * as xml2js from 'xml2js';
import fetch from 'node-fetch';

export type PlatezhkaOperator = {
    id: number;
    name: string;
    checkInfo: string;
    comission: {
        min: number;
        max: number;
        percent: number;
        summa: number;
    };
    template: {
        name: string;
        template: string;
        regexp: string;
    };
};

export class Platezhka extends PaymentSystem {
    private readonly logger = new Logger(Platezhka.name);

    private readonly host: string = 'http://10.100.0.118';

    constructor() {
        super();
    }

    private terminal(): number {
        return isProduction ? 12974 : 12978;
    }

    public async getOperators(): Promise<any[]> {
        try {
            let res: any = await fetch(
                `${this.host}/gate/PaymentGateService.asmx`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'text/xml',
                        SOAPAction: 'http://tempuri.org/GetOperators',
                    },
                    body: `<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                        <GetOperators xmlns="http://tempuri.org/">
                        <type>1</type>
                        <terminal>${this.terminal()}</terminal>
                        </GetOperators>
                    </soap:Body>
                    </soap:Envelope>`,
                },
            );
            res = await res.buffer();

            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });

            if (
                this.isSuccessResponse(
                    res['soap:Envelope']['soap:Body']['GetOperatorsResponse'][
                        'GetOperatorsResult'
                    ]['Ret_Code'],
                )
            ) {
                return res['soap:Envelope']['soap:Body'][
                    'GetOperatorsResponse'
                ]['GetOperatorsResult']['Operators']['Operator'].map(
                    this.makeOperator.bind(this),
                );
            }
        } catch (e) {
            this.logger.error({
                method: 'getOperators',
                message: e,
            });
            throw new HttpException(
                {
                    message: 'Ошибка при получении списка операторов',
                },
                500,
            );
        }
    }

    override async addPayment(
        account: string,
        amount: string,
        operator: number,
    ): Promise<boolean> {
        const payment_code = moment().unix();
        const date_pay = moment().format('YYYY-MM-DD HH:mm:ss');
        const summa_pay = amount.replace('.', ',');

        try {
            let res: any = await fetch(
                `${this.host}/gate/PaymentGateService.asmx`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'text/xml',
                        SOAPAction: 'http://tempuri.org/SendPayment',
                    },
                    body: `<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <SendPayment xmlns="http://tempuri.org/">
                        <type>1</type>
                        <terminal>${this.terminal()}</terminal>
                        <operator_code>${+operator}</operator_code>
                        <number>${account}</number>
                        <payment_code>${payment_code}</payment_code>
                        <check_code>${payment_code}</check_code>
                        <date_pay>${date_pay}</date_pay>
                        <summa_pay>${summa_pay}</summa_pay>
                        <summa_rate>0,00</summa_rate>
                        <extra_param />
                        <hash>${this.credentials()}</hash>
                    </SendPayment>
                </soap:Body>
                </soap:Envelope>`,
                },
            );

            res = await res.buffer();

            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });

            if (
                this.isSuccessResponse(
                    res['soap:Envelope']['soap:Body']['SendPaymentResponse'][
                        'SendPaymentResult'
                    ]['Ret_Code'],
                )
            )
                return true;
            return false;
        } catch (e) {
            this.logger.error({
                method: 'add',
                message: { e },
            });
            throw new HttpException(e, 500);
        }
    }

    override async getAccountInfo(
        account: string,
        operator: number,
    ): Promise<any> {
        return await this.checkAccount(account, operator);
    }

    override async checkAccount(
        account: string,
        operator: number,
    ): Promise<boolean> {
        try {
            let res: any = await fetch(
                `${this.host}/gate/PaymentGateService.asmx`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'text/xml',
                        SOAPAction: 'http://tempuri.org/CheckNumber',
                    },
                    body: `<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                        <CheckNumber xmlns="http://tempuri.org/">
							<type>1</type>
							<terminal>${this.terminal()}</terminal>
							<operator_code>${+operator}</operator_code>
							<number>${account}</number>
							<extra_param />
							<hash>${this.credentials()}</hash>
                        </CheckNumber>
                    </soap:Body>
                    </soap:Envelope>`,
                },
            );
            res = await res.buffer();

            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });
            if (
                this.isSuccessResponse(
                    res['soap:Envelope']['soap:Body']['CheckNumberResponse'][
                        'CheckNumberResult'
                    ]['Ret_Code'],
                )
            ) {
                return true;
            }
            return false;
        } catch (e) {
            this.logger.error({
                method: 'checkAccount',
                message: { e },
            });
            throw new HttpException(
                { message: 'Ошибка при проверке номера' },
                500,
            );
        }
    }

    override async getBalance(): Promise<string> {
        throw new Error('Безлимит');
    }

    private credentials(): string {
        return isProduction
            ? 'K449ke32TKSn8GwBUr8ALHUDvkVrtG'
            : '6emT4RdagHMmZp4SoXZqrDf8SWkZHx';
    }

    private isSuccessResponse(code: string): boolean {
        return +code === 0;
    }

    private makeRegexp(template: string): any {
        return template
            .replace(/\w/g, '')
            .replace(/-/g, '')
            .replace(/\$/g, '0')
            .replace(/\*/g, '[*]'.repeat(30));
    }

    private makeOperator(xml: object): PlatezhkaOperator {
        return {
            id: +xml['OperatorId'],
            name: xml['OperatorName'],
            checkInfo: xml['OperatorCheckInfo'],
            comission: {
                min: xml['Comission']['Min'],
                max: xml['Comission']['Max'],
                percent: xml['Comission']['Percent'],
                summa: xml['Comission']['Summa'],
            },
            template: {
                name: xml['Templates']['Template']['EnterName'],
                template: xml['Templates']['Template']['EnterTemplate'],
                regexp: this.makeRegexp(
                    xml['Templates']['Template']['EnterTemplate'],
                ),
            },
        };
    }
}
