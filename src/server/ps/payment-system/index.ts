import { LTK } from './ltk';
import { Luganet } from './luganet';
import { MCS } from './mcs';
import { Miranda } from './miranda';
import { Platezhka } from './plateshka';
import { RCK } from './rck';
import { PaymentSystem } from './types';

export const isPaymentSystems = (item: any): item is typeof PaymentSystems =>
    item in PaymentSystems;

export const translate: Map<string, string> = new Map<string, string>([
    [MCS.name, 'ООО «МКС»'],
    [LTK.name, 'OOO «ЛТК»'],
    [RCK.name, 'ГУП ЛНР «РЦК»'],
    [Luganet.name, '«Луганет»'],
    [Platezhka.name, 'Платёжка'],
    [Miranda.name, 'Миранда'],
]);

export const PaymentSystems = [
    MCS.name,
    LTK.name,
    RCK.name,
    Luganet.name,
    Platezhka.name,
    Miranda.name,
] as const;

export default new Map<string, PaymentSystem>([
    [MCS.name, new MCS()], // type 'MCS'
    [LTK.name, new LTK()], // type 'LTK'
    [RCK.name, new RCK()], // type 'RCK'
    // [Luganet.name, new Luganet()], // type 'Luganet'
    [Platezhka.name, new Platezhka()], //type 'Platezhka'
    [Miranda.name, new Miranda()], // type 'Miranda'
]);
