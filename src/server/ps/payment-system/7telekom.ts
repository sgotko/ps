import { Logger } from '@nestjs/common';
import { PaymentSystem } from './types';
import * as fetch from 'node-fetch';

export class Telekom7 extends PaymentSystem {
    private logger = new Logger(Telekom7.name);

    private credentials() {
        return '';
    }

    public addPayment(...args: any[]): Promise<boolean> {
        return Promise.resolve(false);
    }

    public checkAccount(...args: any[]): Promise<boolean> {
        return Promise.resolve(false);
    }

    public getBalance(): Promise<string> {
        return Promise.resolve('Безлимит');
    }
}
