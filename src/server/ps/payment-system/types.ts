import { Logger } from '@nestjs/common';

export interface IPaymentSystem {
    addPayment(account: string, amount: string): Promise<boolean>;

    checkAccount(account: string): Promise<boolean>;

    getAccountInfo(account: string): Promise<any>;

    getBalance(): Promise<string>;
}

export interface IHasOperators {
    getOperators(): Promise<any[]>;
}

export abstract class PaymentSystem implements IPaymentSystem, IHasOperators {
    addPayment(...args): Promise<boolean> {
        Logger.debug(args);
        throw new Error('Method not implemented.');
    }
    checkAccount(...args): Promise<boolean> {
        Logger.debug(args);
        throw new Error('Method not implemented.');
    }
    getAccountInfo(...args): Promise<any> {
        Logger.debug(args);
        throw new Error('Method not implemented.');
    }
    getBalance(): Promise<string> {
        throw new Error('Method not implemented.');
    }

    getOperators(): Promise<unknown[]> {
        return Promise.resolve([]);
    }
}

export const isHasOperators = (ps: any): ps is typeof isHasOperators =>
    'getOperators' in ps;
