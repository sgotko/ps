import { PaymentSystem } from './types';
import { Logger, HttpException } from '@nestjs/common';
import * as sha1 from 'node-sha1';
import { uuidv4 } from 'uuidv7';
import fetch from 'node-fetch';

export class LTK extends PaymentSystem {
    private logger = new Logger(LTK.name);

    private credentials() {
        return '81efe501-f5d4-4b42-81fd-b3b9f79bead5';
    }

    override async addPayment(
        account: string,
        amount: string,
    ): Promise<boolean> {
        const formData = new FormData();
        formData.append('account', account);
        formData.append('summa', amount);
        formData.append('acpt_id', uuidv4());
        formData.append('sign', sha1(account + amount + this.credentials()));
        try {
            const res: any = await (
                await fetch(`https://api.lugtel.com/api/user/payment`, {
                    method: 'POST',
                    body: formData as any,
                })
            ).json();
            this.logger.log({ method: 'add', message: { res } });
            return parseInt(res.code) === 0;
        } catch (e) {
            this.logger.error({ method: 'add', message: { e } });
            throw new HttpException(e, 500);
        }
    }

    override async checkAccount(account: any): Promise<boolean> {
        let info = null;
        try {
            info = await this.getAccountInfo(account);
            this.logger.log({
                method: 'check',
                message: { info },
            });
        } catch (e) {
            this.logger.error({
                method: 'check',
                message: { account, info, e },
            });
        }

        return info && parseInt(info.code) === 0;
    }

    override async getAccountInfo(account: string): Promise<any> {
        let res = null;
        const sign = sha1(account + this.credentials());
        try {
            res = await (
                await fetch(
                    `https://api.lugtel.com/api/user/${account}/${sign}`,
                )
            ).json();
            this.logger.log({
                method: 'getAccountInfo',
                message: { account, res },
            });
            if (parseInt(res.code) !== 0) return null;
        } catch (e) {
            this.logger.error({
                method: 'getAccountInfo',
                message: { account, res, e },
            });
        }
        return res;
    }

    override async getBalance(): Promise<string> {
        return 'Безлимит';
    }
}
