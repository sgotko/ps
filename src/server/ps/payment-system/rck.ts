import { HttpException, Logger } from '@nestjs/common';
import { PaymentSystem } from './types';
import FormData = require('form-data');
import fetch from 'node-fetch';

export class RCK extends PaymentSystem {
    private logger = new Logger(RCK.name);

    private credentials(): string {
        return 'pL1nmTr4uyEYq1p59dJ7ChIvP1sp6a8X';
    }

    override async addPayment(account: any, amount: string): Promise<boolean> {
        try {
            const formData = new FormData();
            formData.append('command', 'pay');
            formData.append('uid', account);
            formData.append('key', this.credentials());
            formData.append('sum', amount);
            formData.append('describe', new Date().getTime());
            const res: any = await (
                await fetch(`https://pay.rck.su/check_pay.cgi`, {
                    method: 'POST',
                    body: formData,
                })
            ).json();
            this.logger.log({ method: 'add', message: { res } });
            return parseInt(res.errorNum) === 0;
        } catch (e) {
            this.logger.error({ method: 'add', message: { e } });
            throw new HttpException(e, 500);
        }
    }

    override async checkAccount(account: any): Promise<boolean> {
        let info = null;
        try {
            info = await this.getAccountInfo(account);
            return parseInt(info.errorNum) === 0;
        } catch (e) {
            this.logger.error({
                method: 'check',
                message: {
                    account,
                    info,
                },
            });
        }
        return false;
    }

    override async getAccountInfo(account: string): Promise<any> {
        let res = null;
        try {
            const formData = new FormData();
            formData.append('command', 'check');
            formData.append('uid', `${account}`);
            formData.append('key', this.credentials());

            res = await (
                await fetch(`https://pay.rck.su/check_pay.cgi`, {
                    method: 'POST',
                    body: formData,
                })
            ).json();
            this.logger.log({
                method: 'getAccountInfo',
                message: { account, res },
            });
            if (parseInt(res.errorNum) !== 0) return null;
        } catch (e) {
            this.logger.error({
                method: 'getAccountInfo',
                message: {
                    account,
                    res,
                    e,
                },
            });
        }
        return res;
    }

    override async getBalance(): Promise<string> {
        return 'Безлимит';
    }
}
