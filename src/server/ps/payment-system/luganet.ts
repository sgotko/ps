import { HttpException, Logger } from '@nestjs/common';
import { isProduction } from 'src/server/main';
import { uuidv4 } from 'uuidv7';
import { PaymentSystem } from './types';
import * as xml2js from 'xml2js';
import fetch from 'node-fetch';

export class Luganet extends PaymentSystem {
    private logger = new Logger(Luganet.name);

    private credentials(): string {
        return Buffer.from(
            `${isProduction ? '' : 'verta'}:${
                isProduction ? '' : 'LMtzmO9lXyadofKq'
            }`,
            'utf-8',
        ).toString('base64');
    }

    async add(account: any, amount: string): Promise<boolean> {
        const url = `http://tpay.luganet.ru/luga-xml/main.php?action=bill_input&bill_identifier=${account}&sum=${amount}&date=${new Date().toISOString()}&pkey=${uuidv4()}`;
        let res = null;
        try {
            res = await fetch(url, {
                headers: {
                    Authorization: `Basic ${this.credentials()}`,
                },
            });
            res = await res.buffer();
            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });
            this.logger.log({ method: 'add', message: { res } });
            return res.ResponseDebt?.errorResponse ? false : true;
        } catch (e) {
            this.logger.error({
                method: 'add',
                message: { e, res },
            });
            throw new HttpException(e, 500);
        }
    }

    async check(account: any): Promise<boolean> {
        let info = null;
        try {
            info = await this.getAccountInfo(account);
            return true;
        } catch (e) {
            this.logger.error({
                method: 'check',
                messge: {
                    account,
                    info,
                },
            });
        }
        return false;
    }

    async getAccountInfo(account: any): Promise<any> {
        const url = `http://tpay.luganet.ru/luga-xml/main.php?action=bill_search&bill_identifier=${account}`;
        let res = null;
        try {
            res = await fetch(url, {
                headers: {
                    Authorization: `Basic ${this.credentials()}`,
                },
            });
            res = await res.buffer();
            res = await xml2js.parseStringPromise(res, {
                explicitArray: false,
                mergeAttrs: true,
            });
            this.logger.log({
                method: 'getAccountInfo',
                message: { account, res },
            });

            if (res.ResponseDebt.errorResponse)
                throw new Error('Ошибка при проверке аккаунта');
        } catch (e) {
            this.logger.error({
                method: 'getAccountInfo',
                message: { account, res, e },
            });
            throw new HttpException(e, 500);
        }
        return res;
    }

    async balance(): Promise<string> {
        return 'Безлимит';
    }
}
