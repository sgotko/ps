import {
    Controller,
    Get,
    UseGuards,
    Request,
    Query,
    Post,
    Req,
    UseInterceptors,
    Body,
    UsePipes,
} from '@nestjs/common';

import { RoleGuard } from '../guards/role.guard';
import { SessionAuthGuard } from '../guards/session-auth.guard';

import { JoiValidationPipe } from '../pipes/joi-validation.pipe';
import { Role } from '../users/entities/role';
import { PsService } from './ps.service';

import { BasicAuthGuard } from '../guards/basic-auth.guard';
import { ApiFormatInterceptor } from '../interceptors/api-format.interceptor';
import * as Joi from 'joi';
import { CreateTerminalPaymentDto } from '../statistics/dto/create-terminal-payment.dto';
import { Amount } from './types';
import { PaymentSystems } from './payment-system/index';
import { Platezhka } from './payment-system/plateshka';
import { MCS } from './payment-system/mcs';

export type AddPaymentDto = {
    account: string; // any account id
    amount: Amount; // amount
    type: string; // type of PaymentSystem
    operator?: number; // optional operator param
    passport?: string; // passport param for MCS
    fullName?: string;
    birthDate?: string;
};

export type AddApiPaymentDto = Pick<
    CreateTerminalPaymentDto,
    'tr_number' | 'tr_time' | 'transaction'
> &
    Pick<
        AddPaymentDto,
        | 'account'
        | 'amount'
        | 'type'
        | 'operator'
        | 'passport'
        | 'birthDate'
        | 'fullName'
    >;

const AddPaymentJoiSchema = Joi.object<AddPaymentDto>({
    account: Joi.when('type', {
        is: Platezhka.name,
        then: Joi.string()
            .regex(/^\d{10}$/)
            .required(),
        otherwise: Joi.string().required(),
    }),
    amount: Joi.string()
        .pattern(new RegExp(/^\d+\.\d{2}$/), {
            name: 'Amount',
        })
        .required(),
    operator: Joi.number().when('type', {
        is: Platezhka.name,
        then: Joi.required(),
        otherwise: Joi.optional(),
    }),
    type: Joi.string().required(),
    passport: Joi.when('type', {
        is: MCS.name,
        then: Joi.string()
            .regex(/^\d{10}$/)
            .required(),
        otherwise: Joi.string(),
    }),
    fullName: Joi.when('type', {
        is: MCS.name,
        then: Joi.string().required(),
        otherwise: Joi.string(),
    }),
    birthDate: Joi.when('type', {
        is: MCS.name,
        then: Joi.string().required(),
        otherwise: Joi.string(),
    }),
});

const AddPaymenApiJoiSchema = Joi.object<AddApiPaymentDto & { format: string }>(
    {
        account: Joi.when('type', {
            is: Platezhka.name,
            then: Joi.string()
                .regex(/^\d{10}$/)
                .required(),
            otherwise: Joi.string().required(),
        }),
        amount: Joi.string()
            .pattern(new RegExp(/^\d+\.\d{2}$/), {
                name: 'Amount',
            })
            .required(),
        type: Joi.string().required(),
        tr_number: Joi.string(),
        tr_time: Joi.string(),
        transaction: Joi.string(),
        operator: Joi.number().when('type', {
            is: Platezhka.name,
            then: Joi.required(),
            otherwise: Joi.optional(),
        }),
        passport: Joi.when('type', {
            is: MCS.name,
            then: Joi.string()
                .regex(/^\d{10}$/)
                .required(),
            otherwise: Joi.string(),
        }),
        format: Joi.string().optional().valid('json', 'xml'),
    },
);

export type CheckAccountDto = {
    account: any;
    type: string;
    operator?: number;
};

const CheckAccountJoiSchema = Joi.object<CheckAccountDto & { format: string }>({
    account: Joi.when('type', {
        is: Platezhka.name,
        then: Joi.string()
            .regex(/^\d{10}$/)
            .required(),
        otherwise: Joi.string().required(),
    }),
    type: Joi.string().required(),
    operator: Joi.number().when('type', {
        is: Platezhka.name,
        then: Joi.required(),
        otherwise: Joi.optional(),
    }),
    format: Joi.string().optional().valid('json', 'xml'),
});

export type GetPaymentsDto = {
    type: string;
    account: string;
    startDate: Date;
    endDate: Date;
};

const GetPaymentsJoiSchema = Joi.object<GetPaymentsDto & { format: string }>({
    type: Joi.string()
        .required()
        .valid(...PaymentSystems),
    startDate: Joi.date().iso().required(),
    endDate: Joi.date().iso().greater(Joi.ref('startDate')).required(),
    format: Joi.string().optional().valid('json', 'xml'),
});

export type GetOperatorsDto = {
    type: string;
};

const GetOperatorsJoiSchema = Joi.object<GetOperatorsDto>({
    type: Joi.string()
        .required()
        .valid(...PaymentSystems),
});

const GetOperatorsApiJoiSchema = Joi.object<
    GetPaymentsDto & { format: string }
>({
    type: Joi.string()
        .required()
        .valid(...PaymentSystems),
    format: Joi.string().optional().valid('json', 'xml'),
});

@Controller('api/ps')
export class PsController {
    constructor(private readonly psService: PsService) {}

    @UseGuards(SessionAuthGuard, new RoleGuard([Role.USER]))
    @Post('add_payment')
    @UsePipes(new JoiValidationPipe(AddPaymentJoiSchema))
    addPayment(@Body() addPaymentDto: AddPaymentDto, @Request() req) {
        return this.psService.addPayment(+req.user.id, addPaymentDto);
    }

    @UseGuards(
        SessionAuthGuard,
        new RoleGuard([Role.ADMINISTRATOR, Role.ACCOUNTANT]),
    )
    @Get('balance')
    async getPaymentSystemsBalance() {
        return await this.psService.getBalance();
    }

    @UseGuards(SessionAuthGuard, new RoleGuard([Role.USER]))
    @Post('get_account_info')
    @UsePipes(new JoiValidationPipe(CheckAccountJoiSchema))
    checkAccount(@Body() checkAccountDto: CheckAccountDto) {
        return this.psService.getAccountInfo(checkAccountDto);
    }

    @UseGuards(SessionAuthGuard, new RoleGuard([Role.USER]))
    @UsePipes(new JoiValidationPipe(GetOperatorsJoiSchema))
    @Post('get_operators')
    async getOperators(@Body() getOperatorsDto: GetOperatorsDto) {
        return this.psService.getOperators(getOperatorsDto);
    }

    // Terminals API methods with Http Basic Auth
    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UsePipes(new JoiValidationPipe(CheckAccountJoiSchema))
    @UseInterceptors(ApiFormatInterceptor)
    @Get('check')
    async apiCheckAccountGet(@Query() query: CheckAccountDto) {
        return await this.psService.checkAccountApi(query);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UsePipes(new JoiValidationPipe(CheckAccountJoiSchema))
    @UseInterceptors(ApiFormatInterceptor)
    @Post('check')
    async apiCheckAccountPost(@Query() query: CheckAccountDto) {
        return await this.psService.checkAccountApi(query);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(AddPaymenApiJoiSchema))
    @Get('payment')
    async apiAddPaymentGet(@Query() query: AddApiPaymentDto, @Req() req) {
        return this.psService.addPaymentApi(+req.user.id, query);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(AddPaymenApiJoiSchema))
    @Post('payment')
    async apiAddPaymentPost(@Body() body: AddApiPaymentDto, @Req() req) {
        return this.psService.addPaymentApi(+req.user.id, body);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(GetPaymentsJoiSchema))
    @Get('payments')
    async apiPaymentsGet(@Query() query: GetPaymentsDto, @Req() req) {
        return this.psService.getPaymentsApi(+req.user.id, query);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(GetPaymentsJoiSchema))
    @Post('payments')
    async apiPaymentsPost(@Body() body: GetPaymentsDto, @Req() req) {
        return this.psService.getPaymentsApi(+req.user.id, body);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(GetOperatorsApiJoiSchema))
    @Get('payment_systems')
    async apiGetPaymentSystemsGet() {
        return this.psService.getPaymentSystems();
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(GetOperatorsApiJoiSchema))
    @Get('payment_systems')
    async apiGetPaymentSystemsPost() {
        return this.psService.getPaymentSystems();
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(GetOperatorsApiJoiSchema))
    @Get('operators')
    async apiGetOperatorsGet(@Query() getOperatorsDto: GetOperatorsDto) {
        return this.psService.getOperators(getOperatorsDto);
    }

    @UseGuards(BasicAuthGuard, new RoleGuard([Role.TERMINAL]))
    @UseInterceptors(ApiFormatInterceptor)
    @UsePipes(new JoiValidationPipe(GetOperatorsApiJoiSchema))
    @Post('operators')
    async apiGetOperatorsPost(@Body() getOperatorsDto: GetOperatorsDto) {
        return this.psService.getOperators(getOperatorsDto);
    }
}
