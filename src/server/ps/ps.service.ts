'use strict';

import {
    HttpException,
    Inject,
    Injectable,
    Logger,
    Scope,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

import { Between } from 'typeorm';

import { UsersService } from '../users/users.service';

import * as money from 'money-math';
import { User } from '../users/entities/user.entity';
import PaymentSystems, { translate } from './payment-system/index';
import { StatisticsService } from '../statistics/statistics.service';
import {
    AddApiPaymentDto,
    AddPaymentDto,
    CheckAccountDto,
    GetOperatorsDto,
    GetPaymentsDto,
} from './ps.controller';

import { isProduction } from '../main';
import paymentSystems from './payment-system/index';
import { Amount, ChargeResult } from './types';
import { DataSource, DeepPartial, QueryRunner } from 'typeorm';
import { CreatePaymentDto } from '../statistics/dto/create-payment.dto';
import { Payment } from '../statistics/entities/payment.entity';
import { fallback } from '../jitterFallback';
import { TerminalPayment } from '../statistics/entities/terminal-payment.entity';
import { CreateTerminalPaymentDto } from '../statistics/dto/create-terminal-payment.dto';
import { BalanceLog, LogType } from '../users/entities/balance-log.entity';
import moment from 'moment';
import { Platezhka } from './payment-system/plateshka';
import { PaymentSystem } from './payment-system/types';
import { MCS } from './payment-system/mcs';

type PaymentLogType = 'USER' | 'API';

@Injectable({ scope: Scope.REQUEST })
export class PsService {
    private readonly logger = new Logger(PsService.name);

    private user: User;

    constructor(
        @Inject(REQUEST) private readonly request: Request,
        private readonly usersService: UsersService,
        private readonly statisticsService: StatisticsService,
        private readonly dataSource: DataSource,
    ) {
        this.user = this.request.user as User;
    }

    async addPayment(userId: number, dto: AddPaymentDto) {
        const user = await this.usersService.findOne(userId);
        const ps = PaymentSystems.get(dto.type);
        const { parent } = user;

        if (!ps)
            throw new HttpException(
                `Неверно указана платежная система: ${dto.type}`,
                500,
            );
        if (
            isProduction &&
            !(await ps?.checkAccount(dto.account, dto.operator))
        ) {
            throw new HttpException('Ошибка при проверке аккаунта', 500);
        }

        if (
            user.hasParent() &&
            money.cmp(parent.calcAmount(dto.type, dto.amount), parent.balance) >
                0
        ) {
            throw new HttpException(
                'У вашего партнёра недостаточно средств для пополнения',
                500,
            );
        }

        if (
            money.cmp(user.calcAmount(dto.type, dto.amount), user.balance) <= 0
        ) {
            try {
                // const success =
                //     isProduction || dto.type === 'Miranda'
                //         ? await processPayment(ps, dto)
                //         : true;
                const success = await processPayment(ps, dto);
                if (success) {
                    await fallback({
                        cb: async () =>
                            await this.updateBalances(user, dto, 'USER'),
                        maxRetry: 5,
                        startTimeout: 30,
                    });

                    return await this.usersService.findOne(userId);
                } else {
                    throw new HttpException(
                        `Ошибка при пополнении счёта: ${dto.account}`,
                        500,
                    );
                }
            } catch (e) {
                this.logger.error({ message: e });
                throw new HttpException(
                    `Ошибка при пополнении счёта: ${dto.account}`,
                    500,
                );
            }
        } else {
            throw new HttpException(
                'Недостаточно средств для пополнения счёта',
                500,
            );
        }
    }

    async getAccountInfo(checkAccountDto: CheckAccountDto) {
        const ps = PaymentSystems.get(checkAccountDto.type);
        let res = false;
        try {
            res = await ps?.getAccountInfo(
                checkAccountDto.account,
                checkAccountDto.operator,
            );
        } catch (e) {
            this.logger.error(e);
            throw new HttpException('Ошибка при проверка аккаунта', 500);
        }
        if (!res) throw new HttpException('Аккаунт не найден', 500);
        return res;
    }

    async getOperators(dto: GetOperatorsDto) {
        const ps = PaymentSystems.get(dto.type);
        let res: unknown[] = [];
        try {
            res = await ps.getOperators();
        } catch (e) {
            this.logger.error(e);
            throw new HttpException(
                'Ошибка при получении списка операторов',
                500,
            );
        }
        return res;
    }

    // API for terminals
    async checkAccountApi(dto: CheckAccountDto) {
        try {
            const exists = await PaymentSystems.get(dto.type)?.checkAccount(
                dto.account,
                dto.operator,
            );
            if (exists)
                return {
                    response: {
                        status: 'OK',
                        message: 'OK',
                        dto: dto,
                    },
                };
            return new HttpException(
                {
                    response: {
                        status: 'ERROR',
                        message: 'WRONG ACCOUNT',
                        dto: dto,
                    },
                },
                400,
            );
        } catch (e) {
            throw new HttpException(
                {
                    response: {
                        status: 'ERROR',
                        message: 'TIMEOUT',
                        dto: dto,
                    },
                },
                500,
            );
        }
    }

    async addPaymentApi(userId: number, dto: AddApiPaymentDto) {
        const user = await this.usersService.findOne(userId);
        if (
            money.cmp(user.calcAmount(dto.type, dto.amount), user.balance) <= 0
        ) {
            try {
                const success = isProduction
                    ? await processPayment(PaymentSystems.get(dto.type), dto)
                    : true;

                if (success) {
                    await fallback({
                        cb: async () =>
                            await this.updateBalances(user, dto, 'API'),
                        maxRetry: 5,
                        startTimeout: 30,
                    });
                    return {
                        response: {
                            status: 'OK',
                            message: 'OK',
                            account: dto.account,
                            amount: dto.amount,
                            transaction: this.request.query.transaction,
                        },
                    };
                }
            } catch (e) {
                this.logger.error({ message: e });
                throw new HttpException(
                    {
                        response: {
                            status: 'ERROR',
                            message: 'ERROR DURING PAYMENT',
                            account: dto.account,
                            amount: dto.amount,
                        },
                    },
                    500,
                );
            }
        } else
            throw new HttpException(
                {
                    response: {
                        status: 'ERROR',
                        message: 'NOT ENOUGHT MONEY',
                        amount: dto.amount,
                        balance: user.balance,
                    },
                },
                400,
            );
    }

    async getPaymentsApi(userId: number, dto: GetPaymentsDto) {
        return await this.statisticsService.getTerminalPayments({
            user_id: userId,
            type: dto.type,
            start_date: dto.startDate.toString(),
            end_date: dto.endDate.toString(),
        });
    }

    async getPaymentSystems() {
        return [...paymentSystems.keys()];
    }

    private async updateBalances(
        user: User,
        dto: AddPaymentDto,
        type: PaymentLogType,
    ) {
        let isInitialUser = true;
        const queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction('READ COMMITTED');

        const recursive = async (_user: User, _dto: AddPaymentDto) => {
            const user = await queryRunner.manager.findOne(User, {
                where: {
                    id: _user.id,
                },

                lock: {
                    mode: 'pessimistic_read',
                },
                relations: ['parent', 'percents', 'parent.percents'],
            });

            const percent = user.getPercentByPaymentSystem(_dto.type);
            if (percent) {
                const { oldBalance, newBalance, percentage, totalCharge } =
                    await this.chargeMoneyFromBalance(
                        queryRunner,
                        user,
                        percent,
                        _dto.amount,
                    );
                if (isInitialUser) {
                    switch (type) {
                        case 'USER': {
                            await this.createPaymentLog(queryRunner, {
                                user: user,
                                account: _dto.account,
                                amount: _dto.amount,
                                type: _dto.type,
                                tax: percentage,
                                summary: totalCharge,
                                start_balance: oldBalance,
                                end_balance: newBalance,
                            });
                            break;
                        }
                        case 'API': {
                            await this.createTerminalPaymentLog(queryRunner, {
                                user: user,
                                account: _dto.account,
                                amount: _dto.amount,
                                tax: percentage,
                                summary: totalCharge,
                                start_balance: oldBalance,
                                end_balance: newBalance,
                                tr_number: this.request.query
                                    .tr_number as unknown as string,
                                tr_time: this.request.query
                                    .tr_time as unknown as Date,
                                transaction: this.request.query
                                    .transaction as unknown as string,
                                type: _dto.type,
                            });
                            break;
                        }
                        default:
                            break;
                    }

                    isInitialUser = false;
                }
                if (user.parent) {
                    await recursive(user.parent, _dto);
                }
            }
        };

        await recursive(user, dto);

        try {
            await queryRunner.commitTransaction();
        } catch (e) {
            await queryRunner.rollbackTransaction();
            return Promise.reject();
        } finally {
            await queryRunner.release();
        }
    }

    private async createPaymentLog(
        queryRunner: QueryRunner,
        dto: CreatePaymentDto,
    ) {
        const log = queryRunner.manager.create<Payment>(Payment, dto);
        return await queryRunner.manager.save(log);
    }

    private async createTerminalPaymentLog(
        queryRunner: QueryRunner,
        dto: CreateTerminalPaymentDto,
    ) {
        const log = queryRunner.manager.create<TerminalPayment>(
            TerminalPayment,
            dto,
        );
        return await queryRunner.manager.save(log);
    }

    private async chargeMoneyFromBalance(
        queryRunner: QueryRunner,
        user: User,
        percent: Amount,
        amount: Amount,
    ): Promise<ChargeResult> {
        const percentage = money.percent(amount, percent);
        const totalCharge = money.add(amount, percentage);
        const oldBalance = user.balance;
        const newBalance = money.subtract(user.balance, totalCharge);
        const diff = money.subtract(newBalance, oldBalance);
        const log = queryRunner.manager.create<BalanceLog>(BalanceLog, {
            admin: this.user,
            user: user,
            type: LogType.Charge,
            old_balance: oldBalance,
            new_balance: newBalance,
            diff: diff,
        });
        await queryRunner.manager.save(log);
        user.balance = newBalance;
        user = await queryRunner.manager.save(user);
        return {
            oldBalance,
            newBalance,
            percentage,
            totalCharge,
        };
    }

    async getBalance() {
        const balance = [];
        for (const ps of PaymentSystems) {
            try {
                balance.push({
                    name: translate.get(ps[0]),
                    balance: await ps[1].getBalance(),
                });
            } catch (e) {
                this.logger.error({ message: e });
            }
        }
        return balance;
    }
}
async function processPayment(
    ps: PaymentSystem,
    dto: AddPaymentDto,
): Promise<boolean> {
    switch (dto.type) {
        case Platezhka.name: {
            return await ps.addPayment(dto.account, dto.amount, dto.operator);
        }
        case MCS.name: {
            return await ps.addPayment(
                dto.account,
                dto.amount,
                dto.passport,
                dto.fullName,
                dto.birthDate,
            );
        }
        default:
            return await ps.addPayment(dto.account, dto.amount);
    }
}
