import {HttpException} from "@nestjs/common";

export type MSISDN = `${number}`;
export const MSISDN_LENGTH = 12;
export function testMsisdn(msisdn: MSISDN) {
    return /^\d{11,12}$/.test(msisdn);
}
export type Amount = `${number}.${number}`;
export function testAmount(amount: Amount) {
    return /^(-)?\d+\.\d{2}$/.test(amount);
}
export const defaultAmount: Amount = '0.00';

export type ChargeResult = {
    oldBalance: Amount;
    newBalance: Amount;
    percentage: Amount;
    totalCharge: Amount;
};