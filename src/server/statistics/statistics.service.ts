import { Injectable, Inject, Scope } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { Payment } from './entities/payment.entity';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

import * as excel from 'node-excel-export';
import * as moment from 'moment';
import * as money from 'money-math';
import { BalanceLog, LogType } from '../users/entities/balance-log.entity';
import { User } from '../users/entities/user.entity';
import { CreateTerminalPaymentDto } from './dto/create-terminal-payment.dto';
import { TerminalPayment } from './entities/terminal-payment.entity';

import { StatisticsDto } from './statistics.controller';
import { translate } from '../ps/payment-system/index';
import { Role } from '../users/entities/role';

@Injectable({ scope: Scope.REQUEST })
export class StatisticsService {
    user: User;

    constructor(
        @Inject(REQUEST) private readonly request: Request,
        @InjectRepository(Payment)
        private paymentsRepository: Repository<Payment>,
        @InjectRepository(TerminalPayment)
        private terminalPaymentRepository: Repository<TerminalPayment>,
    ) {
        this.user = this.request.user as User;
    }

    async create(createPaymentDto: CreatePaymentDto) {
        const payment = this.paymentsRepository.create(createPaymentDto);
        return await this.paymentsRepository.save<Payment>(payment);
    }

    async createTerminalPayment(createPaymentDto: CreateTerminalPaymentDto) {
        const payment = this.terminalPaymentRepository.create(createPaymentDto);
        return await this.terminalPaymentRepository.save<TerminalPayment>(
            payment,
        );
    }

    // Payments

    async getPayments(dto: StatisticsDto) {
        const start = moment(dto.start_date, 'YYYY-MM-DD');
        const end = moment(dto.end_date, 'YYYY-MM-DD');
        end.add(1, 'day');

        let user = undefined;
        if (dto.user_id) {
            user = await Payment.query(
                `SELECT * FROM users WHERE id = ${dto.user_id}`,
            );
            user = user[0];
        }

        if (this.user.isPartner())
            return await Payment.query(`
                SELECT payment.*, u1.login, u2.id as "parent", IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name" FROM payment 
                JOIN users u1 ON payment.userId = u1.id
                LEFT JOIN users u2 ON u1.parentId = u2.id
                WHERE u1.parentId = ${this.user.id}
                AND payment.created_at
                BETWEEN '${start.format('YYYY-MM-DD')}' 
                AND '${end.format('YYYY-MM-DD')}'
                ${dto.user_id ? `AND payment.userId = ${dto.user_id}` : ''}
                ${dto.type ? `AND payment.type = "${dto.type}"` : ''}
                ORDER BY payment.created_at`);
        else
            return await Payment.query(`
                SELECT payment.*, u1.login, u2.id as "parent", IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name" FROM payment
                JOIN users u1 ON payment.userId = u1.id
                LEFT JOIN users u2 ON u1.parentId = u2.id
                WHERE payment.created_at 
                BETWEEN '${start.format('YYYY-MM-DD')}' 
                AND '${end.format('YYYY-MM-DD')}'
                ${
                    dto.user_id
                        ? user.role === Role.USER
                            ? `AND payment.userId = ${dto.user_id}`
                            : `AND u2.id = ${dto.user_id}`
                        : ''
                }
                ${dto.type ? `AND payment.type = "${dto.type}"` : ''}
                ORDER BY payment.created_at`);
    }

    async exportPayments(dto: StatisticsDto) {
        const payments = await this.getPayments(dto);
        return this.paymentsToXlsx(payments, dto);
    }

    // BalanceLogs

    async getBalanceLogs(dto: StatisticsDto) {
        const start = moment(dto.start_date, 'YYYY-MM-DD');
        const end = moment(dto.end_date, 'YYYY-MM-DD');
        end.add(1, 'day');

        if (this.user.isPartner())
            return await BalanceLog.query(
                `SELECT balance_log.*, IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name", u2.name AS "parent", u3.name AS "admin" FROM balance_log 
                JOIN users u1 ON balance_log.userId = u1.id 
                LEFT JOIN users u2 ON u1.parentId = u2.id
                LEFT JOIN users u3 ON balance_log.adminId = u3.id
                WHERE 
                balance_log.type IN (${LogType.Add}, ${LogType.Edit})
                AND balance_log.created_at BETWEEN '${start.format(
                    'YYYY-MM-DD',
                )}' AND '${end.format('YYYY-MM-DD')}'
                AND u1.parentId = ${this.user.id}
                ${dto.user_id ? `AND balance_log.userId = ${dto.user_id}` : ''}
                ORDER BY balance_log.created_at DESC`,
            );
        else {
            return BalanceLog.query(
                `SELECT balance_log.*, IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name", u2.name AS "parent", u3.name AS "admin" FROM balance_log 
                JOIN users u1 ON balance_log.userId = u1.id 
                LEFT JOIN users u2 ON u1.parentId = u2.id
                LEFT JOIN users u3 ON balance_log.adminId = u3.id
                WHERE balance_log.type IN (${LogType.Add}, ${LogType.Edit})
                AND balance_log.created_at BETWEEN '${start.format(
                    'YYYY-MM-DD',
                )}' AND '${end.format('YYYY-MM-DD')}'
                ${dto.user_id ? `AND balance_log.userId = ${dto.user_id}` : ''}
                ORDER BY balance_log.created_at DESC`,
            );
        }
    }

    async exportBalanceLogs(dto: StatisticsDto) {
        const logs = await this.getBalanceLogs(dto);
        return this.balanceLogsToXlsx(logs, dto);
    }

    // PaymentsSummary

    async getPaymentsSummary(dto: StatisticsDto) {
        const start = moment(dto.start_date, 'YYYY-MM-DD');
        const end = moment(dto.end_date, 'YYYY-MM-DD');

        let user = undefined;
        if (dto.user_id) {
            user = await Payment.query(
                `SELECT * FROM users WHERE id = ${dto.user_id}`,
            );
            user = user[0];
        }

        end.add(1, 'day');
        let summary = [];

        if (user && user.role === Role.PARTNER) {
            summary = await Payment.query(`
            SELECT bl.total_amount, bl.login, bl.name,
                   bl.parent
            FROM
                (SELECT ABS(SUM(CONVERT(logs.summary, DECIMAL(65,2)))) as "total_amount",
                    u1.login,
                    u2.id as "parent",
                    IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name"
                FROM payment logs
                JOIN users u1 ON logs.userId = u1.id
                LEFT JOIN users u2 ON u1.parentId = u2.id
                WHERE
                logs.created_at
                    BETWEEN '${start.format('YYYY-MM-DD')}'
                    AND '${end.format('YYYY-MM-DD')}'
                    AND u2.id = ${dto.user_id}
                ${dto.type ? `AND logs.type = '${dto.type}'` : ''}
                GROUP BY logs.userId
                ORDER BY logs.userId) bl`);
            const start_end =
                await Payment.query(`SELECT bl1.old_balance as "start_balance", bl2.new_balance as "end_balance", bl.accrual as "accrual",
                bl.first as "first", bl.last as "last"
                FROM 
                (SELECT SUM(CONVERT(diff, DECIMAL(65,2))) as "accrual", 
                    userId, MIN(id) as "first", MAX(id) as "last" 
                    FROM balance_log
                    WHERE created_at BETWEEN '${start.format('YYYY-MM-DD')}'
                    AND '${end.format(
                        'YYYY-MM-DD',
                    )}'  AND balance_log.userId = ${
                    dto.user_id
                } AND balance_log.type = 1
                    GROUP BY userId) bl
                    LEFT JOIN balance_log bl1 ON bl.first = bl1.id
                    LEFT JOIN balance_log bl2 ON bl.last = bl2.id`);

            const all = summary.reduce(
                (prev, curr) => ({
                    name: 'Итого',
                    parent: null,
                    login: 'Итого',
                    total_amount: money.add(
                        prev.total_amount,
                        curr.total_amount || '0.00',
                    ),
                }),
                {
                    name: 'Итого',
                    parent: null,
                    login: 'Итого',
                    total_amount: '0.00',
                },
            );
            return [
                { ...all, ...start_end[0], name: user.name },
                { ...all, ...start_end[0] },
            ];
        }

        if (this.user.isPartner()) {
            summary = await Payment.query(`
            SELECT bl.total_amount, bl.login, bl.name, 
                    bl1.old_balance as "start_balance", bl2.new_balance as "end_balance", 
                    bl.parent, bl.accrual, bl.bonus, bl.first as "first", bl.last as "last"
            FROM
                (SELECT ABS(SUM(CONVERT(logs.summary, DECIMAL(65,2)))) as "total_amount",  
                    u1.login,
                    u2.id as "parent", 
                    IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name",
                    bl.accrual,
                    bn.bonus,
                    bl.first, bl.last
                FROM payment logs
                JOIN users u1 ON logs.userId = u1.id
                LEFT JOIN users u2 ON u1.parentId = u2.id
                LEFT JOIN (SELECT SUM(CONVERT(diff, DECIMAL(65,2))) as "accrual", 
                    userId, MIN(id) as "first", MAX(id) as "last" 
                    FROM balance_log 
                    WHERE created_at BETWEEN '${start.format('YYYY-MM-DD')}' 
                    AND '${end.format('YYYY-MM-DD')}'
                    GROUP BY userId) bl
                    ON logs.userId = bl.userId 
                LEFT JOIN (SELECT SUM(CONVERT(bonus, DECIMAL(65,2))) as "bonus", partnerId FROM bonuses
                    WHERE created_at BETWEEN '${start.format('YYYY-MM-DD')}' 
                    AND '${end.format('YYYY-MM-DD')}'
                    GROUP BY partnerId) bn
                    ON logs.userId = bn.partnerId
                WHERE
                logs.created_at BETWEEN '${start.format('YYYY-MM-DD')}' 
                AND '${end.format('YYYY-MM-DD')}'
                AND u2.id = ${this.user.id}
                ${dto.user_id ? `AND logs.userId = ${dto.user_id}` : ''}
                ${dto.type ? `AND logs.type = '${dto.type}'` : ''}
                GROUP BY logs.userId, bl.accrual, bn.bonus, bl.first, bl.last
                ORDER BY logs.userId) bl
            JOIN balance_log bl1 on bl.first = bl1.id
            JOIN balance_log bl2 on bl.last = bl2.id`);
        } else {
            summary = await Payment.query(`
            SELECT bl.total_amount, bl.login, bl.name,
                   bl1.old_balance as "start_balance", 
                   bl2.new_balance as "end_balance", 
                   bl.parent, bl.accrual, bl.bonus, 
                   bl.last as "last", bl.first as "first"
            FROM
                (SELECT ABS(SUM(CONVERT(logs.summary, DECIMAL(65,2)))) as "total_amount",
                    u1.login,
                    u2.id as "parent",
                    IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name",
                    bl.accrual,
                    bn.bonus,
                    bl.first,
                    bl.last
                FROM payment logs
                JOIN users u1 ON logs.userId = u1.id
                LEFT JOIN users u2 ON u1.parentId = u2.id
                LEFT JOIN (SELECT SUM(CONVERT(diff, DECIMAL(65,2))) as "accrual", 
                    userId, MIN(id) as "first", MAX(id) as "last" 
                    FROM balance_log
                    WHERE created_at BETWEEN '${start.format('YYYY-MM-DD')}'
                    AND '${end.format('YYYY-MM-DD')}'
                    GROUP BY userId) bl
                    ON logs.userId = bl.userId
                LEFT JOIN (SELECT SUM(CONVERT(bonus, DECIMAL(65,2))) as "bonus", partnerId FROM bonuses
                    WHERE created_at BETWEEN '${start.format('YYYY-MM-DD')}'
                    AND '${end.format('YYYY-MM-DD')}'
                    GROUP BY partnerId) bn
                    ON logs.userId = bn.partnerId
                WHERE
                logs.created_at
                    BETWEEN '${start.format('YYYY-MM-DD')}'
                    AND '${end.format('YYYY-MM-DD')}'
                ${dto.user_id ? `AND logs.userId = ${dto.user_id}` : ''}
                ${dto.type ? `AND logs.type = '${dto.type}'` : ''}
                GROUP BY logs.userId, bl.accrual, bl.first, bl.last, bn.bonus
                ORDER BY logs.userId) bl
            JOIN balance_log bl1 on bl.first = bl1.id
            JOIN balance_log bl2 on bl.last = bl2.id`);
        }
        const all = summary.reduce(
            (prev, curr) => ({
                name: 'Итого',
                parent: null,
                login: 'Итого',
                start_balance: money.add(
                    prev.start_balance,
                    curr.start_balance,
                ),
                end_balance: money.add(
                    prev.end_balance,
                    curr.end_balance || '0.00',
                ),
                total_amount: money.add(
                    prev.total_amount,
                    curr.total_amount || '0.00',
                ),
                accrual: money.add(prev.accrual, curr.accrual || '0.00'),
                bonus: money.add(prev.bonus, curr.bonus || '0.00'),
            }),
            {
                name: 'Итого',
                parent: null,
                login: 'Итого',
                start_balance: '0.00',
                end_balance: '0.00',
                total_amount: '0.00',
                accrual: '0.00',
                bonus: '0.00',
            },
        );

        return [...summary, all];
    }

    // BalanceLogsSummary

    async getBalanceLogsSummary(dto: StatisticsDto) {
        const start = moment(dto.start_date, 'YYYY-MM-DD');
        const end = moment(dto.end_date, 'YYYY-MM-DD');
        end.add(1, 'day');
        if (this.user.isPartner())
            return await BalanceLog.query(`
            SELECT IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name", c.charged as "charged", a.added as "added" FROM users u1 
            JOIN
             (SELECT ABS(SUM(CONVERT(bl1.diff, DECIMAL(65, 2)))) AS "charged", bl1.userId FROM balance_log bl1
                    WHERE bl1.diff < 0 
                    AND bl1.created_at
                    BETWEEN '${start.format('YYYY-MM-DD')}' 
                    AND '${end.format('YYYY-MM-DD')}'
                    GROUP BY bl1.userId) c 
            ON u1.id = c.userId
            JOIN 
                (SELECT ABS(SUM(CONVERT(bl2.diff, DECIMAL(65, 2)))) AS "added", bl2.userId FROM balance_log bl2
                WHERE bl2.diff > 0
                AND bl2.created_at
                BETWEEN '${start.format('YYYY-MM-DD')}' 
                AND '${end.format('YYYY-MM-DD')}'
                GROUP BY bl2.userId) a
            ON u1.id = a.userId
            LEFT JOIN users u2 ON u1.parentId = u2.id
            WHERE u1.parentId = ${this.user.id}
            `);
        else
            return await BalanceLog.query(`
            SELECT IFNULL(CONCAT(u1.name, ' (', u2.name, ') '), u1.name) AS "name", c.charged as "charged", a.added as "added" FROM users u1 
             JOIN
             (SELECT ABS(SUM(CONVERT(bl1.diff, DECIMAL(65, 2)))) AS "charged", bl1.userId FROM balance_log bl1
                    WHERE bl1.diff < 0
                    ${dto.user_id ? `AND bl1.userId = ${dto.user_id}` : ''} 
                    AND bl1.created_at
                    BETWEEN '${start.format('YYYY-MM-DD')}' 
                    AND '${end.format('YYYY-MM-DD')}'
                    GROUP BY bl1.userId) c 
            ON u1.id = c.userId
            JOIN 
                (SELECT ABS(SUM(CONVERT(bl2.diff, DECIMAL(65, 2)))) AS "added", bl2.userId FROM balance_log bl2
                WHERE bl2.diff > 0
                ${dto.user_id ? `AND bl2.userId = ${dto.user_id}` : ''}
                AND bl2.created_at
                BETWEEN '${start.format('YYYY-MM-DD')}' 
                AND '${end.format('YYYY-MM-DD')}'
                GROUP BY bl2.userId) a            
            ON u1.id = a.userId
            LEFT JOIN users u2 ON u1.parentId = u2.id
            `);
    }

    // TerminalPayments

    async getTerminalPayments(dto: StatisticsDto) {
        const start = moment(dto.start_date, 'YYYY-MM-DD');
        const end = moment(dto.end_date, 'YYYY-MM-DD');
        end.add(1, 'day');

        return TerminalPayment.query(`
                SELECT terminal_payment.*, u1.login, u1.name AS "name" FROM terminal_payment
                JOIN users u1 ON terminal_payment.userId = u1.id
                WHERE terminal_payment.created_at 
                BETWEEN '${start.format('YYYY-MM-DD')}' 
                AND '${end.format('YYYY-MM-DD')}'
                ${
                    dto.user_id
                        ? `AND terminal_payment.userId = ${dto.user_id}`
                        : ''
                }
                ${dto.type ? `AND terminal_payment.type = "${dto.type}"` : ''}
                ORDER BY terminal_payment.created_at`);
    }

    async exportTerminalPayments(dto: StatisticsDto) {
        const data = await this.getTerminalPayments(dto);
        return this.terminalPaymentsToXlsx(data, dto);
    }

    // TO XLSX

    private paymentsToXlsx(
        payments: Array<Partial<Payment & { name: string; login: string }>>,
        dto: StatisticsDto,
    ) {
        const styles = {
            heading: {
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                },
                font: {
                    name: 'Times New Roman',
                    color: {
                        rgb: '00000000',
                    },
                    sz: 10,
                    bold: true,
                },
            },
            header: {
                fill: {
                    fgColor: {
                        rgb: '3faf46',
                    },
                },
                font: {
                    name: 'Times New Roman',
                    color: {
                        rgb: 'FFFFFFFF',
                    },
                    sz: 8,
                    bold: true,
                },
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                },
                border: {
                    top: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    bottom: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    left: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    right: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                },
            },
            cell: () => {
                return {
                    alignment: {
                        vertical: 'center',
                        horizontal: 'center',
                        wrapText: true,
                    },
                    font: {
                        name: 'Times New Roman',
                        sz: 8,
                    },
                    border: {
                        top: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        bottom: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        left: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        right: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                    },
                };
            },
        };

        const specification = {
            created_at: {
                displayName: 'Дата пополнения',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            user: {
                displayName: 'Пользователь',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            type: {
                displayName: 'Платежная система',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            account: {
                displayName: 'Аккаут',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            amount: {
                displayName: 'Сумма',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            tax: {
                displayName: 'Проценты',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            summary: {
                displayName: 'Итого',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
        };

        const heading = [
            [
                {
                    value: `Платежи с ${dto.start_date} по ${dto.end_date}`,
                    style: styles.heading,
                },
            ],
        ];
        const merges = [
            { start: { row: 1, column: 1 }, end: { row: 1, column: 4 } },
        ];

        const report = excel.buildExport([
            {
                name: 'Платежи',
                heading,
                merges,
                specification,
                data: payments.map((payment) => {
                    return {
                        ...payment,
                        type: translate.get(payment.type),
                        amount: payment.amount.replace('.', ','),
                        tax: payment.tax.replace('.', ','),
                        summary: payment.summary.replace('.', ','),
                        user: payment.name,
                        created_at: moment(payment.created_at).format(
                            'DD.MM.YYYY HH:mm:ss',
                        ),
                    };
                }),
            },
        ]);
        return report;
    }

    private balanceLogsToXlsx(logs: Array<BalanceLog>, dto: StatisticsDto) {
        const styles = {
            heading: {
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                },
                font: {
                    name: 'Times New Roman',
                    color: {
                        rgb: '00000000',
                    },
                    sz: 10,
                    bold: true,
                },
            },
            header: {
                fill: {
                    fgColor: {
                        rgb: '3faf46',
                    },
                },
                font: {
                    name: 'Times New Roman',
                    color: {
                        rgb: 'FFFFFFFF',
                    },
                    sz: 8,
                    bold: true,
                },
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                },
                border: {
                    top: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    bottom: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    left: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    right: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                },
            },
            cell: () => {
                return {
                    alignment: {
                        vertical: 'center',
                        horizontal: 'center',
                        wrapText: true,
                    },
                    font: {
                        name: 'Times New Roman',
                        sz: 8,
                    },
                    border: {
                        top: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        bottom: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        left: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        right: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                    },
                };
            },
        };
        const heading = [
            [
                {
                    value: `История изменения баланса с ${moment(
                        dto.start_date,
                    ).format('DD.MM.YYYY')} по ${moment(dto.end_date).format(
                        'DD.MM.YYYY',
                    )}`,
                    style: styles.heading,
                },
            ],
        ];
        const merges = [
            { start: { row: 1, column: 1 }, end: { row: 1, column: 6 } },
        ];

        const specification = {
            created_at: {
                displayName: 'Дата/время',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            name: {
                displayName: 'Пользователь',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            admin: {
                displayName: 'Оператор',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            type: {
                displayName: 'Тип операции',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            old_balance: {
                displayName: 'Начальный баланс',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            diff: {
                displayName: 'Сумма',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            new_balance: {
                displayName: 'Итоговый баланс',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
        };

        const report = excel.buildExport([
            {
                name: 'Изменения баланса',
                heading,
                merges,
                specification,
                data: logs.map((log) => {
                    return {
                        ...log,
                        old_balance: log.old_balance.replace('.', ','),
                        new_balance: log.new_balance.replace('.', ','),
                        diff: log.diff.replace('.', ','),
                        admin: log?.admin || 'АДМИНИСТРАТОР',
                        type: log.type === 2 ? 'Редактирование' : 'Добавление',
                        created_at: moment(log.created_at).format(
                            'DD.MM.YYYY HH:mm:ss',
                        ),
                    };
                }),
            },
        ]);
        return report;
    }

    private terminalPaymentsToXlsx(
        payments: Array<
            Partial<TerminalPayment & { name: string; login: string }>
        >,
        dto: StatisticsDto,
    ) {
        const styles = {
            heading: {
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                },
                font: {
                    name: 'Times New Roman',
                    color: {
                        rgb: '00000000',
                    },
                    sz: 10,
                    bold: true,
                },
            },
            header: {
                fill: {
                    fgColor: {
                        rgb: '3faf46',
                    },
                },
                font: {
                    name: 'Times New Roman',
                    color: {
                        rgb: 'FFFFFFFF',
                    },
                    sz: 8,
                    bold: true,
                },
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                },
                border: {
                    top: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    bottom: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    left: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                    right: {
                        style: 'thin',
                        color: { rgb: '000000' },
                    },
                },
            },
            cell: () => {
                return {
                    alignment: {
                        vertical: 'center',
                        horizontal: 'center',
                        wrapText: true,
                    },
                    font: {
                        name: 'Times New Roman',
                        sz: 8,
                    },
                    border: {
                        top: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        bottom: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        left: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                        right: {
                            style: 'thin',
                            color: { rgb: '000000' },
                        },
                    },
                };
            },
        };

        const specification = {
            created_at: {
                displayName: 'Дата пополнения',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            user: {
                displayName: 'Пользователь',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            account: {
                displayName: 'Номер телефона',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            type: {
                displayName: 'Платежная система',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            amount: {
                displayName: 'Сумма',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            tax: {
                displayName: 'Проценты',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            summary: {
                displayName: 'Итого',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            tr_number: {
                displayName: 'Номер терминала',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
            tr_time: {
                displayName: 'Время терминала',
                width: 150,
                cellStyle: styles.cell,
                headerStyle: styles.header,
            },
        };

        const heading = [
            [
                {
                    value: `Платежи терминалов с ${moment(
                        dto.start_date,
                    ).format('DD.MM.YYYY')} по ${moment(dto.end_date).format(
                        'DD.MM.YYYY',
                    )}`,
                    style: styles.heading,
                },
            ],
        ];
        const merges = [
            { start: { row: 1, column: 1 }, end: { row: 1, column: 4 } },
        ];

        const report = excel.buildExport([
            {
                name: 'Платежи терминалов',
                heading,
                merges,
                specification,
                data: payments.map((payment) => {
                    return {
                        ...payment,
                        amount: payment.amount.replace('.', ','),
                        type: translate.get(payment.type),
                        tax: payment.tax.replace('.', ','),
                        summary: payment.summary.replace('.', ','),
                        user: payment.name,
                        created_at: moment(payment.created_at).format(
                            'DD.MM.YYYY HH:mm:ss',
                        ),
                        tr_time:
                            payment.tr_time &&
                            moment(payment.tr_time).format(
                                'DD.MM.YYYY HH:mm:ss',
                            ),
                    };
                }),
            },
        ]);
        return report;
    }
}
