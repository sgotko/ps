import { Amount } from 'src/server/ps/types';
import { User } from 'src/server/users/entities/user.entity';

export class CreatePaymentDto {
    user: User;
    amount: Amount;
    account: string;
    type: string;
    tax: string;
    summary: string;
    start_balance: string;
    end_balance: string;
}
