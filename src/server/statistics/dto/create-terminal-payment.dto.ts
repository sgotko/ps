import { Amount } from 'src/server/ps/types';
import { User } from 'src/server/users/entities/user.entity';

export class CreateTerminalPaymentDto {
    user: User;
    amount: Amount;
    type: string;
    account: string;
    tax: string;
    summary: string;
    start_balance: string;
    end_balance: string;
    tr_time?: Date;
    tr_number?: string;
    transaction?: string;
}
