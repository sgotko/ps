import {
    Controller,
    Get,
    Res,
    Query,
    UseGuards,
    Body,
    Post,
    UsePipes,
} from '@nestjs/common';
import { StatisticsService } from './statistics.service';
import { Readable } from 'stream';

import * as moment from 'moment';
import { SessionAuthGuard } from '../guards/session-auth.guard';
import { JoiValidationPipe } from '../pipes/joi-validation.pipe';
import * as Joi from 'joi';

export type StatisticsDto = {
    start_date: string;
    end_date: string;
    user_id?: number;
    type?: string;
};

const StatisticsJoiSchema = Joi.object<StatisticsDto>({
    start_date: Joi.string().required().isoDate(),
    end_date: Joi.string().required().isoDate(),
    user_id: Joi.number(),
    type: Joi.string(),
});

@Controller('api/statistics')
@UseGuards(SessionAuthGuard)
export class StatisticsController {
    constructor(private readonly statisticsService: StatisticsService) {}

    // Payments

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('payments/get')
    async getPayments(@Body() body: StatisticsDto) {
        return await this.statisticsService.getPayments(body);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('payments/export')
    async exportPayments(@Body() body: StatisticsDto, @Res() res) {
        const file = await this.statisticsService.exportPayments(body);

        const stream = new Readable();

        stream.push(file);
        stream.push(null);

        res.set({
            'Content-Type':
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition': `attachment; filename=${body.start_date}_${body.end_date}.xlsx`,
        });

        stream.pipe(res);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('balance_logs/get')
    async getBalanceLogs(@Body() body: StatisticsDto) {
        return this.statisticsService.getBalanceLogs(body);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('balance_logs/export')
    async exportBalanceLogs(@Body() body, @Res() res) {
        const file = await this.statisticsService.exportBalanceLogs(body);

        const stream = new Readable();

        stream.push(file);
        stream.push(null);

        res.set({
            'Content-Type':
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition': `attachment; filename=${moment(
                body.start_date,
            ).format('DD.MM.YYYY')}_${moment(body.end_date).format(
                'DD.MM.YYYY',
            )}.xlsx`,
        });

        stream.pipe(res);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('payments/summary')
    async getSummary(@Body() body: StatisticsDto) {
        return await this.statisticsService.getPaymentsSummary(body);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('balance_logs/summary')
    async getBalanceLogsSummary(@Body() body: StatisticsDto) {
        return await this.statisticsService.getBalanceLogsSummary(body);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('terminal_payments/get')
    async getTerminalPayments(@Body() body: StatisticsDto) {
        return await this.statisticsService.getTerminalPayments(body);
    }

    @UsePipes(new JoiValidationPipe(StatisticsJoiSchema))
    @Post('terminal_payments/export')
    async exportTerminalPayments(@Body() body: StatisticsDto, @Res() res) {
        const file = await this.statisticsService.exportTerminalPayments(body);
        const stream = new Readable();

        stream.push(file);
        stream.push(null);

        res.set({
            'Content-Type':
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition': `attachment; filename=${moment(
                body.start_date,
            ).format('DD.MM.YYYY')}_${moment(body.end_date).format(
                'DD.MM.YYYY',
            )}.xlsx`,
        });

        stream.pipe(res);
    }
}
