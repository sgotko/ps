import { Amount } from 'src/server/ps/types';
import { User } from 'src/server/users/entities/user.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
    BaseEntity,
} from 'typeorm';

@Entity('terminal_payment')
export class TerminalPayment extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @ManyToOne(() => User)
    user: User;

    @Column()
    type: string;

    @Column()
    account: string;

    @Column()
    amount: Amount;

    @Column()
    tax: string;

    @Column()
    summary: string;

    @Column()
    start_balance: string;

    @Column()
    end_balance: string;

    @Column({
        nullable: true,
    })
    tr_number: string;

    @Column({
        precision: null,
        type: 'timestamp',
        nullable: true,
        default: () => null,
    })
    tr_time: Date;

    @Column({
        nullable: true,
        default: () => null,
    })
    transaction: string; // id transaction from api caller

    @CreateDateColumn({
        precision: null,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
