import { User } from 'src/server/users/entities/user.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
    BaseEntity,
} from 'typeorm';

@Entity('payment')
export class Payment extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @ManyToOne(() => User)
    user: User;

    @Column()
    account: string;

    @Column()
    type: string;

    @Column()
    amount: string;

    @Column()
    tax: string;

    @Column()
    summary: string;

    @Column()
    start_balance: string;

    @Column()
    end_balance: string;

    @CreateDateColumn({
        precision: null,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
