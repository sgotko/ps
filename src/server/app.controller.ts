import {
    Body,
    Controller,
    Get,
    Post,
    Req,
    Res,
    UnauthorizedException,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { SessionAuthGuard } from './guards/session-auth.guard';
import { UsersService } from './users/users.service';
import { join } from 'path';

import { UserGuard } from './guards/user.guard';
import { Role } from './users/entities/role';
import { BasicAuthGuard } from './guards/basic-auth.guard';
import { TransformUserInterceptor } from './interceptors/transform.user.interceptor';
import { ApiFormatInterceptor } from './interceptors/api-format.interceptor';

@Controller()
export class AppController {
    constructor(private usersService: UsersService) {}

    @UseGuards(BasicAuthGuard)
    @Get('api/auth/me')
    @UseInterceptors(ApiFormatInterceptor, TransformUserInterceptor)
    async getMe(@Req() req) {
        return this.usersService.findOne(req.user.id);
    }

    @UseGuards(UserGuard)
    @Post('auth/get-code')
    async totp(@Body() body) {
        const { login } = body;
        return this.usersService.sendTotpCode(login);
    }

    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Req() req: any) {
        return req.user;
    }

    @UseGuards(SessionAuthGuard)
    @Get('auth/logout')
    logout(@Req() req) {
        req.session.destroy() as boolean;
        throw new UnauthorizedException();
    }

    @UseGuards(SessionAuthGuard)
    @Get(['/payments', '/user', '/notifications'])
    plainPages(@Res() res) {
        return res.sendFile(join(__dirname, '..', 'public', 'index.html'));
    }

    @UseGuards(SessionAuthGuard)
    @Get(['', '/'])
    rootPage(@Req() req, @Res() res) {
        if (req.user.role !== Role.USER) return res.redirect('/users');
        return res.sendFile(join(__dirname, '..', 'public', 'index.html'));
    }

    @UseGuards(SessionAuthGuard)
    @Get(['/users', '/balance', '/changelog'])
    adminPages(@Req() req, @Res() res) {
        if (req.user.role === Role.USER) return res.redirect('/');
        return res.sendFile(join(__dirname, '..', 'public', 'index.html'));
    }

    @Get(['/login'])
    async loginPage(@Req() req: any, @Res() res: any) {
        if (req.user) {
            try {
                if (req.user.role === Role.USER) return res.redirect('/');
                else return res.redirect('/users');
            } catch (e) {}
        }
        return res.sendFile(join(__dirname, '..', 'public', 'index.html'));
    }
}
