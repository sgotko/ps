import {Request} from "express";
import {User} from "./users/entities/user.entity";

type RequestAuthorized = Request & {
			user: User
}