import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Role } from '../users/entities/role';

@Injectable()
export class RoleGuard implements CanActivate {
    roles: Role[];

    constructor(roles: Role[]) {
        this.roles = roles;
    }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        if (!this.roles) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const user = request.user;

        return this.matchRoles(this.roles, user.role);
    }

    private matchRoles(roles, userRole): boolean | Promise<boolean> {
        return roles.includes(userRole);
    }
}
