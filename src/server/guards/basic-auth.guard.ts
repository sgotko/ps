import { ExecutionContext, HttpException, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Builder } from 'xml2js';

enum ApiResponseFormat {
    'json' = 'json',
    'xml' = 'xml',
}

@Injectable()
export class BasicAuthGuard extends AuthGuard('basic') {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const result = (await super.canActivate(context)) as boolean;
        await super.logIn(request);
        return result;
    }

    handleRequest<TUser = any>(
        err: any,
        user: any,
        _info: any,
        context: any,
    ): TUser {
        const { format } = context.switchToHttp().getRequest().query;

        if (err || !user)
            throw (
                err ||
                new HttpException(
                    this.formatResponse(
                        {
                            response: {
                                status: 'ERROR',
                                message: 'UNAUTHORIZED',
                            },
                        },
                        format,
                    ),
                    401,
                )
            );
        return user;
    }

    private formatResponse(res: any, format: string) {
        switch (format) {
            case ApiResponseFormat.json:
                res = res;
                break;
            default:
                res = new Builder().buildObject(res);
        }

        return res;
    }
}
