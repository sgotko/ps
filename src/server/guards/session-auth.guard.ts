import {
    CanActivate,
    ExecutionContext,
    UnauthorizedException,
    Injectable,
} from '@nestjs/common';

import { UsersService } from '../users/users.service';

@Injectable()
export class SessionAuthGuard implements CanActivate {
    constructor(private usersService: UsersService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const httpContext = context.switchToHttp();
        const request = httpContext.getRequest();

        try {
            if (request.session.passport.user) {
                const user = await this.usersService.findForValidate(
                    request.session.passport.user.login,
                );
                if (user) return true;
                else {
                    request.session.destroy();
                    return false;
                }
            } else {
                return false;
            }
        } catch (e) {
            throw new UnauthorizedException();
        }
    }
}
