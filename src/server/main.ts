import { NestApplication, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import * as session from 'express-session';
import MySqlStoreConnection = require('express-mysql-session');
const MySqlStore = MySqlStoreConnection(session);
import { SeederService } from './database/seeds/seeder.service';
import passport = require('passport');
import { UnauthorizedExceptionFilter } from './exception-filters/unauthorised.filter';
import { ForbiddenExceptionFilter } from './exception-filters/forbidden.filter';

import { WinstonModule } from 'nest-winston';
import DailyRotateFile = require('winston-daily-rotate-file');
import winston = require('winston');
import path = require('path');
import { UsersService } from './users/users.service';
import { PsService } from './ps/ps.service';
const { combine, timestamp, prettyPrint } = winston.format;

export enum AppMode {
    Production = 'production',
    Development = 'development',
}

export const isDevelopment = process.env.mode === AppMode.Development;

export const isProduction = process.env.mode === AppMode.Production;

async function bootstrap() {
    const app = await NestFactory.create<NestApplication>(AppModule, {
        logger: WinstonModule.createLogger({
            format: combine(
                timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                prettyPrint({
                    colorize: true,
                }),
            ),
            transports: [
                new DailyRotateFile({
                    json: true,
                    filename: 'app.log',
                    dirname: path.join('data', 'logs'),
                    maxFiles: 30,
                }),
                new winston.transports.Console({}),
            ],
        }),
    });
    const configService = app.get(ConfigService);
    const sessionSecret = configService.get<string>('SESSION_SECRET');
    const sessionTtl = configService.get<number>('SESSION_TTL');

    const seederService = app.get(SeederService);
    await seederService.seed();

    const store = new MySqlStore({
        host: configService.get('MYSQL_HOST'),
        port: +configService.get<number>('MYSQL_PORT'),
        user: configService.get('MYSQL_USER'),
        password: configService.get('MYSQL_PASSWORD'),
        database: configService.get('MYSQL_DATABASE'),
    });
    await app.use(
        session({
            name: 'session',
            secret: sessionSecret || 'secret',
            resave: false,
            saveUninitialized: false,
            cookie: {
                maxAge: 1000 * 60 * 60 * (+sessionTtl || 1),
            },
            store: store,
        }),
    );

    await app.use(passport.initialize());
    await app.use(passport.session());

    app.useGlobalFilters(
        new UnauthorizedExceptionFilter(),
        new ForbiddenExceptionFilter(),
    );

    const appPort = configService.get('APP_PORT');
    await app.listen(appPort || 3000);
}

bootstrap();
