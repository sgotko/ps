import {
    Injectable,
    NestInterceptor,
    ExecutionContext,
    CallHandler,
} from '@nestjs/common';
import * as xml2js from 'xml2js';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

enum ApiResponseFormat {
    'json' = 'json',
    'xml' = 'xml',
}

type ApiResponse = {
    response?: {
        status: string;
        message: string;
    } & any;
};

@Injectable()
export class ApiFormatInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const request = context.switchToHttp().getRequest();
        request.query = { ...request.query, ...request.body };
        const { format } = context.getArgs()[0].query;

        return next.handle().pipe(
            map((value) => this.formatResponse(value, format)),
            catchError(async (e) => {
                console.error(e);
                return await this.formatResponse(e.response, format);
            }),
        );
    }

    private formatResponse(res: ApiResponse, format: string) {
        switch (format) {
            case ApiResponseFormat.json:
                return res;
            default:
                if (Array.isArray(res)) {
                    return new xml2js.Builder().buildObject(
                        JSON.parse(
                            JSON.stringify(
                                removeEmptyValues(
                                    res.map((item) => ({ item: item })),
                                ),
                            ),
                        ),
                    );
                } else
                    return new xml2js.Builder().buildObject(
                        JSON.parse(JSON.stringify(removeEmptyValues(res))),
                    );
        }
    }
}

function removeEmptyValues(val) {
    if (Array.isArray(val)) {
        return val.reduce((res, cur) => {
            if (cur !== null) {
                return [...res, removeEmptyValues(cur)];
            }
            return res;
        }, []);
    } else if (Object.prototype.toString.call(val) === '[object Object]') {
        return Object.keys(val).reduce((res, key) => {
            if (val[key] !== null) {
                return Object.assign({}, res, {
                    [key]: removeEmptyValues(val[key]),
                });
            }
            return res;
        }, undefined);
    }
    return val;
}
