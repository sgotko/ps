import {
    CallHandler,
    ExecutionContext,
    Injectable,
    NestInterceptor,
} from '@nestjs/common';
import { Observable, map } from 'rxjs';
import { User } from '../users/entities/user.entity';

@Injectable()
export class TransformUserInterceptor implements NestInterceptor {
    intercept(
        _context: ExecutionContext,
        next: CallHandler,
    ): Observable<Partial<User>> {
        return next.handle().pipe(
            map((value: User) => ({
                id: value.id,
                name: value.name,
                balance: value.balance,
            })),
        );
    }
}
