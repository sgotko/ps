import * as React from 'react';

import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-mui';
import {
    Backdrop,
    Button,
    CircularProgress,
    InputAdornment,
    Theme,
    Snackbar,
    Container,
    FormControl,
    Input,
    InputLabel,
    Box,
    IconButton,
} from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import { Alert } from '@mui/lab';
import { IMaskInput } from 'react-imask';

import { connect } from 'react-redux';
import { setUser } from '../../../plugins/redux/actions';

import { useFetcher } from '../../../utils/useFetcher';
import { isSuccess } from '../../../utils/responses';

import { AddPaymentFormValues } from '../index';
import { ClearOutlined } from '@mui/icons-material';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        field: {
            width: '100%',
            marginBottom: theme.spacing(2),
        },
        button: {
            width: '100%',
        },
    }),
);

interface AccountInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const AccountInput = React.forwardRef<HTMLElement, AccountInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={/^\d+$/}
                signed={false}
                min={0}
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

interface CurrencyInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const CurrencyInput = React.forwardRef<HTMLElement, CurrencyInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={Number}
                scale={2}
                signed={true}
                padFractionalZeros={true}
                thousandsSeparator=""
                min={1}
                radix="."
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

const LtkTab = ({ setUser }) => {
    const classes = useStyles();
    const fetcher = useFetcher();
    const [open, setOpen] = React.useState<boolean>(false);
    const [success, setSuccess] = React.useState<boolean>(false);
    const [checked, setChecked] = React.useState<boolean>(false);
    const [account, setAccount] = React.useState<any>();
    const addPaymentInitialValues: AddPaymentFormValues = {
        account: '',
        amount: '',
        type: 'LTK',
    };

    const defaultErrorMessage = 'Ошибка при пополнении счёта';
    const [message, setMessage] = React.useState<string>();

    const handleClose = () => setOpen(false);

    const addPayment = async (values: AddPaymentFormValues, { resetForm }) => {
        const account = values.account.trim();
        const amount = values.amount.split(' ').join('').trim();
        const type = values.type;

        try {
            let res = await fetcher(`api/ps/add_payment`, {
                redirect: 'follow',
                method: 'POST',
                body: JSON.stringify({
                    account,
                    amount,
                    type,
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (!isSuccess(res)) {
                const err = await res.json();
                if (err.message) setMessage(err.message);
                else setMessage(defaultErrorMessage);
                setSuccess(false);
                setOpen(true);
            } else {
                setUser(await res.json());
                setMessage(`Аккаунт ${account} успешно пополнен`);
                setSuccess(true);
                resetForm();
                setOpen(true);
            }
        } catch (error) {
            if (error.message) setMessage(error.message);
            else setMessage(defaultErrorMessage);
            setSuccess(false);
            setOpen(true);

            console.error(error);
        } finally {
            setChecked(false);
            setAccount(undefined);
        }
    };

    const checkAccount = async (
        values: Pick<AddPaymentFormValues, 'account' | 'type'>,
    ) => {
        const account = values.account.trim();
        const type = values.type;
        const res = await fetcher(`api/ps/get_account_info`, {
            method: 'POST',
            body: JSON.stringify({
                account,
                type,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        try {
            if (isSuccess(res)) {
                setAccount(await res.json());
                setChecked(true);
            } else {
                const err = await res.json();
                if (err.message) setMessage(err.message);
                else setMessage('Ошибка при получении информации о аккаунте');
                setSuccess(false);
                setOpen(true);
            }
        } catch (error) {
            if (error.message) setMessage(error.message);
            else setMessage(defaultErrorMessage);
            setSuccess(false);
            setOpen(true);
        }

    };

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: {
                    xs: 'center',
                    sm: 'flex-start',
                },
            }}
        >
            <Formik
                initialValues={addPaymentInitialValues}
                validate={(values) => {
                    const errors: Partial<AddPaymentFormValues> = {};
                    if (!values.account) {
                        errors.account = 'Введите номер счёта/номер телефона';
                    }
                    if (checked && !parseFloat(values.amount)) {
                        errors.amount = 'Введите сумму';
                    } else if (checked && parseFloat(values.amount) < 1.0)
                        errors.amount = 'Минимальная сумма - 1.00 ₽';

                    return errors;
                }}
                onSubmit={checked ? addPayment : checkAccount}
            >
                {({ isSubmitting, setFieldValue }) => (
                    <Form>
                        <Field
                            className={classes.field}
                            component={TextField}
                            name="account"
                            type="account"
                            label="Счёт"
                            variant="standard"
                            disabled={isSubmitting || checked}
                            InputProps={{
                                inputComponent: AccountInput,
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            disabled={checked}
                                            aria-label="clear"
                                            onClick={() => {
                                                setFieldValue('account', '');
                                            }}
                                            onMouseDown={() => {
                                                setFieldValue('account', '');
                                            }}
                                            edge="end"
                                        >
                                            <ClearOutlined />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                            helperText="Номер счёта или номер телефона в формате 064XХХХХХХХ"
                        />

                        {checked && <p>Аккаунт: {account.name}</p>}

                        {checked && (
                            <Field
                                className={classes.field}
                                component={TextField}
                                name="amount"
                                type="amount"
                                label="Сумма"
                                variant="standard"
                                disabled={isSubmitting}
                                InputProps={{
                                    inputComponent: CurrencyInput,
                                }}
                            />
                        )}

                        <Button
                            className={classes.button}
                            type="submit"
                            variant="contained"
                            color="success"
                            disabled={isSubmitting}
                        >
                            {checked ? 'Пополнить' : 'Проверить счёт'}
                        </Button>

                        <Backdrop open={isSubmitting}>
                            <CircularProgress color="primary" />
                        </Backdrop>
                    </Form>
                )}
            </Formik>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Alert
                    onClose={handleClose}
                    severity={success ? 'success' : 'error'}
                    style={{
                        width: '100%',
                    }}
                >
                    {message}
                </Alert>
            </Snackbar>
        </Box>
    );
};

const mapDispatchToProps = {
    setUser,
};

export default connect(null, mapDispatchToProps)(LtkTab);
