import * as React from 'react';
import { locale } from 'moment';
import * as moment from 'moment';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-mui';
import {
    Backdrop,
    Button,
    CircularProgress,
    InputAdornment,
    Theme,
    Snackbar,
    Box,
    IconButton,
    TextField as TField
} from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import { Alert } from '@mui/lab';
import { IMaskInput } from 'react-imask';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';

import { connect } from 'react-redux';
import { setUser } from '../../../plugins/redux/actions';

import { useFetcher } from '../../../utils/useFetcher';

import { AddPaymentFormValues } from '../index';
import { ClearOutlined } from '@mui/icons-material';
import { isSuccess } from '../../../utils/responses';

interface McsAddPaymentFormValues extends AddPaymentFormValues {
    passport: string;
    birthDate: string;
    fullName: string;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        field: {
            width: '100%',
            marginBottom: theme.spacing(2),
        },
        button: {
            width: '100%',
        },
    }),
);

interface AccountInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const AccountInput = React.forwardRef<HTMLElement, AccountInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={/^\d+$/}
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

const PassportInput = React.forwardRef<HTMLElement, AccountInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={/^\d+$/}
                maxLength={10}
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

interface CurrencyInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const CurrencyInput = React.forwardRef<HTMLElement, CurrencyInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={Number}
                scale={2}
                signed={true}
                padFractionalZeros={true}
                thousandsSeparator=""
                min={1}
                radix="."
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

const McsTab = ({ setUser }) => {
    const classes = useStyles();
    const fetcher = useFetcher();
    const [open, setOpen] = React.useState<boolean>(false);
    const [success, setSuccess] = React.useState<boolean>(false);
    const addPaymentInitialValues: McsAddPaymentFormValues = {
        account: '',
        amount: '',
        type: 'MCS',
        passport: '',
        birthDate: new Date().toString(),
        fullName: '',
    };

    const defaultErrorMessage = 'Ошибка при пополнении счёта';
    const [message, setMessage] = React.useState<string>();

    const handleClose = () => setOpen(false);

    const addPayment = async (
        values: McsAddPaymentFormValues,
        { resetForm },
    ) => {
        const account = values.account.trim();
        const amount = values.amount.split(' ').join('').trim();
        const type = values.type;
        const passport = values.passport;
        const birthDate = moment(values.birthDate).format('YYYY-MM-DD');
        const fullName = values.fullName;

        try {
            let res = await fetcher(`api/ps/add_payment`, {
                redirect: 'follow',
                method: 'POST',
                body: JSON.stringify({
                    account,
                    passport,
                    amount,
                    type,
                    birthDate,
                    fullName
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (!isSuccess(res)) {
                const err = await res.json();
                if (err.message) setMessage(err.message);
                else setMessage(defaultErrorMessage);
                setSuccess(false);
                setOpen(true);
            } else {
                setUser(await res.json());
                setMessage(`Номер ${account} успешно пополнен`);
                setSuccess(true);
                resetForm();
                setOpen(true);
            }
        } catch (error) {
            if (error.message) setMessage(error.message);
            else setMessage(defaultErrorMessage);
            setSuccess(false);
            setOpen(true);

            console.error(error);
        }
    };

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: {
                    xs: 'center',
                    sm: 'flex-start',
                },
            }}
        >
            <Formik
                initialValues={addPaymentInitialValues}
                validate={(values) => {
                    const errors: Partial<McsAddPaymentFormValues> = {};
                    if (!values.account) {
                        errors.account = 'Введите номер Абонента';
                    }
                    if (values.account.length < 11)
                        errors.account = 'Введите номер Абонента полностью';
                    if (!parseFloat(values.amount)) {
                        errors.amount = 'Введите сумму';
                    } else if (parseFloat(values.amount) < 1.0)
                        errors.amount = 'Минимальная сумма - 1.00 ₽';

                    if (!values.passport) {
                        errors.passport = 'Введите номер и серию паспорта';
                    } else if (values.passport.length < 10) {
                        errors.passport =
                            'Введите номер и серию паспорта полностью';
                    }
                    return errors;
                }}
                onSubmit={addPayment}
            >
                {({
                    isSubmitting,
                    setFieldValue,
                    values,
                    touched,
                    errors,
                    validateForm,
                }) => (
                    <Form>
                        <Field
                            className={classes.field}
                            component={TextField}
                            name="account"
                            type="account"
                            label="Абонент"
                            variant="standard"
                            disabled={isSubmitting}
                            InputProps={{
                                inputComponent: AccountInput,
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="clear"
                                            onClick={() => {
                                                setFieldValue('account', '');
                                            }}
                                            onMouseDown={() => {
                                                setFieldValue('account', '');
                                            }}
                                            edge="end"
                                        >
                                            <ClearOutlined />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                            helperText="Номер телефона в международном формате"
                        />

                        <Field
                            className={classes.field}
                            component={TextField}
                            name="passport"
                            type="passport"
                            label="Паспорт (серия и номер, 10 цифр)"
                            variant="standard"
                            disabled={isSubmitting}
                            InputProps={{
                                inputComponent: PassportInput,
                            }}
                            helperText="Паспорт (серия и номер, 10 цифр)"
                        />

                        <Field
                            className={classes.field}
                            component={TextField}
                            name="fullName"
                            type="fullName"
                            label="ФИО"
                            variant="standard"
                            disabled={isSubmitting}
                        />

                        <LocalizationProvider
                            dateAdapter={AdapterMoment}
                            adapterLocale={locale('ru')}
                        >
                            <DatePicker
                                mask="__.__.____"
                                renderInput={(params) => (
                                    <TField
                                        name="birthDate"
                                        error={
                                            touched['birthDate'] &&
                                            Boolean(errors['birthDate'])
                                        }
                                        helperText={
                                            touched['birthDate'] &&
                                            (errors[
                                                'birthDate'
                                            ] as unknown as string)
                                        }
                                        {...params}
                                    />
                                )}
                                onChange={(value) =>
                                    setFieldValue('birthDate', value)
                                }
                                label="Дата рождения"
                                value={values.birthDate}
                            />
                        </LocalizationProvider>

                        <Field
                            className={classes.field}
                            component={TextField}
                            name="amount"
                            type="amount"
                            label="Сумма"
                            variant="standard"
                            disabled={isSubmitting}
                            InputProps={{
                                inputComponent: CurrencyInput,
                            }}
                        />

                        <Button
                            className={classes.button}
                            type="submit"
                            variant="contained"
                            color="success"
                            disabled={isSubmitting}
                        >
                            Пополнить
                        </Button>

                        <Backdrop open={isSubmitting}>
                            <CircularProgress color="primary" />
                        </Backdrop>
                    </Form>
                )}
            </Formik>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Alert
                    onClose={handleClose}
                    severity={success ? 'success' : 'error'}
                    style={{
                        width: '100%',
                    }}
                >
                    {message}
                </Alert>
            </Snackbar>
        </Box>
    );
};

const mapDispatchToProps = {
    setUser,
};

export default connect(null, mapDispatchToProps)(McsTab);
