import * as React from 'react';

import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-mui';
import {
    Backdrop,
    Button,
    CircularProgress,
    InputAdornment,
    Theme,
    Snackbar,
    Container,
    FormControl,
    Input,
    InputLabel,
    Box,
    IconButton,
    Select,
    MenuItem,
} from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import { Alert } from '@mui/lab';
import { IMaskInput } from 'react-imask';

import { connect } from 'react-redux';
import { setUser } from '../../../plugins/redux/actions';

import type { AddPaymentFormValues as A } from '..';

type AddPaymentFormValues = A & { operator: number | string };

import { useFetcher } from '../../../utils/useFetcher';

import { ClearOutlined } from '@mui/icons-material';
import { isSuccess } from '../../../utils/responses';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        field: {
            width: '100%',
            marginBottom: theme.spacing(2),
        },
        button: {
            width: '100%',
        },
    }),
);

interface AccountInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
    mask: any;
}

const AccountInput = React.forwardRef<HTMLElement, AccountInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, mask, ...other } = props;
        return (
			<IMaskInput
                {...other}
                mask={mask}
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

interface CurrencyInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const CurrencyInput = React.forwardRef<HTMLElement, CurrencyInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={Number}
                scale={2}
                signed={true}
                padFractionalZeros={true}
                thousandsSeparator=""
                min={10}
                max={14999}
                radix="."
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

const PlatezhkaTab = ({ setUser }) => {
    const classes = useStyles();
    const fetcher = useFetcher();

    const [operators, setOperators] = React.useState<Array<any>>([]);
    const [checked, setChecked] = React.useState<boolean>(false);
    const [account, setAccount] = React.useState<any>();
    const [operator, setOperator] = React.useState<any>();

    const getOperators = async (): Promise<void> => {
        const res = await (
            await fetcher(`api/ps/get_operators`, {
                method: 'POST',
                body: JSON.stringify({
                    type: 'Platezhka',
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
        ).json();
        setOperators([...res]);
    };

    React.useEffect(() => {
        getOperators();
    }, []);

    const [open, setOpen] = React.useState<boolean>(false);
    const [success, setSuccess] = React.useState<boolean>(false);
    const addPaymentInitialValues: AddPaymentFormValues = {
        account: '',
        amount: '',
        type: 'Platezhka',
        operator: '',
    };

    const defaultErrorMessage = 'Ошибка при пополнении счёта';
    const [message, setMessage] = React.useState<string>();

    const handleClose = () => setOpen(false);

    const addPayment = async (values: AddPaymentFormValues, { resetForm }) => {
        const account = values.account.trim();
        const amount = values.amount.split(' ').join('').trim();

        try {
            let res = await fetcher(`api/ps/add_payment`, {
                redirect: 'follow',
                method: 'POST',
                body: JSON.stringify({
                    operator: operator?.id,
                    account: account,
                    type: values.type,
                    amount,
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (!isSuccess(res)) {
                const err = await res.json();
                if (err.message) setMessage(err.message);
                else setMessage(defaultErrorMessage);
                setSuccess(false);
                setOpen(true);
            } else {
                setUser(await res.json());
                setMessage(`Номер ${account} успешно пополнен`);
                setSuccess(true);
                resetForm();
                setOpen(true);
            }
        } catch (error) {
            if (error.message) setMessage(error.message);
            else setMessage(defaultErrorMessage);
            setSuccess(false);
            setOpen(true);

            console.error(error);
        } finally {
            setChecked(false);
            setAccount(undefined);
        }
    };

    const checkAccount = async (values) => {
        const res = await fetcher(`api/ps/get_account_info`, {
            method: 'POST',
            body: JSON.stringify({
                account: values.account.trim(),
                type: values.type,
                operator: values.operator,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        try {
            if (isSuccess(res)) {
                setAccount('Аккаунт проверен');
                setChecked(true);
            } else {
                const err = await res.json();
                if (err.message) setMessage(err.message);
                else setMessage('Ошибка при получении информации о аккаунте');
                setSuccess(false);
                setOpen(true);
            }
        } catch (error) {
            if (error.message) setMessage(error.message);
            else setMessage(defaultErrorMessage);
            setSuccess(false);
            setOpen(true);

            console.error(error);
        }
    };

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: {
                    xs: 'center',
                    sm: 'flex-start',
                },
            }}
        >
            <Formik
                initialValues={addPaymentInitialValues}
                validate={(values) => {
                    const errors: Partial<AddPaymentFormValues> = {};

                    if (!values.account) {
                        errors.account = 'Введите номер';
                    }

                    if (!values.operator) {
                        errors.operator = 'Выберите оператора';
                    }

                    if (checked && !parseFloat(values.amount)) {
                        errors.amount = 'Введите сумму';
                    } else if (checked && parseFloat(values.amount) < 10.0)
                        errors.amount = 'Минимальная сумма - 10.00 ₽';

                    return errors;
                }}
                onSubmit={checked ? addPayment : checkAccount}
            >
                {({ isSubmitting, setFieldValue, errors }) => (
                    <Form>
                        <FormControl>
                            <InputLabel id="operator-label">
                                Оператор
                            </InputLabel>
                            <Select
                                sx={{
                                    maxWidth: 300,
                                    minWidth: 300,
                                }}
                                disabled={isSubmitting || checked}
                                label="Оператор"
                                labelId="operator-label"
                                id="operator"
                                value={operator?.id}
                                name="operator"
                                onChange={(e) => {
                                    setOperator(
                                        operators.find(
                                            (item) =>
                                                item.id === e.target.value,
                                        ),
                                    );
                                    setFieldValue(
                                        'operator',
                                        e.target.value,

                                        true,
                                    );
                                }}
                            >
                                {operators.map((operator) => {
                                    return (
                                        <MenuItem
                                            value={operator.id}
                                            key={operator.id}
                                        >
                                            {operator.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                            {errors['operator'] && (
                                <p className="MuiFormHelperText-root Mui-error MuiFormHelperText-sizeMedium css-1d1r5q-MuiFormHelperText-root">
                                    {errors['operator']}
                                </p>
                            )}
                        </FormControl>

                        <Field
                            className={classes.field}
                            component={TextField}
                            name="account"
                            type="account"
                            label={operator?.template?.name || ''}
                            variant="standard"
                            placeholder={operator?.template?.regexp || ''}
                            disabled={isSubmitting || checked}
                            InputProps={{
                                inputComponent: AccountInput,
                                inputProps: {
									mask: operator?.template?.regexp || '',
                                },
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            disabled={isSubmitting || checked}
                                            aria-label="clear"
                                            onClick={() => {
                                                setFieldValue('account', '');
                                            }}
                                            onMouseDown={() => {
                                                setFieldValue('account', '');
                                            }}
                                            edge="end"
                                        >
                                            <ClearOutlined />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                            helperText={operator?.template?.regexp || 'Номер'}
                        />

                        {checked && <p>{account}</p>}

                        {checked && (
                            <Field
                                className={classes.field}
                                component={TextField}
                                name="amount"
                                type="amount"
                                label="Сумма"
                                variant="standard"
                                disabled={isSubmitting}
                                InputProps={{
                                    inputComponent: CurrencyInput,
                                }}
                                helperText="Минимум 10 ₽, максимум 14999 ₽"
                            />
                        )}

                        <Button
                            className={classes.button}
                            type="submit"
                            variant="contained"
                            color="success"
                            disabled={isSubmitting}
                        >
                            {checked ? 'Пополнить' : 'Проверить счёт'}
                        </Button>

                        <Backdrop open={isSubmitting}>
                            <CircularProgress color="primary" />
                        </Backdrop>
                    </Form>
                )}
            </Formik>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Alert
                    onClose={handleClose}
                    severity={success ? 'success' : 'error'}
                    style={{
                        width: '100%',
                    }}
                >
                    {message}
                </Alert>
            </Snackbar>
        </Box>
    );
};

const mapDispatchToProps = {
    setUser,
};

export default connect(null, mapDispatchToProps)(PlatezhkaTab);
