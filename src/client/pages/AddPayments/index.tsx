import * as React from 'react';
import { Box } from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import { connect } from 'react-redux';

import 'moment/locale/ru';

export interface AddPaymentFormValues {
    account: string;
    amount: string;
    type: string;
}

import McsTab from './tabs/McsTab';
import LtkTab from './tabs/LtkTab';
import RckTab from './tabs/RckTab';
import PlatezhkaTab from './tabs/PlatezhkaTab';
import MirandaTab from './tabs/MirandaTab';

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`tabpanel-${index}`}
            aria-labelledby={`tab-${index}`}
            {...other}
        >
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabpanel-${index}`,
    };
}

interface PaymentsProps {}
const Payments: React.FC<PaymentsProps & { user?: any }> = ({ user }) => {
    const [tab, setTab] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setTab(newValue);
    };

    return (
        <Box>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                    value={tab}
                    onChange={handleChange}
                    variant={'scrollable'}
                    scrollButtons={true}
                >
                    <Tab label="МКС" {...a11yProps(0)} value={0} />
                    <Tab label="ЛТК" {...a11yProps(1)} value={1} />
                    <Tab label="РЦК" {...a11yProps(2)} value={2} />
                    {/* <Tab label="Луганет" {...a11yProps(3)} value={3} /> */}
                    <Tab label="Платежка" {...a11yProps(4)} value={4} />
                    <Tab label="Миранда" {...a11yProps(5)} value={5} />
                </Tabs>
            </Box>

            {/* МКС */}
            <TabPanel value={tab} index={0}>
                <McsTab />
            </TabPanel>

            {/* ЛТК */}
            <TabPanel value={tab} index={1}>
                <LtkTab />
            </TabPanel>

            {/*  РЦК */}
            <TabPanel value={tab} index={2}>
                <RckTab />
            </TabPanel>

            {/*  Луганет */}
            {/* <TabPanel value={tab} index={3}>
                <LuganetTab />
            </TabPanel> */}

            <TabPanel value={tab} index={4}>
                <PlatezhkaTab />
            </TabPanel>

            <TabPanel value={tab} index={5}>
                <MirandaTab />
            </TabPanel>
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps, null)(Payments);
