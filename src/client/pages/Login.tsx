import * as React from 'react';
import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-mui';
import {
    Button,
    IconButton,
    Card,
    CardHeader,
    CardContent,
    InputAdornment,
    Grid,
    LinearProgress,
    Theme,
} from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { makeStyles, createStyles } from '@mui/styles';
import { connect } from 'react-redux';
import { setUser } from '../plugins/redux/actions';

type Props = {
    setUser: () => void;
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        form: {
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',

            justifyContent: 'center',

            '& > div, & > button': {
                width: '100%',
                marginBottom: theme.spacing(2),
            },
        },
        card: {
            minWidth: 300,
        },
        button: {
            width: '100%',
        },
        cardHeader: {
            textAlign: 'center',
        },
        grid: {
            backgroundColor: theme.palette.primary.main,
            height: '100vh',
            width: '100vw',
        },
    }),
);

interface LoginFormValues {
    login: string;
    password: string;
    code: string;
}

const authWithSms = process.env.SMS_AUTH;

const Login = ({ setUser }) => {
    const classes = useStyles();

    const [showPassword, setShowPassword] = React.useState(false);
    const [codeSended, setCodeSended] = React.useState(false);

    const initialValues: LoginFormValues = {
        login: '',
        password: '',
        code: '',
    };

    const onLogin = (values: LoginFormValues, { setSubmitting, setErrors }) => {
        if (authWithSms && !codeSended) {
            fetch('/auth/get-code', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    login: values.login,
                    password: values.password,
                }),
            })
                .then((res) => {
                    if (res.status === 201) {
                        setCodeSended(true);
                    } else
                        setErrors({
                            login: 'Введены неверные данные.',
                            password: 'Введены неверные данныe.',
                        });
                })
                .finally(() => {
                    setSubmitting(false);
                });
        } else
            fetch('/auth/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    login: values.login,
                    password: values.password,
                    code: values.code,
                }),
            })
                .then(async (res) => {
                    if (res.status === 201) {
                        window.location.reload();
                    } else
                        setErrors({
                            login: 'Введены неверные данные.',
                            password: 'Введены неверные данныe.',
                        });
                })
                .finally(() => {
                    setSubmitting(false);
                });
    };

    return (
        <Grid
            container
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            className={classes.grid}
        >
            <Card className={classes.card}>
                <CardHeader subheader="вход" className={classes.cardHeader} />
                <CardContent>
                    <Formik
                        initialValues={initialValues}
                        validate={(values) => {
                            const errors: Partial<LoginFormValues> = {};
                            if (!values.login) {
                                errors.login = 'Введите логин';
                            }
                            if (!values.password) {
                                errors.password = 'Введите пароль';
                            }

                            if (authWithSms && codeSended && !values.code) {
                                errors.code = 'Введите код из СМС';
                            }

                            return errors;
                        }}
                        onSubmit={onLogin}
                    >
                        {({ submitForm, isSubmitting }) => (
                            <Form className={classes.form}>
                                <Field
                                    component={TextField}
                                    name="login"
                                    type="login"
                                    label="Логин"
                                    variant="standard"
                                    disabled={authWithSms && codeSended}
                                />
                                <Field
                                    component={TextField}
                                    type={showPassword ? 'text' : 'password'}
                                    name="password"
                                    label="Пароль"
                                    variant="standard"
                                    disabled={authWithSms && codeSended}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() =>
                                                        setShowPassword(
                                                            !showPassword,
                                                        )
                                                    }
                                                    disabled={authWithSms && codeSended}
                                                >
                                                    {showPassword ? (
                                                        <Visibility />
                                                    ) : (
                                                        <VisibilityOff />
                                                    )}
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />

                                { authWithSms && codeSended && (
                                    <Field
                                        component={TextField}
                                        name="code"
                                        type="code"
                                        label="Код"
                                        variant="standard"
                                    />
                                )}

                                {isSubmitting && <LinearProgress />}
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    disabled={isSubmitting}
                                    onClick={submitForm}
                                    type="submit"
                                >
																	{
																		authWithSms && !codeSended ? "Получить код" : "Войти"
																	}
                                </Button>
                            </Form>
                        )}
                    </Formik>
                </CardContent>
            </Card>
        </Grid>
    );
};

const mapDispatchToProps = {
    setUser,
};

export default connect(null, mapDispatchToProps)(Login);
