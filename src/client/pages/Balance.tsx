import { Card, CardContent, Typography } from '@mui/material';
import * as React from 'react';
import { useFetcher } from '../utils/useFetcher';

const Balance: React.FC<{}> = ({}) => {
    const [balance, setBalance] = React.useState<Array<any>>([]);
    const fetcher = useFetcher();

    React.useEffect(() => {
        async function getBalance() {
            const data = await (await fetcher('/api/ps/balance')).json();
            setBalance([...data]);
        }

        getBalance();
    }, []);

    return (
        <div>
            {balance.map((item, index) => (
                <Card variant="outlined" key={index}>
                    <CardContent>
                        <Typography fontWeight={600}>{item.name}</Typography>
                        <Typography>{item.balance}</Typography>
                    </CardContent>
                </Card>
            ))}
        </div>
    );
};

export default Balance;
