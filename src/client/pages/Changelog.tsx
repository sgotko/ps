import * as React from 'react';
import { Box } from '@mui/material';

import ReactMarkdown from 'react-markdown';
import { useFetcher } from '../utils/useFetcher';

interface ChangelogProps {}

const Changelog: React.FC<ChangelogProps> = ({}) => {
    const [log, setLog] = React.useState('');
    const fetcher = useFetcher();

    const fetchLog = async () => {
        const res = await fetcher('/changelog.md');
        setLog(await res.text());
    };

    React.useEffect(() => {
        fetchLog();
    }, []);

    return (
        <Box>
            <ReactMarkdown>{log}</ReactMarkdown>
        </Box>
    );
};

export default Changelog;
