import * as React from 'react';
import { locale } from 'moment';
import * as moment from 'moment';
import {
    FormControl,
    Button,
    Theme,
    TextField,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TablePagination,
    TableRow,
    TableHead,
    Box,
    Autocomplete,
    Backdrop,
    CircularProgress,
} from '@mui/material';
import 'moment/locale/ru';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { makeStyles, createStyles } from '@mui/styles';

import { Formik, Form } from 'formik';

import { connect } from 'react-redux';

import 'moment/locale/ru';
import { useFetcher } from '../../../utils/useFetcher';
import downloadFileFromResponse from '../../../utils/downloadFile';

import { Role } from '../../../../common/types';
import { paymentSystems, allUsersValue } from '../index';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        form: {
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            width: '100%',
            '& > div, & > button': {
                width: '100%',
                maxWidth: 300,
                marginBottom: theme.spacing(2),
            },
        },
    }),
);

const BonusesTab: React.FC<{ user?: any }> = ({ user }) => {
    const initialValues = {
        start_date: moment(),
        end_date: moment(),
        user: allUsersValue,
        type: paymentSystems[0],
    };
    const styles = useStyles();
    const fetcher = useFetcher();
    const [loading, setLoading] = React.useState<boolean>(false);
    const [users, setUsers] = React.useState<Array<any>>([]);
    const [data, setData] = React.useState<Array<any>>([]);
    const initialPagination = {
        page: 0,
        rowsPerPage: 15,
    };
    const [pagination, setPagination] = React.useState(initialPagination);

    const disableLoading = () => setLoading(false);
    React.useEffect(() => {
        setPagination(initialPagination);
    }, [loading]);

    React.useEffect(() => {
        async function getUsers() {
            const res = await fetcher(`/api/users/filtered`, {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							},
							body: JSON.stringify({
								roles: [Role.ACCOUNTANT, Role.PARTNER, Role.USER]
							})
						});
            const _users = [...(await res.json())];

            setUsers(_users);
        }
        user.role !== Role.USER && getUsers();
    }, []);

    const handleChangePage = (e, page) => {
        setPagination({ ...pagination, page: page });
    };

    const handleChangeRowsPerPage = (e) => {
        const rowsPerPage = parseInt(e.target.value, 10);
        setPagination({ page: 0, rowsPerPage: rowsPerPage });
    };

    const getBonuses = async (values) => {
        let body: any = {};
        if (
            user.role === Role.ADMINISTRATOR ||
            user.role === Role.PARTNER ||
            user.role === Role.ACCOUNTANT
        ) {
            if (values.user.id === 0) {
                body.start_date = values.start_date.format('YYYY-MM-DD');
                body.end_date = values.end_date.format('YYYY-MM-DD');
            } else {
                body.start_date = values.start_date.format('YYYY-MM-DD');
                body.end_date = values.end_date.format('YYYY-MM-DD');

                body.user_id = values.user.id;
            }
        } else {
            body.start_date = values.start_date.format('YYYY-MM-DD');
            body.end_date = values.end_date.format('YYYY-MM-DD');

            body.user_id = user.id;
        }
        setLoading(true);
        const res = await (
            await fetcher(`/api/statistics/bonuses/get`, {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
        ).json();
        setData([...res]);
        disableLoading();
    };

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: {
                    xs: 'center',
                    sm: 'flex-start',
                },
            }}
        >
            <Formik
                initialValues={initialValues}
                onSubmit={() => {}}
                validate={(values) => {
                    const errors: Partial<{
                        user: any;
                        start_date: any;
                        end_date: any;
                    }> = {};
                    if (!values.start_date) {
                        errors.start_date = 'Выберите дату начала';
                    }

                    if (!values.end_date) {
                        errors.end_date = 'Выберите дату окончания';
                    }

                    if (
                        values.end_date &&
                        values.end_date.isBefore(values.start_date)
                    ) {
                        errors.end_date =
                            'Дата окончания не может быть раньше даты начала';
                    }

                    if (
                        values.start_date &&
                        values.start_date.isAfter(values.end_date)
                    ) {
                        errors.start_date =
                            'Дата начала не может быть позднее даты окончания';
                    }

                    return errors;
                }}
            >
                {({ values, setFieldValue, touched, errors, validateForm }) => (
                    <Form className={styles.form}>
                        {user.role !== Role.USER && (
                            <FormControl>
                                <Autocomplete
                                    disablePortal
                                    id="user"
                                    value={values.user}
                                    options={[allUsersValue, ...users].filter(
                                        (user) => !user.isAdmin,
                                    )}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(event: any, newValue: any) => {
                                        if (!newValue)
                                            setFieldValue(
                                                'user',
                                                allUsersValue,
                                            );
                                        else setFieldValue('user', newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            error={
                                                touched['user'] &&
                                                Boolean(errors['user'])
                                            }
                                            helperText={
                                                touched['user'] &&
                                                (errors[
                                                    'user'
                                                ] as unknown as string)
                                            }
                                            {...params}
                                            label="Пользователь"
                                        />
                                    )}
                                />
                            </FormControl>
                        )}
                        <LocalizationProvider
                            dateAdapter={AdapterMoment}
                            locale={locale('ru')}
                        >
                            <DatePicker
                                renderInput={(params) => (
                                    <TextField
                                        name="start_date"
                                        error={
                                            touched['start_date'] &&
                                            Boolean(errors['start_date'])
                                        }
                                        helperText={
                                            touched['start_date'] &&
                                            (errors[
                                                'start_date'
                                            ] as unknown as string)
                                        }
                                        {...params}
                                    />
                                )}
                                onChange={(value) =>
                                    setFieldValue('start_date', value)
                                }
                                label="Дата начала"
                                value={values.start_date}
                            />
                        </LocalizationProvider>
                        <LocalizationProvider
                            dateAdapter={AdapterMoment}
                            locale={locale('ru')}
                        >
                            <DatePicker
                                renderInput={(params) => (
                                    <TextField
                                        name="end_date"
                                        error={
                                            touched['end_date'] &&
                                            Boolean(errors['end_date'])
                                        }
                                        helperText={
                                            touched['end_date'] &&
                                            (errors[
                                                'end_date'
                                            ] as unknown as string)
                                        }
                                        {...params}
                                    />
                                )}
                                onChange={(value) =>
                                    setFieldValue('end_date', value)
                                }
                                label="Дата окончания"
                                value={values.end_date}
                            />
                        </LocalizationProvider>
                        <Button
                            onClick={async () => {
                                if (await validateForm()) getBonuses(values);
                            }}
                            variant="contained"
                            color="primary"
                        >
                            Выбрать
                        </Button>
                    </Form>
                )}
            </Formik>
            <TableContainer>
                <Table size="small" sx={{ minWidth: 650, maxWidth: '100%' }}>
                    <TableHead>
                        <TableRow>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Дата
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Партнёр
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Пользователь
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Сумма пополнения
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Бонус
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                            .slice(
                                pagination.page * pagination.rowsPerPage,
                                pagination.page * pagination.rowsPerPage +
                                    pagination.rowsPerPage,
                            )
                            .map((row, _index) => {
                                return (
                                    <TableRow
                                        hover
                                        key={row.id}
                                        sx={{
                                            '&:last-child td, &:last-child th':
                                                {
                                                    border: 0,
                                                },
                                        }}
                                    >
                                        <TableCell scope="row" align="left">
                                            {moment(row.created_at).format(
                                                'DD.MM.yyyy HH:mm:ss',
                                            )}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.parent}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.parent ? (
                                                <>
                                                    {row.name.slice(
                                                        0,
                                                        row.name.lastIndexOf(
                                                            '(',
                                                        ),
                                                    )}
                                                    <b>
                                                        <i>
                                                            {row.name.slice(
                                                                row.name.lastIndexOf(
                                                                    '(',
                                                                ),
                                                            )}
                                                        </i>
                                                    </b>
                                                </>
                                            ) : (
                                                <>{row.name}</>
                                            )}
                                        </TableCell>

                                        <TableCell scope="row" align="left">
                                            {row.pAmount}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.bonus}
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                    </TableBody>

                    <TablePagination
                        rowsPerPageOptions={[15, 25, 35]}
                        onPageChange={handleChangePage}
                        rowsPerPage={pagination.rowsPerPage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        count={data.length}
                        page={pagination.page}
                    />
                </Table>
            </TableContainer>

            <Backdrop
                sx={{
                    color: '#fff',
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                }}
                open={loading}
                onClick={disableLoading}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps, null)(BonusesTab);
