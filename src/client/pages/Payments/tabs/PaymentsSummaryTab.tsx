import * as React from 'react';
import { locale } from 'moment';
import * as moment from 'moment';
import {
    FormControl,
    Button,
    Theme,
    TextField,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TablePagination,
    TableRow,
    TableHead,
    Box,
    Autocomplete,
    Backdrop,
    CircularProgress,
    TableFooter,
} from '@mui/material';
import 'moment/locale/ru';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { makeStyles, createStyles } from '@mui/styles';

import { Formik, Form } from 'formik';

import { connect } from 'react-redux';

import 'moment/locale/ru';
import { useFetcher } from '../../../utils/useFetcher';
import downloadFileFromResponse from '../../../utils/downloadFile';

import { Role } from '../../../../common/types';
import { paymentSystems, allUsersValue } from '../index';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        form: {
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            width: '100%',
            '& > div, & > button': {
                width: '100%',
                maxWidth: 300,
                marginBottom: theme.spacing(2),
            },
        },
    }),
);

const PaymentsSummaryTab: React.FC<{ user?: any }> = ({ user }) => {
    const initialValues = {
        start_date: moment(),
        end_date: moment(),
        user: allUsersValue,
        type: paymentSystems[0],
    };

    const fetcher = useFetcher();

    const styles = useStyles();
    const [loading, setLoading] = React.useState<boolean>(false);
    const [users, setUsers] = React.useState<Array<any>>([]);
    const [data, setData] = React.useState<Array<any>>([]);
    const initialPagination = {
        page: 0,
        rowsPerPage: 15,
    };
    const [pagination, setPagination] = React.useState(initialPagination);

    const disableLoading = () => setLoading(false);

    React.useEffect(() => {
        setPagination(initialPagination);
    }, [loading]);

    const handleChangePage = (e, page) => {
        setPagination({ ...pagination, page: page });
    };

    const handleChangeRowsPerPage = (e) => {
        const rowsPerPage = parseInt(e.target.value, 10);
        setPagination({ page: 0, rowsPerPage: rowsPerPage });
    };

    const getSummary = async (values) => {
        let body: any = {};
        if (
            user.role === Role.ADMINISTRATOR ||
            user.role === Role.PARTNER ||
            user.role === Role.ACCOUNTANT
        ) {
            if (values.user.id === 0) {
                body.start_date = values.start_date.format('YYYY-MM-DD');
                body.end_date = values.end_date.format('YYYY-MM-DD');
            } else {
                body.start_date = values.start_date.format('YYYY-MM-DD');
                body.end_date = values.end_date.format('YYYY-MM-DD');

                body.user_id = values.user.id;
            }
        } else {
            body.start_date = values.start_date.format('YYYY-MM-DD');
            body.end_date = values.end_date.format('YYYY-MM-DD');

            body.user_id = user.id;
        }
        body.type = values.type.value || undefined;
        setLoading(true);
        const res = await (
            await fetcher(`/api/statistics/payments/summary`, {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
        ).json();
        setData([...res]);
        disableLoading();
    };

    React.useEffect(() => {
        async function getUsers() {
            const res = await fetcher(`/api/users/filtered`, {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							},
							body: JSON.stringify({
								roles: [Role.ACCOUNTANT, Role.PARTNER, Role.USER]
							})
						});
            const _users = [...(await res.json())];

            setUsers(_users);
        }
        user.role !== Role.USER && getUsers();
    }, []);

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: {
                    xs: 'center',
                    sm: 'flex-start',
                },
            }}
        >
            <Formik
                initialValues={initialValues}
                onSubmit={() => {}}
                validate={(values) => {
                    const errors: Partial<{
                        user: any;
                        start_date: any;
                        end_date: any;
                    }> = {};
                    if (!values.start_date) {
                        errors.start_date = 'Выберите дату начала';
                    }

                    if (!values.end_date) {
                        errors.end_date = 'Выберите дату окончания';
                    }

                    if (
                        values.end_date &&
                        values.end_date.isBefore(values.start_date)
                    ) {
                        errors.end_date =
                            'Дата окончания не может быть раньше даты начала';
                    }

                    if (
                        values.start_date &&
                        values.start_date.isAfter(values.end_date)
                    ) {
                        errors.start_date =
                            'Дата начала не может быть позднее даты окончания';
                    }

                    return errors;
                }}
            >
                {({ values, setFieldValue, touched, errors, validateForm }) => (
                    <Form className={styles.form}>
                        {user.role !== Role.USER && (
                            <FormControl>
                                <Autocomplete
                                    disablePortal
                                    id="user"
                                    value={values.user}
                                    options={[allUsersValue, ...users].filter(
                                        (user) => !user.isAdmin,
                                    )}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(event: any, newValue: any) => {
                                        if (!newValue)
                                            setFieldValue(
                                                'user',
                                                allUsersValue,
                                            );
                                        else setFieldValue('user', newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            error={
                                                touched['user'] &&
                                                Boolean(errors['user'])
                                            }
                                            helperText={
                                                touched['user'] &&
                                                (errors[
                                                    'user'
                                                ] as unknown as string)
                                            }
                                            {...params}
                                            label="Пользователь"
                                        />
                                    )}
                                />
                            </FormControl>
                        )}

                        <FormControl>
                            <Autocomplete
                                disablePortal
                                id="type"
                                value={values.type}
                                options={paymentSystems}
                                getOptionLabel={(option) => option.name}
                                onChange={(event: any, newValue: any) => {
                                    setFieldValue('type', newValue);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        error={
                                            touched['type'] &&
                                            Boolean(errors['type'])
                                        }
                                        helperText={
                                            touched['type'] &&
                                            (errors[
                                                'type'
                                            ] as unknown as string)
                                        }
                                        {...params}
                                        label="Платежная система"
                                    />
                                )}
                            />
                        </FormControl>

                        <LocalizationProvider
                            dateAdapter={AdapterMoment}
                            locale={locale('ru')}
                        >
                            <DatePicker
                                renderInput={(params) => (
                                    <TextField
                                        name="start_date"
                                        error={
                                            touched['start_date'] &&
                                            Boolean(errors['start_date'])
                                        }
                                        helperText={
                                            touched['start_date'] &&
                                            (errors[
                                                'start_date'
                                            ] as unknown as string)
                                        }
                                        {...params}
                                    />
                                )}
                                onChange={(value) =>
                                    setFieldValue('start_date', value)
                                }
                                label="Дата начала"
                                value={values.start_date}
                            />
                        </LocalizationProvider>
                        <LocalizationProvider
                            dateAdapter={AdapterMoment}
                            locale={locale('ru')}
                        >
                            <DatePicker
                                renderInput={(params) => (
                                    <TextField
                                        name="end_date"
                                        error={
                                            touched['end_date'] &&
                                            Boolean(errors['end_date'])
                                        }
                                        helperText={
                                            touched['end_date'] &&
                                            (errors[
                                                'end_date'
                                            ] as unknown as string)
                                        }
                                        {...params}
                                    />
                                )}
                                onChange={(value) =>
                                    setFieldValue('end_date', value)
                                }
                                label="Дата окончания"
                                value={values.end_date}
                            />
                        </LocalizationProvider>
                        <Button
                            onClick={async () => {
                                if (await validateForm()) getSummary(values);
                            }}
                            variant="contained"
                            color="primary"
                        >
                            Выбрать
                        </Button>
                    </Form>
                )}
            </Formik>
            <TableContainer>
                <Table size="small" sx={{ minWidth: 650, maxWidth: '100%' }}>
                    <TableHead>
                        <TableRow>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Пользователь
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Баланс на начало периода
                            </TableCell>

                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Баланс на конец периода
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Итоговая сумма
                            </TableCell>
                            <TableCell
                                sx={{
                                    fontWeight: 'bold',
                                }}
                                align="left"
                            >
                                Начисления
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                            .slice(0, -1)
                            .slice(
                                pagination.page * pagination.rowsPerPage,
                                pagination.page * pagination.rowsPerPage +
                                    pagination.rowsPerPage,
                            )
                            .map((row, _index) => {
                                return (
                                    <TableRow
                                        hover
                                        key={row.id}
                                        sx={{
                                            '&:last-child td, &:last-child th':
                                                {
                                                    border: 0,
                                                },
                                        }}
                                    >
                                        <TableCell scope="row" align="left">
                                            {row.parent ? (
                                                <>
                                                    {row.name.slice(
                                                        0,
                                                        row.name.lastIndexOf(
                                                            '(',
                                                        ),
                                                    )}
                                                    <b>
                                                        <i>
                                                            {row.name.slice(
                                                                row.name.lastIndexOf(
                                                                    '(',
                                                                ),
                                                            )}
                                                        </i>
                                                    </b>
                                                </>
                                            ) : (
                                                row.name
                                            )}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.start_balance}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.end_balance}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.total_amount}
                                        </TableCell>
                                        <TableCell scope="row" align="left">
                                            {row.accrual}
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                    <TableFooter>
                        {data.slice(-1).map((row, _index) => (
                            <TableRow
                                hover
                                key={row.id}
                                sx={{
                                    '&:last-child td, &:last-child th': {
                                        border: 0,
                                    },
                                }}
                            >
                                <TableCell scope="row" align="left">
                                    {row.parent ? (
                                        <>
                                            {row.name.slice(
                                                0,
                                                row.name.lastIndexOf('('),
                                            )}
                                            <b>
                                                <i>
                                                    {row.name.slice(
                                                        row.name.lastIndexOf(
                                                            '(',
                                                        ),
                                                    )}
                                                </i>
                                            </b>
                                        </>
                                    ) : (
                                        row.name
                                    )}
                                </TableCell>
                                <TableCell scope="row" align="left">
                                    {row.start_balance}
                                </TableCell>
                                <TableCell scope="row" align="left">
                                    {row.end_balance}
                                </TableCell>
                                <TableCell scope="row" align="left">
                                    {row.total_amount}
                                </TableCell>
                                <TableCell scope="row" align="left">
                                    {row.accrual}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableFooter>
                    <TablePagination
                        rowsPerPageOptions={[15, 25, 35]}
                        onPageChange={handleChangePage}
                        rowsPerPage={pagination.rowsPerPage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        count={data.length - 1}
                        page={pagination.page}
                    />
                </Table>
            </TableContainer>

            <Backdrop
                sx={{
                    color: '#fff',
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                }}
                open={loading}
                onClick={disableLoading}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps, null)(PaymentsSummaryTab);
