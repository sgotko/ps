import * as React from 'react';
import * as moment from 'moment';
import { Theme, Box } from '@mui/material';
import { makeStyles, createStyles } from '@mui/styles';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import { connect } from 'react-redux';

import 'moment/locale/ru';
import { useFetcher } from '../../utils/useFetcher';
import { Role } from '../../../common/types';

import PaymentsTab from './tabs/PaymentsTab';
import TerminalsPaymentsTab from './tabs/TerminalsPaymentsTab';
import PaymentsSummaryTab from './tabs/PaymentsSummaryTab';
import BalanceChangeHistoryTab from './tabs/BalanceChangeHistoryTab';
import BalanceChangeSummaryTab from './tabs/BalanceChangeSummaryTab';
import BonusesTab from './tabs/BonusesTab';

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`tabpanel-${index}`}
            aria-labelledby={`tab-${index}`}
            {...other}
        >
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const allUsersValue = { id: 0, login: 'ВСЕ', name: 'ВСЕ' };
export const paymentSystems = [
    {
        id: -1,
        name: 'ВСЕ',
        value: undefined,
    },
    {
        id: 0,
        name: 'ООО «МКС»',
        value: 'MCS',
    },
    {
        id: 1,
        name: 'ООО «ЛТК»',
        value: 'LTK',
    },
    {
        id: 2,
        name: 'ГУП ЛНР «РЦК»',
        value: 'RCK',
    },
    // {
    //     id: 3,
    //     name: '«Луганет»',
    //     value: 'Luganet',
    // },
    {
        id: 4,
        name: 'Платёжка',
        value: 'Platezhka',
    },
    {
        id: 5,
        name: 'Миранда',
        value: 'Miranda',
    },
];

interface PaymentsProps {}
const Payments: React.FC<PaymentsProps & { user?: any }> = ({ user }) => {
    const [tab, setTab] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setTab(newValue);
    };

    return (
        <Box>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                    value={tab}
                    onChange={handleChange}
                    variant={'scrollable'}
                    scrollButtons={true}
                >
                    <Tab value={0} label="Пополнения" {...a11yProps(0)} />
                    {user.role === Role.ADMINISTRATOR ||
                    user.role === Role.ACCOUNTANT ? (
                        <Tab
                            value={1}
                            label="Пополнения (терминалы)"
                            {...a11yProps(1)}
                        />
                    ) : null}
                    <Tab
                        label="Сводная статистика пополнений"
                        {...a11yProps(2)}
                        value={2}
                    />
                    {user.role !== Role.USER && (
                        <Tab
                            label="История изменения баланса"
                            {...a11yProps(3)}
                            value={3}
                        />
                    )}
                    {user.role !== Role.USER && (
                        <Tab
                            label="Сводная статистика изменения баланса"
                            {...a11yProps(4)}
                            value={4}
                        />
                    )}
                </Tabs>
            </Box>
            {/* Пополнения*/}
            <TabPanel value={tab} index={0}>
                <PaymentsTab />
            </TabPanel>
            {/* Пополнения ТЕРМИНАЛЫ */}
            {
                <TabPanel value={tab} index={1}>
                    <TerminalsPaymentsTab />
                </TabPanel>
            }
            {/* Сводная статистика пополнений */}
            {
                <TabPanel value={tab} index={2}>
                    <PaymentsSummaryTab />
                </TabPanel>
            }
            {/* История изменения баланса */}
            {
                <TabPanel value={tab} index={3}>
                    <BalanceChangeHistoryTab />
                </TabPanel>
            }
            {/* Сводная статистика изменения баланса*/}
            {
                <TabPanel value={tab} index={4}>
                    <BalanceChangeSummaryTab />
                </TabPanel>
            }
            {/* Бонусы */}
            {
                <TabPanel value={tab} index={5}>
                    <BonusesTab />
                </TabPanel>
            }
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps, null)(Payments);
