import * as React from 'react';
import { Box, Button, Theme, Snackbar } from '@mui/material';
import { connect } from 'react-redux';
import { setUser } from '../plugins/redux/actions';
import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-mui';
import { useFetcher } from '../utils/useFetcher';
import { makeStyles, createStyles } from '@mui/styles';
import { Alert } from '@mui/lab';
import { PaymentSystems, makePercents } from '../types';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        form: {
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',

            justifyContent: 'center',

            '& > div, & > button': {
                width: '100%',
                marginBottom: theme.spacing(2),
            },
        },
        card: {
            minWidth: 300,
        },
        button: {
            width: '100%',
        },
        cardHeader: {
            textAlign: 'center',
        },
        grid: {
            backgroundColor: theme.palette.primary.main,
            height: '100vh',
            width: '100vw',
        },
    }),
);

interface UserProps {}

const User: React.FC<UserProps & { user?: any }> = ({ user }) => {
    const styles = useStyles();
    const fetcher = useFetcher();
    const [open, setOpen] = React.useState<boolean>(false);
    const [success, setSuccess] = React.useState<boolean>(false);
    const [message, setMessage] = React.useState<string>();
    const handleClose = () => setOpen(false);

    const updateUser = async (values, { resetForm }) => {
        const res = await fetcher(`/api/users/${user.id}`, {
            method: 'PUT',
            body: JSON.stringify({
                password: values.password,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).finally(() => resetForm());

        if (res.status === 200) {
            setMessage('Пароль успешно изменен');
            setSuccess(true);
            setOpen(true);
        } else {
            setMessage('Ошибка при изменении пароля');
            setSuccess(false);
            setOpen(true);
        }
    };
    return (
        <Box sx={{ maxWidth: 600 }}>
            {user?.id && (
                <Formik
                    initialValues={{
                        ...user,
                        password: '',
                        password2: '',
                        percents: makePercents(user),
                    }}
                    validate={(values) => {
                        const errors: Partial<{ password; password2 }> = {};
                        if (!values.password || !values.password2) {
                            if (!values.password)
                                errors.password = 'Введите пароль';
                            if (!values.password2)
                                errors.password2 = 'Повторите пароль';
                        } else if (values.password !== values.password2) {
                            errors.password = 'Пароли должны совпадать';
                            errors.password2 = 'Пароли должны совпадать';
                        }
                        return errors;
                    }}
                    onSubmit={updateUser}
                >
                    {({ submitForm, isSubmitting }) => (
                        <Form className={styles.form}>
                            <Field
                                component={TextField}
                                name="name"
                                type="text"
                                label="Имя"
                                variant="standard"
                                disabled
                            />
                            {[...PaymentSystems].map(([_key, name]) => {
                                return (
                                    <Field
                                        component={TextField}
                                        name={`percents.${_key}`}
                                        type="percent"
                                        label={`Процент для ${name}`}
                                        variant="standard"
                                        disabled
                                    />
                                );
                            })}
                            <Field
                                component={TextField}
                                name="password"
                                type="text"
                                label="Пароль"
                                variant="standard"
                            />
                            <Field
                                component={TextField}
                                name="password2"
                                type="text"
                                label="Повторите пароль"
                                variant="standard"
                            />
                            <Button
                                variant="contained"
                                color="secondary"
                                disabled={isSubmitting}
                                onClick={submitForm}
                                type="submit"
                            >
                                Сохранить
                            </Button>
                        </Form>
                    )}
                </Formik>
            )}

            <Snackbar
                open={open}
                autoHideDuration={6000}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Alert
                    onClose={handleClose}
                    severity={success ? 'success' : 'error'}
                    style={{
                        width: '100%',
                    }}
                >
                    {message}
                </Alert>
            </Snackbar>
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

const mapDispatchToProps = {
    setUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
