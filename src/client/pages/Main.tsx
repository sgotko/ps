import * as React from 'react';
import { NavLink, Link, useLocation, Outlet } from 'react-router-dom';

import {
    Toolbar,
    AppBar,
    IconButton,
    Typography,
    Theme,
    Drawer,
    CssBaseline,
    List,
    ListItem,
    Divider,
    Box,
    ListItemSecondaryAction,
    Badge,
} from '@mui/material';
import {
    ExitToApp as ExitToAppIcon,
    Menu as MenuIcon,
} from '@mui/icons-material';

import { Spacer } from '../components/Spacer';

import { connect } from 'react-redux';
import { useFetcher } from '../utils/useFetcher';

import { Role } from '../../common/types';

import {
    disconnectSocket,
    initiateSocket,
    onNotificationCreated,
    onNotificationUpdated,
    onNotificationDeleted,
} from '../plugins/socket.js';
import {
    decrementNotificationsCount,
    incrementNotificationsCount,
} from '../plugins/redux/actions';

interface MainProps {}

const drawerWidth = 240;

const Main: React.FC<
    MainProps & {
        user?: any;
        notifications?: any;
        incrementNotificationsCount;
        decrementNotificationsCount;
    }
> = ({
    user,
    notifications,
    incrementNotificationsCount,
    decrementNotificationsCount,
}) => {
    const fetcher = useFetcher();
    const [loading, setLoading] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const location = useLocation();

    React.useEffect(() => handleDrawerClose(), [location]);

    React.useEffect(() => {
        if (user && user.role !== Role.ADMINISTRATOR && notifications) {
            initiateSocket('notifications');
            onNotificationCreated((err: any, data: any) => {
                if (err) return;

                incrementNotificationsCount();
            });
            onNotificationDeleted((err: any, data: any) => {
                if (err) return;
                const { id } = data;
                if (parseInt(id) > notifications.lastViewedId)
                    decrementNotificationsCount();
            });
        }
        return () => disconnectSocket();
    }, []);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleDrawerToggle = () => {
        setOpen(!open);
    };

    const handleLogout = async () => {
        setLoading(true);

        await fetcher('/auth/logout');
    };

    const container = window.document.body;
    let sidebar;
    switch (user.role) {
        case Role.ADMINISTRATOR:
            sidebar = (
                <List>
                    <ListItem to="/notifications" button component={NavLink}>
                        Объявления
                    </ListItem>
                    <ListItem to={'/users'} button component={NavLink}>
                        Пользователи
                    </ListItem>
                    <ListItem to={'/payments'} button component={NavLink}>
                        История пополнений
                    </ListItem>
                    <ListItem to={'/balance'} button component={NavLink}>
                        Баланс платежных систем
                    </ListItem>
                    <ListItem to={'/changelog'} button component={NavLink}>
                        Версии приложения
                    </ListItem>
                </List>
            );
            break;
        case Role.ACCOUNTANT:
            sidebar = (
                <List>
                    <ListItem to="/notifications" button component={NavLink}>
                        Объявления
                        <ListItemSecondaryAction>
                            {notifications.count > 0 && (
                                <Badge
                                    badgeContent={notifications.count}
                                    color="info"
                                ></Badge>
                            )}
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem to={'/users'} button component={NavLink}>
                        Пользователи
                    </ListItem>
                    <ListItem to={'/payments'} button component={NavLink}>
                        История пополнений
                    </ListItem>
                    <ListItem to={'/balance'} button component={NavLink}>
                        Баланс платежных систем
                    </ListItem>
                    <ListItem to={'/changelog'} button component={NavLink}>
                        Версии приложения
                    </ListItem>
                </List>
            );
            break;
        case Role.PARTNER:
            sidebar = (
                <List>
                    <ListItem to="/notifications" button component={NavLink}>
                        Объявления
                        <ListItemSecondaryAction>
                            {notifications.count > 0 && (
                                <Badge
                                    badgeContent={notifications.count}
                                    color="info"
                                ></Badge>
                            )}
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem to={'/users'} button component={NavLink}>
                        Пользователи
                    </ListItem>
                    <ListItem to={'/payments'} button component={NavLink}>
                        История пополнений
                    </ListItem>
                </List>
            );
            break;
        case Role.USER:
            sidebar = (
                <List>
                    <ListItem to="/notifications" button component={NavLink}>
                        Объявления
                        <ListItemSecondaryAction>
                            {notifications.count > 0 && (
                                <Badge
                                    badgeContent={notifications.count}
                                    color="info"
                                ></Badge>
                            )}
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem to={'/'} button component={NavLink}>
                        Пополнить счёт
                    </ListItem>
                    <ListItem to={'/payments'} button component={NavLink}>
                        История пополнений
                    </ListItem>
                </List>
            );
            break;
        default:
            sidebar = null;
    }

    const drawer = (
        <>
            <Divider />

            {sidebar}
        </>
    );
    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
                color="secondary"
            >
                <Toolbar>
                    <IconButton
                        onClick={handleDrawerToggle}
                        sx={{
                            display: { xs: 'block', sm: 'none' },
                        }}
                        color="inherit"
                    >
                        <MenuIcon />
                    </IconButton>

                    <Spacer />

                    <Link
                        to="/user"
                        style={{
                            color: 'white',
                            textDecoration: 'none',
                        }}
                    >
                        <Typography variant="caption" sx={{ marginRight: 2 }}>
                            {user.name}

                            {user.role !== Role.ADMINISTRATOR &&
                            user.role !== Role.ACCOUNTANT ? (
                                <>
                                    <br />
																		{user.role === Role.PARTNER ? (
																			<>
																			Баланс: {user.balance}
																			<br />
																			Cвободно: {user.available}
																			</>
																		) : (
																			<>Баланс: {user.balance}</>
																		)
																	}
                                </>
                            ) : null}
                        </Typography>
                    </Link>

                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={handleLogout}
                        disabled={loading}
                    >
                        <ExitToAppIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>

            <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
            >
                <Drawer
                    container={container}
                    variant="temporary"
                    open={open}
                    onClose={handleDrawerClose}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': {
                            boxSizing: 'border-box',
                            width: drawerWidth,
                        },
                    }}
                >
                    <Toolbar />
                    {drawer}
                </Drawer>
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: 'none', sm: 'block' },
                        width: drawerWidth,
                        flexShrink: 0,
                        [`& .MuiDrawer-paper`]: {
                            width: drawerWidth,
                            boxSizing: 'border-box',
                        },
                    }}
                    open
                >
                    <Toolbar />
                    {drawer}
                </Drawer>
            </Box>

            <Box
                component="main"
                sx={{
                    flexGrow: 1,
                    p: 3,
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                    maxWidth: '100%',
                }}
            >
                <Toolbar />
                <Outlet />
            </Box>
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
    notifications: state.notifications,
});

const mapDispatchToProps = {
    incrementNotificationsCount,
    decrementNotificationsCount,
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
