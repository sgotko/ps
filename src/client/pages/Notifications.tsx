import * as React from 'react';
import { connect } from 'react-redux';
import { useFetcher } from '../utils/useFetcher';
import { Role } from '../../common/types';
import {
    Badge,
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormGroup,
    IconButton,
    Skeleton,
    TextareaAutosize,
    Typography,
} from '@mui/material';
import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-mui';
import { Edit as EditIcon, Delete as DeleteIcon } from '@mui/icons-material';
import { setLastViewed, setNotificationsCount } from '../plugins/redux/actions';
import {
    disconnectSocket,
    initiateSocket,
    onNotificationCreated,
    onNotificationUpdated,
    onNotificationDeleted,
} from '../plugins/socket.js';

const parseTextToHtml = (text) => {
    return text.replace(/\n/g, '<br/>');
};

const dateFormatter = new Intl.DateTimeFormat('ru');

export type Notification = {
    id: number;
    text: string;
    user: any;
    created_at: Date;
    updated_at: Date;
};

type NotificationFormValues = Pick<Notification, 'text'>;

const initialValues: NotificationFormValues = {
    text: '',
};

const Notifications: React.FC<{
    user?: any;
    notifications;
    setNotificationsCount;
    setLastViewed;
}> = ({
    user,
    notifications: notificationsState,
    setNotificationsCount,
    setLastViewed,
}) => {
    const fetcher = useFetcher();
    const [notifications, setNotifications] = React.useState<
        Array<Notification>
    >([]);
    const [notification, setNotification] = React.useState<Notification>();
    const [deletion, setDeletion] = React.useState<boolean>(false);
    const [edit, setEdit] = React.useState<boolean>(false);

    const [loading, setLoading] = React.useState<boolean>(false);
    const enableLoading = () => setLoading(true);
    const disableLoading = () => setLoading(false);

    React.useEffect(() => {
        getNotifications();
    }, []);

    React.useEffect(() => {
        initiateSocket('notifications');
        onNotificationCreated((err: any, data: any) => {
            if (err) return;
            setNotifications((oldValue) => {
                return [data, ...oldValue];
            });
        });
        onNotificationUpdated((err: any, data: any) => {
            if (err) return;
            const { id } = data;
            setNotifications((oldValue) => {
                const index = oldValue.findIndex((item) => item.id === +id);
                if (index > -1) {
                    const newValue = [...oldValue];
                    newValue.splice(index, 1, data);
                    return [...newValue];
                } else return oldValue;
            });
        });
        onNotificationDeleted((err: any, data: any) => {
            if (err) return;
            const { id } = data;
            setNotifications((oldValue) => {
                return [...oldValue.filter((item) => item.id !== +id)];
            });
        });
    }, []);

    function viewNotification() {
        if (notifications.length && !document.hidden) {
            const { id } = notifications[0];
            setTimeout(() => {
                if (!document.hidden)
                    fetcher(`/api/notifications/view/${id}`, {
                        method: 'POST',
                    }).then((res) => {
                        if (res.ok) {
                            setNotificationsCount(0);
                            setLastViewed(id);
                        }
                    });
            }, 3500);
        }
    }

    React.useEffect(() => {
        window.addEventListener('focus', viewNotification);
        return () => window.removeEventListener('focus', viewNotification);
    }, [notifications]);

    const getNotifications = async () => {
        enableLoading();
        setNotifications(await (await fetcher('/api/notifications')).json());
        setTimeout(() => disableLoading(), 1000);
    };

    const createNotification = async (values, { resetForm }) => {
        try {
            const res = await fetcher('/api/notifications', {
                body: JSON.stringify({
                    text: values.text,
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST',
            });
            if (res.ok) resetForm();
        } catch (e) {
            console.error(e);
        }
    };

    const updateNotification = async (values) => {
        try {
            const res = await fetcher(
                `/api/notifications/${notification?.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        text: values.text,
                    }),
                },
            );
            if (res.ok) {
                setNotifications([
                    ...notifications.map((item) =>
                        item.id === notification?.id
                            ? { ...notification, text: values.text }
                            : item,
                    ),
                ]);
                setNotification(undefined);
                setEdit(false);
            }
        } catch (e) {
            console.error(e);
        }
    };
    const deleteNotification = async (notification) => {
        try {
            const res = await fetcher(`/api/notifications/${notification.id}`, {
                method: 'DELETE',
            });
            if (res.ok) {
                setNotifications([
                    ...notifications.filter(
                        (item) => +item.id !== notification.id,
                    ),
                ]);
                setNotification(undefined);
                setDeletion(false);
            }
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <div>
            {user.role === Role.ADMINISTRATOR && (
                <>
                    <Formik
                        initialValues={initialValues}
                        validate={(values) => {
                            const errors: Partial<NotificationFormValues> = {};
                            if (!values.text || !values.text.trim())
                                errors.text = 'Введите текст';
                            return errors;
                        }}
                        onSubmit={createNotification}
                    >
                        {({ submitForm, isSubmitting }) => (
                            <Form>
                                <FormGroup>
                                    <Field
                                        name="text"
                                        type="text"
                                        component={TextField}
                                        multiline
                                        sx={{ mb: 2 }}
                                        label="Текст"
                                        InputProps={{
                                            endAdornment: (
                                                <Button
                                                    variant="contained"
                                                    disabled={isSubmitting}
                                                    onClick={submitForm}
                                                    type="submit"
                                                    color="success"
                                                    sx={{
                                                        alignSelf: 'end',
                                                    }}
                                                >
                                                    отправить
                                                </Button>
                                            ),
                                        }}
                                    />
                                </FormGroup>
                            </Form>
                        )}
                    </Formik>
                </>
            )}

            {loading ? (
                new Array(6).fill(0).map((_, index) => (
                    <Skeleton
                        variant="rectangular"
                        width={'100%'}
                        height={125}
                        sx={{
                            mb: 1,
                        }}
                        key={index}
                    />
                ))
            ) : (
                <>
                    {notifications.length ? (
                        notifications.map((notification) => (
                            <Card
                                sx={{
                                    mb: 1,
                                    backgroundColor:
                                        user.role !== Role.ADMINISTRATOR &&
                                        notificationsState.lastViewedId <
                                            notification.id
                                            ? 'lightgrey'
                                            : '',
                                }}
                                key={notification.id}
                            >
                                <CardHeader
                                    subheader={dateFormatter.format(
                                        new Date(notification.updated_at),
                                    )}
                                ></CardHeader>
                                <CardContent>
                                    <Typography
                                        sx={{
                                            wordBreak: 'break-word',
                                        }}
                                        dangerouslySetInnerHTML={{
                                            __html: parseTextToHtml(
                                                notification.text,
                                            ),
                                        }}
                                    ></Typography>
                                </CardContent>
                                {user.role === Role.ADMINISTRATOR && (
                                    <CardActions
                                        sx={{
                                            display: 'flex',
                                            justifyContent: 'flex-end',
                                        }}
                                    >
                                        <IconButton
                                            size="small"
                                            onClick={() => {
                                                setNotification(notification);
                                                setEdit(true);
                                            }}
                                            color="info"
                                        >
                                            <EditIcon />
                                        </IconButton>
                                        <IconButton
                                            size="small"
                                            onClick={() => {
                                                setNotification(notification);
                                                setDeletion(true);
                                            }}
                                            color="error"
                                        >
                                            <DeleteIcon />
                                        </IconButton>
                                    </CardActions>
                                )}
                            </Card>
                        ))
                    ) : (
                        <Typography>Список объявлений пуст</Typography>
                    )}
                </>
            )}

            <Dialog open={deletion}>
                <DialogContent>
                    <DialogTitle>Удаление</DialogTitle>
                    <DialogContent>
                        Вы уверены, что хотите удалить сообщение?
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={() => {
                                setNotification(undefined);
                                setDeletion(false);
                            }}
                        >
                            Отмена
                        </Button>
                        <Button
                            variant="contained"
                            onClick={() => {
                                deleteNotification(notification);
                            }}
                        >
                            Ок
                        </Button>
                    </DialogActions>
                </DialogContent>
            </Dialog>

            <Dialog open={edit}>
                <DialogTitle>Редактирование</DialogTitle>
                <Formik
                    initialValues={
                        {
                            text: notification?.text,
                        } as NotificationFormValues
                    }
                    validate={(values) => {
                        const errors: Partial<NotificationFormValues> = {};
                        if (!values.text || !values.text.trim())
                            errors.text = 'Введите текст';
                        return errors;
                    }}
                    onSubmit={updateNotification}
                >
                    {({ submitForm }) => (
                        <>
                            <DialogContent>
                                <Form>
                                    <FormGroup>
                                        <Field
                                            name="text"
                                            type="text"
                                            component={TextField}
                                            multiline
                                        />
                                    </FormGroup>
                                </Form>
                            </DialogContent>
                            <DialogActions>
                                <Button
                                    onClick={() => {
                                        setNotification(undefined);
                                        setEdit(false);
                                    }}
                                >
                                    Отмена
                                </Button>
                                <Button
                                    variant="contained"
                                    type="submit"
                                    onClick={submitForm}
                                >
                                    Ок
                                </Button>
                            </DialogActions>
                        </>
                    )}
                </Formik>
            </Dialog>
        </div>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
    notifications: state.notifications,
});

const mapDispatchToProps = {
    setNotificationsCount,
    setLastViewed,
};

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
