import * as React from 'react';
import { clsx } from 'clsx';
import { makeStyles, createStyles } from '@mui/styles';
import { connect } from 'react-redux';

import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-mui';
import {
    Button,
    IconButton,
    InputAdornment,
    LinearProgress,
    Dialog,
    DialogContent,
    DialogTitle,
    Paper,
    TableContainer,
    Table,
    TableBody,
    TableRow,
    TableCell,
    TablePagination,
    Checkbox,
    Toolbar,
    Tooltip,
    lighten,
    Theme,
    Typography,
    Box,
    TextField as TextInput,
    Select,
    TableHead,
    FormControl,
    InputLabel,
    MenuItem,
    Backdrop,
    CircularProgress,
    Tabs,
    Tab,
} from '@mui/material';
import {
    Visibility,
    VisibilityOff,
    Delete as DeleteIcon,
    Clear as ClearIcon,
    Edit as EditIcon,
    Money as MoneyIcon,
    Message,
    RestoreFromTrash,
} from '@mui/icons-material';
import { IMaskInput } from 'react-imask';

import { Spacer } from '../components/Spacer';
import { useFetcher } from '../utils/useFetcher';
import { Role } from '../../common/types';

const useToolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(1),
        },
        highlight: {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        },

        title: {
            flex: '1 1 100%',
        },
    }),
);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginBottom: theme.spacing(2),
        },
        button: {
            marginBottom: theme.spacing(2),
        },
        field: {
            width: '100%',
            maxWidth: 400,
        },
        form: {
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            justifyContent: 'center',
            '& > div, & > button': {
                width: '100%',
                marginBottom: theme.spacing(2),
            },
        },
    }),
);

interface CurrencyInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const CurrencyInput = React.forwardRef<HTMLElement, CurrencyInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask={Number}
                scale={2}
                signed={true}
                padFractionalZeros={true}
                thousandsSeparator=""
                min={0}
                radix="."
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`tabpanel-${index}`}
            aria-labelledby={`tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

interface MsisdnInputProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const MsisdnInput = React.forwardRef<HTMLElement, MsisdnInputProps>(
    function TextMaskCustom(props, ref) {
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask="000-00-00"
                inputRef={ref as any}
                onAccept={(value: any) =>
                    onChange({ target: { name: props.name, value } })
                }
            />
        );
    },
);

const Terminals: React.FC<{ user: any }> = ({ user }) => {
    const classes = useStyles();
    const toolbarStyles = useToolbarStyles();
    const fetcher = useFetcher();
    const [users, setUsers] = React.useState<any>([]);
    const [showPassword, setShowPassword] = React.useState(false);
    const [showDialog, setShowDialog] = React.useState(false);
    const [selected, setSelected] = React.useState<any | null>(null);
    const [showBalanceDialog, setShowBalanceDialog] =
        React.useState<boolean>(false);

    const [username, setUsername] = React.useState<string>();
    const initialPagination = {
        page: 0,
        rowsPerPage: 15,
    };
    const [pagination, setPagination] = React.useState(initialPagination);

    const [tab, setTab] = React.useState<number>(0);
    const [loading, setLoading] = React.useState<boolean>(false);
    const disableLoading = () => setLoading(false);

    const initialValues = {
        msisdn: '',
        login: '',
        password: '',
        name: '',
        balance: '0.00',
        percent: '0.00',
        role: Role.USER,
    };

    const fetchUsers = () => {
        setLoading(true);
        if (tab === 0) {
            fetcher('/api/users/active')
                .then(async (res) => {
                    setUsers([...(await res.json())]);
                })
                .finally(() => {
                    disableLoading();
                });
        } else {
            fetcher('/api/users/deleted')
                .then(async (res) => {
                    setUsers([...(await res.json())]);
                })
                .finally(() => {
                    disableLoading();
                });
        }
    };

    const createUser = (values, { setErrors }) => {
        const msisdn = `38072${values.msisdn.replace(/-/g, '')}`;
        fetcher('/api/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                msisdn: msisdn,
                login: values.login,
                password: values.password,
                name: values.name,
                balance: values.balance,
                percent: values.percent,
                role: values.role,
            }),
        })
            .then(async (res) => {
                if (res.status === 201) setShowDialog(false);
                else {
                    let { message } = await res.json();
                    if (!message) {
                        message = 'Ошибка при создании пользователя';
                    }
                    setErrors({
                        login: message,
                        password: message,
                        name: message,
                        balance: message,
                    });
                }
            })
            .finally(() => {
                setSelected(false);
                fetchUsers();
            });
    };

    const deleteUser = () => {
        fetcher(`/api/users/${selected.id}`, {
            method: 'DELETE',
        })
            .then((res) => {})
            .catch((err) => {})
            .finally(() => {
                setSelected(null);
                fetchUsers();
            });
    };

    const restoreUser = () => {
        fetcher(`/api/users/${selected.id}/restore`, {
            method: 'POST',
        }).finally(() => {
            setSelected(null);
            fetchUsers();
        });
    };

    const handleEditUser = () => {
        setShowDialog(true);
    };

    const handleCreateUser = () => {
        setSelected(undefined);
        setShowDialog(true);
    };

    const handleAddBalance = () => {
        setShowBalanceDialog(true);
    };

    const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
        setSelected(undefined);
        setTab(newValue);
        setPagination(initialPagination);
    };

    const editUser = (values, { setErrors }) => {
        const msisdn = `38072${values.msisdn.replace(/-/g, '')}`;
        let edited: any = {
            login: values.login,
            password: values.password,
            name: values.name,
            role: values.role,
            percent: values.percent,
            msisdn: msisdn,
        };
        if (selected.balance !== values.balance)
            edited.balance = values.balance;
        fetcher(`/api/users/${selected.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(edited),
        })
            .then((res) => {
                if (res.status === 200) setShowDialog(false);
                else
                    setErrors({
                        login: 'Ошибка при редактировании пользователя',
                        password: 'Ошибка при редактировании пользователя',
                        name: 'Ошибка при редактировании пользователя',
                        balance: 'Ошибка при редактировании пользователя',
                    });
            })
            .finally(() => {
                setSelected(false);
                setShowDialog(false);
                fetchUsers();
            });
    };

    const addBalance = async (values) => {
        try {
            const res = await fetcher(`/api/users/${selected.id}/add_money`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ amount: values.amount }),
            });
            if (res.status === 201) fetchUsers();
        } catch (e) {
        } finally {
            setShowBalanceDialog(false);
        }
    };

    React.useEffect(() => {
        fetchUsers();
    }, [tab]);

    const handleChangePage = (e, page) => {
        setPagination({ ...pagination, page: page });
    };

    const handleChangeRowsPerPage = (e) => {
        const rowsPerPage = parseInt(e.target.value, 10);
        setPagination({ page: 0, rowsPerPage: rowsPerPage });
    };

    const isSelected = (id) => selected && selected.id === id;

    const handleOneSelected = (event: React.MouseEvent<unknown>, id) => {
        let newSelected = null;

        if (!isSelected(id)) {
            newSelected = users.find((item) => item.id === id);
        }

        setSelected(newSelected);
    };

    return (
        <Box>
            <Button
                variant="contained"
                color="success"
                onClick={handleCreateUser}
                className={classes.button}
            >
                Создать пользователя
            </Button>

            <Box
                sx={{
                    marginBottom: 1,
                }}
            >
                <TextInput
                    className={classes.field}
                    size="medium"
                    label="Поиск по имени"
                    placeholder="Введите имя"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="очистить"
                                    onClick={() => setUsername('')}
                                    edge="end"
                                >
                                    <ClearIcon />
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
            </Box>

            <Tabs
                value={tab}
                onChange={handleTabChange}
                aria-label="basic tabs example"
            >
                <Tab label="Активные" {...a11yProps(0)} />
                <Tab label="Удалённые" {...a11yProps(1)} />
            </Tabs>

            <TabPanel value={tab} index={0}>
                <Toolbar
                    className={clsx({
                        [toolbarStyles.highlight]: selected,
                    })}
                >
                    <Spacer />
                    {selected && (
                        <React.Fragment>
                            {!selected.isAdmin && (
                                <Tooltip title="Добавить баланс">
                                    <IconButton
                                        aria-label="add-balance"
                                        onClick={handleAddBalance}
                                    >
                                        <MoneyIcon />
                                    </IconButton>
                                </Tooltip>
                            )}
                            <Tooltip title="Редактировать">
                                <IconButton
                                    aria-label="edit"
                                    onClick={handleEditUser}
                                >
                                    <EditIcon />
                                </IconButton>
                            </Tooltip>
                            {!selected.isAdmin && !selected.deleted_at ? (
                                <Tooltip title="Удалить">
                                    <IconButton
                                        aria-label="delete"
                                        onClick={deleteUser}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </Tooltip>
                            ) : null}
                            {!selected.isAdmin && selected.deleted_at ? (
                                <Tooltip title="Восстановить">
                                    <IconButton
                                        aria-label="restore"
                                        onClick={restoreUser}
                                    >
                                        <RestoreFromTrash />
                                    </IconButton>
                                </Tooltip>
                            ) : null}
                        </React.Fragment>
                    )}
                </Toolbar>
                <TableContainer>
                    <Table
                        aria-labelledby="tableTitle"
                        size="small"
                        aria-label="enhanced table"
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                ></TableCell>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                >
                                    Пользователь
                                </TableCell>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                >
                                    Баланс
                                </TableCell>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                >
                                    Дата/время последней активности
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users
                                .filter((user) =>
                                    username
                                        ? user.name
                                              .toLowerCase()
                                              .includes(username.toLowerCase())
                                        : true,
                                )
                                .slice(
                                    pagination.page * pagination.rowsPerPage,
                                    pagination.page * pagination.rowsPerPage +
                                        pagination.rowsPerPage,
                                )
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.id);
                                    const labelId = `table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) =>
                                                handleOneSelected(event, row.id)
                                            }
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.id}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    checked={isItemSelected}
                                                    inputProps={{
                                                        'aria-labelledby':
                                                            labelId,
                                                    }}
                                                    value={selected}
                                                />
                                            </TableCell>
                                            <TableCell
                                                component="td"
                                                scope="row"
                                                align="left"
                                            >
                                                {row.parent ? (
                                                    <>
                                                        {row.name.slice(
                                                            0,
                                                            row.name.lastIndexOf(
                                                                '(',
                                                            ),
                                                        )}
                                                        <b>
                                                            <i>
                                                                {row.name.slice(
                                                                    row.name.lastIndexOf(
                                                                        '(',
                                                                    ),
                                                                )}
                                                            </i>
                                                        </b>
                                                    </>
                                                ) : (
                                                    row.name
                                                )}
                                            </TableCell>
                                            <TableCell
                                                component="td"
                                                scope="row"
                                                align="left"
                                            >
                                                <Typography
                                                    sx={{
                                                        color:
                                                            parseInt(
                                                                row.balance,
                                                            ) === 0
                                                                ? 'red'
                                                                : 'inherit',
                                                    }}
                                                >
                                                    {parseInt(row.balance)}
                                                </Typography>
                                            </TableCell>
                                            <TableCell
                                                component="td"
                                                scope="row"
                                                align="left"
                                            >
                                                {row.last_login
                                                    ? new Date(
                                                          row.last_login,
                                                      ).toLocaleString('ru')
                                                    : '--------'}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                        <TablePagination
                            rowsPerPageOptions={[15, 25, 35]}
                            onPageChange={handleChangePage}
                            rowsPerPage={pagination.rowsPerPage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            count={users.length}
                            page={pagination.page}
                        />
                    </Table>
                </TableContainer>

                <Dialog
                    open={showDialog}
                    onClose={() => setShowDialog(false)}
                    aria-labelledby="dialog-title"
                    aria-describedby="dialog-description"
                >
                    <DialogTitle id="dialog-title">
                        {selected ? 'Редактирование' : 'Создание'} пользователя
                    </DialogTitle>
                    <DialogContent>
                        <Formik
                            initialValues={
                                selected
                                    ? {
                                          ...selected,
                                          msisdn:
                                              selected?.msisdn?.length > 0
                                                  ? selected?.msisdn.slice(5)
                                                  : selected?.msisdn,
                                      }
                                    : initialValues
                            }
                            validate={(values) => {
                                const errors: any = {};
                                if (!values.login) {
                                    errors.login = 'Введите логин';
                                }
                                if (!values.name)
                                    errors.name = 'Введите имя пользователя';

                                if (!values.msisdn)
                                    errors.msisdn = 'Введите номер телефона';

                                if (!selected && !values.password) {
                                    errors.password = 'Введите пароль';
                                }

                                return errors;
                            }}
                            onSubmit={selected ? editUser : createUser}
                        >
                            {({
                                submitForm,
                                isSubmitting,
                                values,
                                setFieldValue,
                            }) => (
                                <Form className={classes.form}>
                                    <Field
                                        component={TextField}
                                        name="name"
                                        type="name"
                                        label="Имя пользователя"
                                        variant="standard"
                                    />
                                    <Field
                                        component={TextField}
                                        name="login"
                                        type="login"
                                        label="Логин"
                                        variant="standard"
                                        disabled={selected}
                                    />
                                    {user.isAdmin && (
                                        <FormControl fullWidth>
                                            <InputLabel id="role-label">
                                                Роль
                                            </InputLabel>
                                            <Select
                                                labelId="role-label"
                                                id="role"
                                                value={values.role}
                                                label="Роль"
                                                name="role"
                                                onChange={(e) =>
                                                    setFieldValue(
                                                        'role',
                                                        e.target.value,
                                                    )
                                                }
                                            >
                                                {Object.entries(Role)
                                                    .slice(1)
                                                    .map(([key, value]) => {
                                                        return (
                                                            <MenuItem
                                                                value={value}
                                                                key={key}
                                                            >
                                                                {value}
                                                            </MenuItem>
                                                        );
                                                    })}
                                            </Select>
                                        </FormControl>
                                    )}
                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="msisdn"
                                        type="msisdn"
                                        label="Номер телефона"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: MsisdnInput,
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    +38072
                                                </InputAdornment>
                                            ),
                                        }}
                                    />

                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="balance"
                                        type="balance"
                                        label="Баланс"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: CurrencyInput,
                                        }}
                                    />

                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="percent"
                                        type="percent"
                                        label="Процент"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: CurrencyInput,
                                        }}
                                    />

                                    <Field
                                        component={TextField}
                                        variant="standard"
                                        type={
                                            showPassword ? 'text' : 'password'
                                        }
                                        name="password"
                                        label="Пароль"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        onClick={() =>
                                                            setShowPassword(
                                                                !showPassword,
                                                            )
                                                        }
                                                    >
                                                        {showPassword ? (
                                                            <Visibility />
                                                        ) : (
                                                            <VisibilityOff />
                                                        )}
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                    />

                                    {isSubmitting && <LinearProgress />}
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        disabled={isSubmitting}
                                        onClick={submitForm}
                                        type="submit"
                                    >
                                        {selected ? 'Сохранить' : 'Создать'}
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                    </DialogContent>
                </Dialog>

                {/* Add balance dialog */}
                <Dialog
                    open={showBalanceDialog}
                    onClose={() => setShowBalanceDialog(false)}
                    aria-labelledby="dialog-title"
                    aria-describedby="dialog-description"
                >
                    <DialogTitle id="dialog-title">
                        Добавление баланса
                    </DialogTitle>
                    <DialogContent>
                        <Formik
                            initialValues={{
                                amount: '0.00',
                            }}
                            validate={(values) => {
                                const errors: any = {};
                                if (!values.amount) {
                                    errors.amount = 'Введите сумму';
                                }

                                return errors;
                            }}
                            onSubmit={addBalance}
                        >
                            {({ submitForm, isSubmitting }) => (
                                <Form className={classes.form}>
                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="amount"
                                        type="amount"
                                        label="Сумма"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: CurrencyInput,
                                        }}
                                    />

                                    {isSubmitting && <LinearProgress />}
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        disabled={isSubmitting}
                                        onClick={submitForm}
                                        type="submit"
                                    >
                                        Добавить
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                    </DialogContent>
                </Dialog>
            </TabPanel>
            <TabPanel value={tab} index={1}>
                <Toolbar
                    className={clsx({
                        [toolbarStyles.highlight]: selected,
                    })}
                >
                    <Spacer />
                    {selected && (
                        <React.Fragment>
                            {!selected.isAdmin && (
                                <Tooltip title="Добавить баланс">
                                    <IconButton
                                        aria-label="add-balance"
                                        onClick={handleAddBalance}
                                    >
                                        <MoneyIcon />
                                    </IconButton>
                                </Tooltip>
                            )}
                            <Tooltip title="Редактировать">
                                <IconButton
                                    aria-label="edit"
                                    onClick={handleEditUser}
                                >
                                    <EditIcon />
                                </IconButton>
                            </Tooltip>
                            {!selected.isAdmin && !selected.deleted_at ? (
                                <Tooltip title="Удалить">
                                    <IconButton
                                        aria-label="delete"
                                        onClick={deleteUser}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </Tooltip>
                            ) : null}
                            {!selected.isAdmin && selected.deleted_at ? (
                                <Tooltip title="Восстановить">
                                    <IconButton
                                        aria-label="restore"
                                        onClick={restoreUser}
                                    >
                                        <RestoreFromTrash />
                                    </IconButton>
                                </Tooltip>
                            ) : null}
                        </React.Fragment>
                    )}
                </Toolbar>
                <TableContainer>
                    <Table
                        aria-labelledby="tableTitle"
                        size="small"
                        aria-label="enhanced table"
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                ></TableCell>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                >
                                    Пользователь
                                </TableCell>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                >
                                    Баланс
                                </TableCell>
                                <TableCell
                                    sx={{
                                        fontWeight: 'bold',
                                    }}
                                    align="left"
                                >
                                    Дата/время последней активности
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users
                                .filter((user) =>
                                    username
                                        ? user.name
                                              .toLowerCase()
                                              .includes(username.toLowerCase())
                                        : true,
                                )
                                .slice(
                                    pagination.page * pagination.rowsPerPage,
                                    pagination.page * pagination.rowsPerPage +
                                        pagination.rowsPerPage,
                                )
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.id);
                                    const labelId = `table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) =>
                                                handleOneSelected(event, row.id)
                                            }
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.id}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    checked={isItemSelected}
                                                    inputProps={{
                                                        'aria-labelledby':
                                                            labelId,
                                                    }}
                                                    value={selected}
                                                />
                                            </TableCell>
                                            <TableCell
                                                component="td"
                                                scope="row"
                                                align="left"
                                            >
                                                {row.parent ? (
                                                    <>
                                                        {row.name.slice(
                                                            0,
                                                            row.name.lastIndexOf(
                                                                '(',
                                                            ),
                                                        )}
                                                        <b>
                                                            <i>
                                                                {row.name.slice(
                                                                    row.name.lastIndexOf(
                                                                        '(',
                                                                    ),
                                                                )}
                                                            </i>
                                                        </b>
                                                    </>
                                                ) : (
                                                    row.name
                                                )}
                                            </TableCell>
                                            <TableCell
                                                component="td"
                                                scope="row"
                                                align="left"
                                            >
                                                <Typography
                                                    sx={{
                                                        color:
                                                            parseInt(
                                                                row.balance,
                                                            ) === 0
                                                                ? 'red'
                                                                : 'inherit',
                                                    }}
                                                >
                                                    {parseInt(row.balance)}
                                                </Typography>
                                            </TableCell>
                                            <TableCell
                                                component="td"
                                                scope="row"
                                                align="left"
                                            >
                                                {row.last_login
                                                    ? new Date(
                                                          row.last_login,
                                                      ).toLocaleString('ru')
                                                    : '--------'}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                        <TablePagination
                            rowsPerPageOptions={[15, 25, 35]}
                            onPageChange={handleChangePage}
                            rowsPerPage={pagination.rowsPerPage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            count={users.length}
                            page={pagination.page}
                        />
                    </Table>
                </TableContainer>

                <Dialog
                    open={showDialog}
                    onClose={() => setShowDialog(false)}
                    aria-labelledby="dialog-title"
                    aria-describedby="dialog-description"
                >
                    <DialogTitle id="dialog-title">
                        {selected ? 'Редактирование' : 'Создание'} пользователя
                    </DialogTitle>
                    <DialogContent>
                        <Formik
                            initialValues={
                                selected
                                    ? {
                                          ...selected,
                                          msisdn:
                                              selected?.msisdn?.length > 0
                                                  ? selected?.msisdn.slice(5)
                                                  : selected?.msisdn,
                                      }
                                    : initialValues
                            }
                            validate={(values) => {
                                const errors: any = {};
                                if (!values.login) {
                                    errors.login = 'Введите логин';
                                }
                                if (!values.name)
                                    errors.name = 'Введите имя пользователя';

                                if (!values.msisdn)
                                    errors.msisdn = 'Введите номер телефона';

                                if (!selected && !values.password) {
                                    errors.password = 'Введите пароль';
                                }

                                return errors;
                            }}
                            onSubmit={selected ? editUser : createUser}
                        >
                            {({
                                submitForm,
                                isSubmitting,
                                values,
                                setFieldValue,
                            }) => (
                                <Form className={classes.form}>
                                    <Field
                                        component={TextField}
                                        name="name"
                                        type="name"
                                        label="Имя пользователя"
                                        variant="standard"
                                    />
                                    <Field
                                        component={TextField}
                                        name="login"
                                        type="login"
                                        label="Логин"
                                        variant="standard"
                                        disabled={selected}
                                    />
                                    {user.isAdmin && (
                                        <FormControl fullWidth>
                                            <InputLabel id="role-label">
                                                Роль
                                            </InputLabel>
                                            <Select
                                                labelId="role-label"
                                                id="role"
                                                value={values.role}
                                                label="Роль"
                                                name="role"
                                                onChange={(e) =>
                                                    setFieldValue(
                                                        'role',
                                                        e.target.value,
                                                    )
                                                }
                                            >
                                                {Object.entries(Role)
                                                    .slice(1)
                                                    .map(([key, value]) => {
                                                        return (
                                                            <MenuItem
                                                                value={value}
                                                                key={key}
                                                            >
                                                                {value}
                                                            </MenuItem>
                                                        );
                                                    })}
                                            </Select>
                                        </FormControl>
                                    )}
                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="msisdn"
                                        type="msisdn"
                                        label="Номер телефона"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: MsisdnInput,
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    +38072
                                                </InputAdornment>
                                            ),
                                        }}
                                    />

                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="balance"
                                        type="balance"
                                        label="Баланс"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: CurrencyInput,
                                        }}
                                    />

                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="percent"
                                        type="percent"
                                        label="Процент"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: CurrencyInput,
                                        }}
                                    />

                                    <Field
                                        component={TextField}
                                        variant="standard"
                                        type={
                                            showPassword ? 'text' : 'password'
                                        }
                                        name="password"
                                        label="Пароль"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        onClick={() =>
                                                            setShowPassword(
                                                                !showPassword,
                                                            )
                                                        }
                                                    >
                                                        {showPassword ? (
                                                            <Visibility />
                                                        ) : (
                                                            <VisibilityOff />
                                                        )}
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                    />

                                    {isSubmitting && <LinearProgress />}
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        disabled={isSubmitting}
                                        onClick={submitForm}
                                        type="submit"
                                    >
                                        {selected ? 'Сохранить' : 'Создать'}
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                    </DialogContent>
                </Dialog>

                {/* Add balance dialog */}
                <Dialog
                    open={showBalanceDialog}
                    onClose={() => setShowBalanceDialog(false)}
                    aria-labelledby="dialog-title"
                    aria-describedby="dialog-description"
                >
                    <DialogTitle id="dialog-title">
                        Добавление баланса
                    </DialogTitle>
                    <DialogContent>
                        <Formik
                            initialValues={{
                                amount: '0.00',
                            }}
                            validate={(values) => {
                                const errors: any = {};
                                if (!values.amount) {
                                    errors.amount = 'Введите сумму';
                                }

                                return errors;
                            }}
                            onSubmit={addBalance}
                        >
                            {({ submitForm, isSubmitting }) => (
                                <Form className={classes.form}>
                                    <Field
                                        className={classes.field}
                                        component={TextField}
                                        name="amount"
                                        type="amount"
                                        label="Сумма"
                                        variant="standard"
                                        InputProps={{
                                            inputComponent: CurrencyInput,
                                        }}
                                    />

                                    {isSubmitting && <LinearProgress />}
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        disabled={isSubmitting}
                                        onClick={submitForm}
                                        type="submit"
                                    >
                                        Добавить
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                    </DialogContent>
                </Dialog>
            </TabPanel>

            <Backdrop
                sx={{
                    color: '#fff',
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                }}
                open={loading}
                onClick={disableLoading}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </Box>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps, null)(Users);
