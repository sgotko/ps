export const PaymentSystems: Map<string, string> = new Map<string, string>([
    ["MCS", 'ООО «МКС»'],
    ["LTK", 'OOO «ЛТК»'],
    ["RCK", 'ГУП ЛНР «РЦК»'],
    ["Luganet", '«Луганет»'],
	[ "Platezhka", 'Платёжка' ],
	["Miranda", 'Миранда']
] );

export const defaultPercents = (keys: string[]): Record<string, string> => {
	const percents = {};
	for ( let key of keys ) {
		percents[ key ] = "0.00";
	}
	return percents;
}

export const makePercents = ( user: any ): Record<string, string> => {
	const percents = {};
	user.percents.forEach(percent => {
		percents[ percent.ps ] = percent.percent;
	});
	return percents;
}