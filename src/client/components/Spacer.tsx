import * as React from 'react';

export const Spacer = () => {
    return (
        <span
            style={{
                flexGrow: 1,
                boxSizing: 'border-box',
            }}
        >
            {''}
        </span>
    );
};
