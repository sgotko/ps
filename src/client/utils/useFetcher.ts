export const fetcher = async (input: RequestInfo, init?: RequestInit) => {
        const res = await fetch(`${input}`, init);

        if (res.redirected && window.location.pathname !== '/login')
            window.location.reload();
        return res;
    };

export const useFetcher = () => {
    return fetcher;
};
