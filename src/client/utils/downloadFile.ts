const downloadFileFromResponse = async (response: any) => {
    let filename = '';
    const disposition = response.headers.get('content-disposition');
    if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
    }
    const url = window.URL.createObjectURL(new Blob([await response.blob()]));
    const link = window.document.createElement('a');
    link.href = url;

    link.setAttribute('download', decodeURI(filename));
    window.document.body.appendChild(link);
    link.click();
    window.document.body.removeChild(link);
};

export default downloadFileFromResponse;
