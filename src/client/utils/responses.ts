const isSuccess = (response) => {
    const { status } = response;

    return status === 200 || status === 201;
};

export { isSuccess };