import * as React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { ThemeProvider } from '@mui/styles';
import { createTheme } from '@mui/material/styles';
import { ruRU } from '@mui/material/locale';

import store from './plugins/redux/store';

import Login from './pages/Login';
import Main from './pages/Main';

import AddPayments from './pages/AddPayments';
import Users from './pages/Users';
import User from './pages/User';

import Payments from './pages/Payments/index';
import Balance from './pages/Balance';
import Changelog from './pages/Changelog';

import { connect } from 'react-redux';
import {
    setUser,
    setNotificationsCount,
    setLastViewed,
} from './plugins/redux/actions';

const theme = createTheme(
    {
        palette: {
            primary: {
                main: '#512da8',
            },
            secondary: {
                main: '#ab47bc',
            },
            success: {
                main: '#76ff03',
            },
        },
    },
    ruRU,
);

import './app.css';
import { CssBaseline } from '@mui/material';
import Notifications from './pages/Notifications';
import { useFetcher } from './utils/useFetcher';

const AppBefore = ({ setUser, setNotificationsCount, setLastViewed }) => {
    const fetcher = useFetcher();
    React.useEffect(() => {
        fetcher('/api/users/user').then(async (res) => {
            if (res.status === 200) setUser(await res.json());
        });
        fetcher('/api/notifications/viewed').then(async (res) => {
            if (res.ok) {
                const { notifications_count, last_viewed } = await res.json();
                setNotificationsCount(notifications_count);
                setLastViewed(last_viewed);
            }
        });
    }, []);

    return (
        <Routes>
            <Route path="login" element={<Login />} />
            <Route path="/" element={<Main />}>
                <Route index element={<AddPayments />} />
                <Route path="users" element={<Users />} />
                <Route path="user" element={<User />} />
                <Route path="payments" element={<Payments />} />
                <Route path={'/balance'} element={<Balance />} />
                <Route path={'/notifications'} element={<Notifications />} />

                <Route path="changelog" element={<Changelog />} />
            </Route>
        </Routes>
    );
};

const mapDispatchToProps = {
    setUser,
    setNotificationsCount,
    setLastViewed,
};

const App = connect(null, mapDispatchToProps)(AppBefore);

render(
    <Provider store={store}>
        <Router>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <App />
            </ThemeProvider>
        </Router>
    </Provider>,
    document.getElementById('app'),
);
