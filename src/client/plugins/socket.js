const io = require('socket.io-client');
let socket;

export const initiateSocket = (room) => {
    socket = io('/events');
    if (socket && room) socket.emit('join', room);
};

export const disconnectSocket = () => {
    if (socket) socket.disconnect();
};

export const onNotificationCreated = (cb) => {
    if (!socket) return true;
    socket.on('notification.created', (request) => {
        return cb(null, request);
    });
};

export const onNotificationUpdated = (cb) => {
    if (!socket) return true;
    socket.on('notification.updated', (request) => {
        return cb(null, request);
    });
};

export const onNotificationDeleted = (cb) => {
    if (!socket) return true;
    socket.on('notification.deleted', (request) => {
        return cb(null, request);
    });
};
