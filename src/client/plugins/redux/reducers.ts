import { combineReducers } from 'redux';

import {
    CLEAR_STORAGE,
    DECREMENT_NOTIFICATIONS_NUMBER,
    INCREMENT_NOTIFICATIONS_NUMBER,
    SET_LAST_VIEWED,
    SET_NOTIFICATIONS_COUNT,
    SET_USER,
} from './actions';

function userReduser(state = { isAdmin: false }, action) {
    switch (action.type) {
        case SET_USER:
            return { ...action.value };
        default:
            return state;
    }
}

function notificationsReducer(
    state = {
        lastViewedId: 0,
        length: 0,
        count: 0,
    },
    action,
) {
    switch (action.type) {
        case SET_NOTIFICATIONS_COUNT:
            return { ...state, count: action.value };
        case INCREMENT_NOTIFICATIONS_NUMBER:
            return { ...state, count: state.count + 1 };
        case DECREMENT_NOTIFICATIONS_NUMBER:
            return { ...state, count: state.count > 0 ? state.count - 1 : 0 };
        case SET_LAST_VIEWED:
            return { ...state, lastViewedId: action.value };
        default:
            return state;
    }
}

const appReducer = combineReducers({
    user: userReduser,
    notifications: notificationsReducer,
});

const rootReducer = (state, action) => {
    switch (action.type) {
        case CLEAR_STORAGE:
            return undefined;
        default:
            return appReducer(state, action);
    }
};

export default rootReducer;
