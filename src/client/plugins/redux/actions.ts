/*
 * action types
 */

export const SET_USER = 'SET_USER';
export const CLEAR_STORAGE = 'CLEAR_STORAGE';

export const SET_NOTIFICATIONS_COUNT = 'SET_NOTIFICATIONS_COUNT';
export const INCREMENT_NOTIFICATIONS_NUMBER = 'INCREMENT_NOTIFICATIONS_COUNT';
export const DECREMENT_NOTIFICATIONS_NUMBER = 'DECREMENT_NOTIFICATIONS_NUMBER';
export const SET_LAST_VIEWED = 'SET_LAST_VIEWED';
/*
 * action creators
 */

export const clearStorage = () => {
    return { type: CLEAR_STORAGE };
};
export const setUser = (value) => {
    return { type: SET_USER, value };
};
export const setNotificationsCount = (value) => {
    return { type: SET_NOTIFICATIONS_COUNT, value };
};
export const incrementNotificationsCount = () => ({
    type: INCREMENT_NOTIFICATIONS_NUMBER,
});
export const decrementNotificationsCount = () => ({
    type: DECREMENT_NOTIFICATIONS_NUMBER,
});
export const setLastViewed = (value) => ({ type: SET_LAST_VIEWED, value });
