import { fallback } from '../src/server/jitterFallback';

describe('Jitter tests', () => {
    const alwaysFail = () => Promise.reject();

    const success = (count: number) => {
        let number = count;
        return () => {
            if (number > 0) {
                number--;
                return Promise.reject();
            }
            return Promise.resolve();
        };
    };

    it('Must fail', () => {
        expect(
            fallback({
                cb: alwaysFail,
                maxRetry: 5,
                startTimeout: 10,
            }),
        ).rejects.toEqual(undefined);
    });

    it('Must success', () => {
        expect(
            fallback({
                cb: success(4),
                maxRetry: 5,
                startTimeout: 10,
            }),
        ).resolves;
    });

    it('Max retry exceed', () => {
        expect(
            fallback({
                cb: success(6),
                maxRetry: 5,
                startTimeout: 10,
            }),
        ).rejects.toEqual(undefined);
    });
});
