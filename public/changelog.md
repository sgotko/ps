#### Версия 0.6.6
-   Улучшена загрузка списка пользователей для страниц отчётов

#### Версия 0.6.5

-   Сводная статистика пополнений с фильтром по платежной системе;
-   Новая платежная система: ГУП ЛНР "РЦК";

#### Версия 0.6

-   Авторизация и пополнение с помощью мобильного кода "7959"
-   Роль "БУХГАЛТЕР"
-   Добавлена возможность пополнять счёт "ЛТК". В связи с этим:
    -   переработана часть серверного кода, взаимодействующая с платежными системами;
    -   добавлена возможность реализации различных платежных систем на стороне сервера;
    -   добавлены разделы в "Пополнить счёт" для пополнения счётов различных платежных систем;
    -   добавлен пункт меню "Баланс платежных систем", в котором можно отследить текущий баланс различных платежных систем, реализованных в приложении;
    -   переработаны логи пополнений (обычных и через API): теперь они хранят тип платежной системы, в которой был совершен платеж;
    -   получение логов пополнений можно запрашивать с учётом платежной системы, с помощью которой был произведен платеж;
    -   в логи пополнений через API добавлено поле "transaction", в которое можно передать идентификатор транзакции со стороны вызывающей API;

#### Версия 0.5

-   Добавлены роли для пользователей: "АДМИНИСТРАТОР", "ПАРТНЁР", "ПОЛЬЗОВАТЕЛЬ"
-   Добавлены бонусы для партнёров
-   Улучшены методы получения статистик пополнений

#### Версия 0.4

-   В раздел "История пополнений" добавились вкладки "История изменения баланса" и "Сводная статистика изменения баланса"
-   Исправлена ошибка, когда в "Сводной статистике пополнений" сумма на начало периода была всегда меньше суммы на конец периода
-   Исправлена ошибка, когда в выборе любой статистики не учитывался последний день на конец периода

#### Версия 0.3

-   Отображение даты последней активности (транзакции) пользователя
-   Фильтр пользователей в выгрузке платежей
-   Сортировка пользователей по алфовиту
-   Проверка номера во время пополнения
-   Самостоятельная смена пароля для пользователя (нажмите вверху на имя пользователя для перехода на страницу смены пароля)
-   В форме пополнения теперь номер телефона сбрасывается после успешного пополнения
-   Раздел "История пополнений" разделился на 2 части:
    -   Во вкладке "Платежи" отображаются все платежи за период
    -   Во вкладке "Сводная статистика" при выборе за период отображаются баланс на начало периода, баланс на конец периода и итоговая потраченная сумма за период

#### Версия 0.2

-   Выделение красным цветом нулевого баланса у пользователя
-   Мобильная версия
-   Добавлены проценты у пользователей:
    -   При пополнении снимается сумма, равная "количество + проценты от количества"
    -   В разделе "История платежей" отображаются "количество", "проценты" и "итого"
-   В разделе "Пользователи" добавлена возможность добавлять средства к текущему балансу пользователя
-   Удаление пользователей
-   В разделе "История платежей" в списке выбора пользователей теперь отображаются их имена, а не логины
-   Вход по коду из СМС
-   История изменений

#### Версия 0.1

-   Пополнение счёта абонента
-   Добавление, редактирование пользователей
-   История платежей с выбором всех или конкретного пользователя
